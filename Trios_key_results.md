# Some take home messages from the trio assignment

## Basic orthofinder [run 2b](https://git.ist.ac.at/bvicoso/carp_2022/-/blob/main/trios.md#2b-run-orthofinder) statistics support the diploidy of our barb species

B | Barb | Cyprinus_carpio | Danio_rerio.GRCz11 | Paracanthobrama_guichenoti | Puntius_tetrazona
--- | --- | --- | --- | --- | ---
Number of genes | 51880 | 47942 | 30313 | 24307 | 21946
Number of genes in orthogroups | 21541 | 42016 | 28977 | 22802 | 20944
% of orthogroups with 0 genes | 28.8 | 12.6 | 18.6 | 20.4 | 25.4
% of orthogroups with 1 genes | **57.4** | 28.9 | **57.0** | **67.5** | **63.0**
% of orthogroups with 2 genes | 9.9 | **44.4** | 16.8 | 8.4 | 8.5


## Orthogroups that have 1 ortholog in Barb tend to have 2 in carp

Number of carp homologs | Number of Orthogroups
--- | ---
0 | 917
1 | 4141
2 | **6802**
3 | 621
4 | 264
5 | 54
6 | 40
7 | 23

<details><summary>Click here to recover these counts</summary>

```
cat Orthogroups/Orthogroups.GeneCount.tsv | awk '($2=="1")' | cut -f 3 | sort | uniq -c | sort -nr | head | awk '{print $2,$1}' | sort -n
```

</details>

But there is clearly a lot of rediploidization!

## Distribution of 1:1 genes on two subgenomes

First make the orthologues file unix readable:

```
~/scripts/flip.linux-static -u Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv
```

Then let's check whether 1:1 orthologues are primarily on one subgenome:

```
cat Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep -v ', CAF' | awk '{print $3, $2}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $4}' | sort |  uniq -c | grep '[AB]' | awk '{print $2, $1}' | sort -n
```

<details><summary>Click here for full count</summary>

Sub1 | Genes | Sub2 | Genes
--- | --- | --- | ---
A1 | 172 | B1 | 154
A10 | 78 | B10 | 142
A11 | 98 | B11 | 128
A12 | 120 | B12 | 134
A13 | 112 | B13 | 147
A14 | 109 | B14 | 108
A15 | 75 | B15 | 131
A16 | 95 | B16 | 147
A17 | 102 | B17 | 131
A18 | 90 | B18 | 126
A19 | 103 | B19 | 152
A2 | 128 | B2 | 180
A20 | 113 | B20 | 149
A21 | 199 | B21 | 100
A22 | 62 | B22 | 152
A23 | 111 | B23 | 126
A24 | 79 | B24 | 104
A25 | 99 | B25 | 134
A3 | 162 | B3 | 176
A4 | 80 | B4 | 121
A5 | 178 | B5 | 215
A6 | 105 | B6 | 134
A7 | 148 | B7 | 184
A8 | 154 | B8 | 122
A9 | 112 | B9 | 127
sumA | 2884 | sumB | 3524

</details>

At first sight, yes:
subA: 2884, sunB: 3524
Versus: totalA: 22025, totalB: 22935 (in file: ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable)

This gives the following 2x2 table:

bla | SubA | SubB
--- | --- | ---
Diploid | 2884 | 3524
NotDip | 19141 | 19411

p<.0001 (but this could just be due to the proximity between Barb and subgenome B)


## Distribution of 1:2 genes on two subgenomes

Let's check that 1:2 genes have one duplicate on each subgenome. 

First, let's count how many 1:2 orthologs we have:

```
#number of 1:1
cat Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep -v ', CAF' | wc
#number of 1:many
cat Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep -o -n ',' | perl -pi -e 's/:,//gi' | sort | uniq -c | awk '{print $1}' | sort | uniq -c | sort -rn | awk '{print $2+1, $1}' | head
```
Number of orthologs | Genes
--- | ---
1 | (6641)
2 | 7827
3 | 536
4 | 140
5 | 35
6 | 30
7 | 21


Now let's look at how many are in the two subgenome:

```
cat Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep ',*,' | awk '($5 !~ /CAFS/)' | perl -pi -e 's/, /\t/gi' | awk '{print $3, $1, $2, $4}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $4, $2, $3, $1, $6}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $2, $3, $4, $5, $1, $7}' | awk '{print $4, $6}' | perl -pi -e 's/[0-9]*//gi' | sort | uniq -c | sort -rn 
```

Number of pairs | SubgenomeLocation
--- | ---
4366 | A B
2875 | B A
119 | A A
111 | B B
104 | S B
81 | A S
74 | B S
48 | S A
14 | S S

--> 7241 with one homeolog in each subgenome!
