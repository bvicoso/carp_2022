# dS analysis of inferred carp homeologs

Let's take the barb and carp protein coding sequences and estimate dS between homeologs (dS_ab).

## Get files

WD: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/4-dNdS

Fasta files:

```
ln -s ../Cyprinus_carpio-mRNA.fa .
ln -s ../Barb.trinity.fasta.long.500bp_min_unwrapped.fasta .
```

Keep only longest ORF:

```
#make all capital letters
cat Cyprinus_carpio-mRNA.fa | perl -lane 'print uc($_)' > Cyprinus_carpio_capital.fa
perl ~/scripts/GetLongestCDS_v2_October2020.pl Cyprinus_carpio_capital.fa
perl ~/scripts/GetLongestCDS_v2_October2020.pl Barb.trinity.fasta.long.500bp_min_unwrapped.fasta
```

Get list of homeologs and their barb homologs:

```
ln -s ../1-Trios/3-OrthoTables/Barb_Carp_1to2_exhaust_correctChrom.txt .
```

And make three lists for the three analyses:

```
 cat Barb_Carp_1to2_exhaust_correctChrom.txt | awk '{print $2, $3}' > List_barb_subA.txt
 cat Barb_Carp_1to2_exhaust_correctChrom.txt | awk '{print $2, $5}' > List_barb_subB.txt
 cat Barb_Carp_1to2_exhaust_correctChrom.txt | awk '{print $3, $5}' > List_subA_subB.txt
```

## Get dS values

The script dnds_estimate.pl is [here](scripts/dnds_estimate.pl).

```
module load blat/20170224
module load gblocks/0.91b
module load muscle

perl dnds_estimate.pl List_barb_subA.txt Barb.trinity.fasta.long.500bp_min_unwrapped.fasta.cds Cyprinus_carpio_capital.fa.cds dS_a
perl dnds_estimate.pl List_barb_subB.txt Barb.trinity.fasta.long.500bp_min_unwrapped.fasta.cds Cyprinus_carpio_capital.fa.cds dS_b
perl dnds_estimate.pl List_subA_subB.txt Cyprinus_carpio_capital.fa.cds Cyprinus_carpio_capital.fa.cds dS_ab
```


Parse results:

```
cat dS_a/KaKs/* | cut -f 1,2,4,7 | grep 'NG' | perl -pi -e 's/-/\t/gi' > dS_a_NG.txt
cat dS_b/KaKs/* | cut -f 1,2,4,7 | grep 'NG' | perl -pi -e 's/-/\t/gi' > dS_b_NG.txt
cat dS_ab/KaKs/* | cut -f 1,2,4,7 | grep 'NG' | perl -pi -e 's/-/\t/gi' > dS_ab_NG.txt
```

## Plot

In R:

```
par(mfrow=c(1,2))
#Panel1: 
par( mar=c(0,0,0,0))
plot(0,type='n',axes=FALSE,ann=FALSE, ylim=c(0,10), xlim=c(0,10))

cola<-"#1F78B4"
colb<-"#33A02C"
colab<-"#FB9A99"

xstart <-1
xend<-6
xmid=(xstart+xend)/2
ystart <-5
yend<-8
yend2<-(ystart-(yend-ystart))

ymid=(yend + yend2)/2

segments(xstart, ystart,xend, yend)
segments(xstart, ystart,xend, yend2)
segments(xmid, (ystart+yend)/2,xend,ymid)
text(xend+1.5,yend, "Barb")
text(xend+1.5, ymid, "Carp SubB")
text(xend+1.5,yend2, "Carp SubA")

segments(xstart-0.3, ystart,xend, yend+0.2, col=cola, lwd=3)
segments(xstart-0.3, ystart,xend, yend2-0.2, col=cola, lwd=3)

segments(xstart+0.3, ystart,xend, yend2+0.2, col=colab, lwd=3)
segments(xstart+0.3, ystart, xmid, ((ystart+yend)/2)-0.2, col=colab, lwd=3)
segments(xmid, ((ystart+yend)/2)-0.2, xend, ymid-0.2, col=colab, lwd=3)

segments(xmid+0.3, ((ystart+yend)/2), xend, ymid+0.2, col=colb, lwd=3)
segments(xmid+0.3, ((ystart+yend)/2), xend, yend-0.2, col=colb, lwd=3)
#Panel 2: ds

minL<-0

dsa<-read.table("~/Documents/Will/Carp/Carp2022/4-dNdS/dS_a_NG.txt", head=F, sep="\t", fill=T)
dsb<-read.table("~/Documents/Will/Carp/Carp2022/4-dNdS/dS_b_NG.txt", head=F, sep="\t", fill=T)
dsab<-read.table("~/Documents/Will/Carp/Carp2022/4-dNdS/dS_ab_NG.txt", head=F, sep="\t", fill=T)

colnames(dsa)<-c("barb", "subA", "NG", "dSa", "La")
colnames(dsb)<-c("barb", "subB", "NG", "dSb", "Lb")
colnames(dsab)<-c("subA", "subB", "NG", "dSab", "Lab")

dsa<-na.omit(dsa)
dsa<-subset(dsa, La> minL)
dsb<-na.omit(dsb)
dsb<-subset(dsb, Lb> minL)
dsab<-na.omit(dsab)
dsab<-subset(dsab, Lab> minL)
head(dsa)

#plot
par(mar=c(4,4,1,1))
plot(density(dsa$dSa), col="#1F78B4", xlim=c(0,0.8), ylim=c(0,10), lwd=2, main="", xlab="dS (Nei-Gojobori)")
lines(density(dsb$dSb), col="#33A02C", lwd=2)
lines(density(dsab$dSab), col="#FB9A99", lwd=2)
#abline(v=median(dsa$dSa, na.rm=T), col="#1F78B4")
#abline(v=median(dsb$dSb, na.rm=T), col="#33A02C")
#abline(v=median(dsab$dSab, na.rm=T), col="#FB9A99")

```
