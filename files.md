# Key starting input files

## Cyprinus carpio (carp) genome assembly

We use the genome of Li et al (2021), which has the two annotated subgenomes.

Li et al (2021):
https://ngdc.cncb.ac.cn/gwh/Assembly/1093/show
ftp://download.big.ac.cn/gwh/Animals/Cyprinus_carpio_Cyprinus_carpio_genome_assembly_GWHACFI00000000/GWHACFI00000000.genome.fasta.gz


## Transcriptome assemblies / Protein sequences

**Cyprinus carpio (carp)**:

We use the published carp mRNA set (Li 2021):
https://figshare.com/articles/dataset/Sequence_information_and_functional_annotation_of_four_fish_species/13886912?file=26430659


**Puntius titteya (barb)**:

We produced our own [transcriptome assembly](https://git.ist.ac.at/bvicoso/carp_2022/-/blob/main/Transcriptome.md).

And extracted the protein sequences:
```
perl ~/scripts/GetLongestAA_v1_July2020.pl Barb.trinity.fasta.long.500bp_min_unwrapped.fasta
```

The perl script is taken from Elkewi et al (2020) and is available [here](https://github.com/Melkrewi/Schisto_project).

**Other barb and close outgroup, and goldfish (Li et al, 2021)**:

These were all obtained from: https://figshare.com/articles/dataset/Sequence_information_and_functional_annotation_of_four_fish_species/13886912?file=26430659

* Puntius_tetrazona-protein.fa
* Paracanthobrama_guichenoti-protein.fa
* Carassius_auratus-protein.fa
* Carassius_auratus-mRNA.fa

**Zebrafish**:

wget http://ftp.ensembl.org/pub/release-104/fasta/danio_rerio/pep/Danio_rerio.GRCz11.pep.all.fa.gz

We kept only the longest protein per gene:

```
cat Danio_rerio.GRCz11.pep.all.fa | perl -pi -e 's/>.*gene:/>/gi' | perl -pi -e 's/ .*//gi' | perl -pi -e 's/\n/ /gi' | perl -pi -e 's/>/\n>/gi' | sort | perl -pi -e 's/ /\n/gi' | perl -pi -e 's/^\n//gi' > Danio_rerio_cleanprot.fa
perl ~/GetLongestCDS.pl Danio_rerio_cleanprot.fa

#first > missing
perl -pi -e 's/ENSDARG00000000001.6/>ENSDARG00000000001.6/gi' Danio_rerio_cleanprot.fa.longestCDS 
mv Danio_rerio_cleanprot.fa.longestCDS Danio_rerio_Longestprot.fa
```

## RNA reads

These have been deposited here [add before publication]().
