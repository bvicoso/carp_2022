# Assess microsynteny conservation between inferred orthologs

WD: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/5-MicroSynteny

## Get files

We need the location of each gene on a genomic scaffold (inferred with blat), and the scaffold-chromosome correspondence (extracted from the genome fasta file itself):

```
ln -s ../1-Trios/1-Carp_GeneSubgenome/Ccarp_RNA_vs_genome.blat.sorted.besthit .
ln -s ../1-Trios/1-Carp_GeneSubgenome/Scaffold_chromosome.joinable .
```

Let's also get the list of homeologs:

```
ln -s ../1-Trios/3-OrthoTables/Barb_Carp_1to2_exhaust_correctChrom.txt .
```

## Merge into usable table 

First get location on chromosomes for each carp gene:

```
cat Ccarp_RNA_vs_genome.blat.sorted.besthit | awk '{print $14, $10, ($16+$17)/2}' | sort | join /dev/stdin Scaffold_chromosome.joinable | awk '{print $2, $1, $4, $3}' | sort > CarpGene_scaf_chrom_coord.joinable
```

Then merge with set of homeologs:

```
cat Barb_Carp_1to2_exhaust_correctChrom.txt | awk '{print $3, $5}' | sort | join /dev/stdin CarpGene_scaf_chrom_coord.joinable | awk '{print $2, $1, $3, $4, $5}' | sort | join /dev/stdin CarpGene_scaf_chrom_coord.joinable | awk '{print $2, $3, $4, $5, $1, $6, $7, $8}' > SubA_SubB_coordinate.txt
```

## Plot in R 

<details><summary>R commands for the plot are here.</summary>

```
synt<-read.table("~/Documents/Will/Carp2022/Microsynteny/SubA_SubB_coordinate.txt", head=F, sep=" ")
colnames(synt)<-c("subA", "scafA", "chromA", "coordA", "subB", "scafB", "chromB", "coordB")
synt$coordA<-(synt$coordA/10^6)
synt$coordB<-(synt$coordB/10^6)

head(synt)
par(mfrow=c(5,5))
par(mar=c(4,4,1,1))
par(mgp=c(1.8,0.8,0))

xlabplot<-"SubA coordinate (MB)"
ylabplot<-"SubB coordinate (MB)"
colplot<-"#1F78B4"

###chr1
subchrom<-subset(synt, chromA=="A1")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 1")

###chr2
subchrom<-subset(synt, chromA=="A2")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 2")

###chr3
subchrom<-subset(synt, chromA=="A3")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 3")

###chr4
subchrom<-subset(synt, chromA=="A4")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 4")

###chr5
subchrom<-subset(synt, chromA=="A5")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 5")

###chr6
subchrom<-subset(synt, chromA=="A6")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 6")

###chr7
subchrom<-subset(synt, chromA=="A7")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 7")

###chr8
subchrom<-subset(synt, chromA=="A8")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 8")

###chr9
subchrom<-subset(synt, chromA=="A9")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 9")

###chr10
subchrom<-subset(synt, chromA=="A10")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 10")

###chr11
subchrom<-subset(synt, chromA=="A11")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 11")

###chr12
subchrom<-subset(synt, chromA=="A12")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 12")

###chr13
subchrom<-subset(synt, chromA=="A13")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 13")

###chr14
subchrom<-subset(synt, chromA=="A14")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 14")

###chr15
subchrom<-subset(synt, chromA=="A15")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 15")

###chr16
subchrom<-subset(synt, chromA=="A16")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 16")

###chr17
subchrom<-subset(synt, chromA=="A17")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 17")

###chr18
subchrom<-subset(synt, chromA=="A18")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 18")

###chr19
subchrom<-subset(synt, chromA=="A19")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 19")

###chr20
subchrom<-subset(synt, chromA=="A20")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 20")

###chr21
subchrom<-subset(synt, chromA=="A21")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 21")

###chr22
subchrom<-subset(synt, chromA=="A22")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 22")

###chr23
subchrom<-subset(synt, chromA=="A23")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 23")

###chr24
subchrom<-subset(synt, chromA=="A24")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 24")

###chr25
subchrom<-subset(synt, chromA=="A25")
plot(subchrom$coordA, subchrom$coordB, pch=20, xlab= xlabplot, ylab= ylabplot, col= colplot, main="Chr. 25")
```

</details>

The final plot is below:

<img src="images/MicrosyntenyPlot.jpg" width=700>
