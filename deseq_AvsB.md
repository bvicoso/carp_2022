# Let's find homeologs with significant differential expression

Load counts with TXimport

```
####PICK KALLISTO RUN TO ANALYZE
dir <- ("/nfs/scistore03/vicosgrp/bvicoso//FishFun/Carp2022/3b-Sleuth_noBadSamples/3-Trios/SleuthSexBias/Carp_Spleen/")
list.files(dir)

#load necessary packages
library("tximport")
library("rhdf5") #it was already there

#prepare table with sample description
samples <- read.table(file.path(dir, "SleuthConfig.txt"), header = TRUE, sep=" ")
colnames(samples)<-c("sample", "sex")
samples$run<-gsub (" ", "", paste("KalFiles/", samples$sample))

#import kallisto data
files <- file.path(dir, samples$run, "abundance.h5")
#names(files) <- paste0("sample", 1:6)
txi.kallisto <- tximport(files, type = "kallisto", txOut = TRUE)
counttable<-txi.kallisto$counts 
colnames(counttable)<-samples$sample
head(counttable)
```

Make pairs of homeologs with either of these files:
* /nfs/scistore03/vicosgrp/bvicoso//FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_exhaust_correctChrom.txt (7147 genes)
* /nfs/scistore03/vicosgrp/bvicoso//FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_stringent_correctChrom.txt (6241 genes)

```
#load trio information
ortho<-read.table("/nfs/scistore03/vicosgrp/bvicoso//FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_exhaust_correctChrom.txt", head=F, sep=" ")

#CHANGE VARIABLE ACCORINDINLY
stringency<-"exhaust" #stringent or exhaust 
colnames(ortho)<-c("OG", "Barb", "SubA", "ChrA", "SubB", "ChrB")

#make table with counts
temp<-(merge(ortho, counttable, by.x="SubA", by.y=0))
colnames(temp)<-gsub("Carp", "SubA", colnames(temp))
big<-merge(temp, counttable, by.x="SubB", by.y=0)
colnames(big)<-gsub("Carp", "SubB", colnames(big))
```

Separate the male and female data and make table of sample info for each:

```
#separate male and female
bigF<-big[grep("_F_", colnames(big))]
bigM<-big[grep("_M_", colnames(big))]
rownames(bigM)<-paste(big$SubA, big$SubB, sep="-")
rownames(bigF)<-paste(big$SubA, big$SubB, sep="-")

sampleM<-as.data.frame(colnames(bigM))
rownames(sampleM)<-colnames(bigM)
sampleM$Status<-gsub("_.*", "", colnames(bigM))
colnames(sampleM)<-c("Samplename", "Status")


sampleF<-as.data.frame(colnames(bigF))
rownames(sampleF)<-colnames(bigF)
sampleF$Status<-gsub("_.*", "", colnames(bigF))
colnames(sampleF)<-c("Samplename", "Status")

```

Run DEseq2:

```
#######In male tissue

########create the DESeq object
library("DESeq2") # existed
		# countData is the matrix containing the counts
		# sampletable is the sample sheet / metadata we created
		# design is how we wish to model the data: what we want to measure here is the difference between the treatment times
count_matrix<-round(bigM, digits = 0)
se_star_matrix <- DESeqDataSetFromMatrix(countData = count_matrix , colData = sampleM, design = ~ Status)

#########optional: filter out genes with a sum of counts below 10
#se_star_matrix <- se_star_matrix[rowSums(counts(se_star_matrix)) > 10, ]
dds <- DESeq(se_star_matrix)
resultsNames(dds) # lists the coefficients, so you know what to write for "coef=" in the next command

#####Standard differential expression analysis
subbiasedM <- results(dds, name="Status_SubB_vs_SubA", pAdjustMethod = "BH", alpha=0.05)

#######In female tissue

########create the DESeq object
library("DESeq2") # existed
		# countData is the matrix containing the counts
		# sampletable is the sample sheet / metadata we created
		# design is how we wish to model the data: what we want to measure here is the difference between the treatment times
count_matrix<-round(bigF, digits = 0)
se_star_matrix <- DESeqDataSetFromMatrix(countData = count_matrix , colData = sampleF, design = ~ Status)

#########optional: filter out genes with a sum of counts below 10
#se_star_matrix <- se_star_matrix[rowSums(counts(se_star_matrix)) > 10, ]
dds <- DESeq(se_star_matrix)
resultsNames(dds) # lists the coefficients, so you know what to write for "coef=" in the next command

#####Standard differential expression analysis
subbiasedF <- results(dds, name="Status_SubB_vs_SubA", pAdjustMethod = "BH", alpha=0.05)
```

Output the results:

```
final<-merge(as.data.frame(subbiasedM), as.data.frame(subbiasedF), by.x=0, by.y=0)[,c(1,3,7,9,13)]
colnames(final)<-c("Genes", "Log2(B/A)_M", "padj_M", "Log2(B/A)_F", "padj_F")
head(final)

tissuename<-gsub("\\/", "", gsub(".*SleuthSexBias\\/", "", dir))
write.table(final, file.path(dir, paste(tissuename, "_", stringency, "_AvsBdeseq.txt", sep="")))
```
