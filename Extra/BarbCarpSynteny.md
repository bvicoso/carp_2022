# Let's make table of chromosomal correspondence between barb and carp

In: /nfs/scistore18/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/2-Orthogroups/BLAT_Barb


## Get tiger barb genome (Puntigrus tetrazona) and our transcriptome

https://ngdc.cncb.ac.cn/gwh/Assembly/1094/show

```
nohup wget ftp://download.cncb.ac.cn/gwh/Animals/Puntigrus_tetrazona_the_genome_of_Puntius_tetrazoa_GWHACFJ00000000/GWHACFJ00000000.genome.fasta.gz

gzip -d GWHACFJ00000000.genome.fasta.gz
```

Make table of P. tetrazona scaffolds and their corresponding chromosome:
```
grep '>' GWHACFJ00000000.genome.fasta | perl -pi -e 's/>//gi' | perl -pi -e 's/\tOriSeqID=/ /gi' | perl -pi -e 's/\t.*//gi' | sort > Ptetra_scaf_chr.joinable
```

Get carp chromosomal location of homeologs:

```
ln ~/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_stringent_correctChrom.txt .
```

## BLAT

```
module load pblat
pblat -q=dnax -t=dnax -threads=30 GWHACFJ00000000.genome.fasta Barb_orthoRNA.fa barbrna_vs_barbgenome.blat
```

Then get best hit:

```
sort -k 10 barbrna_vs_barbgenome.blat > barbrna_vs_barbgenome.blat.sorted
perl ~/2-besthitblat.pl barbrna_vs_barbgenome.blat.sorted 
```

## Merge all files

We select carp homeologs for which the barb homolog mapped to a P. tetrazona chromosome (i.e. we filter out unmapped scaffolds).

```
cat barbrna_vs_barbgenome.blat.sorted.besthit | awk '{print $14, $10}' | sort | join /dev/stdin Ptetra_scaf_chr.joinable | awk '{print $2, $3}' | sort > PuntiusRNA_PtetraChrom.joinable

cat Barb_Carp_1to2_stringent_correctChrom.txt | awk '{print $2, $6}' | sort | join /dev/stdin PuntiusRNA_PtetraChrom.joinable | grep -v 'S[0-9]*' > PuntiusRNA_carpChr_PtetraChr.txt
```

We can also have a quick look at counts:
```
cat Barb_Carp_1to2_stringent_correctChrom.txt | awk '{print $2, $6}' | sort | join /dev/stdin PuntiusRNA_PtetraChrom.joinable | awk '{print $2, $3}' | grep -v 'S[0-9]*' | sort | uniq -c | sort -rn | awk '{print $2, $3, $1}' > CarpChr_BarbChr_counts.txt
```

Gives a table of counts that looks like:

CarpChr | PtetrazonaChr | GeneCount
--- | --- | ---
B7 | B7 | 341
B5 | B5 | 333
B3 | B3 | 309
B16 | B16 | 301

--> 5684/5833 genes (97%) are assigned to the same chromosomal element.


## Plot in R 

```
synt<-read.table("~/Documents/Will/Carp2022/Revisions/BarbSynteny/PuntiusRNA_carpChr_PtetraChr.txt", head=F, sep=" ")
colnames(synt)<-c("Transcript", "CarpChr", "PtetraChr")

#synt$CarpChr<-gsub("B", "", synt$CarpChr)
#synt<-synt[order(as.numeric(gsub("B", "", synt$CarpChr))),] 

final<-table(synt[,-1])

library(gplots)

heatmap.2(final, col= colorRampPalette(c("blue", "white", "red", "firebrick4"))(15), scale="none", margins = c(4,4), xlab="P.tetrazona Chromosome", ylab="Carp chromosome", trace="none", dendrogram="none", Rowv = F, Colv=F, key=T, lmat=( matrix(c(1,1,1, 2,4,3), nrow=2, byrow=TRUE)  ), lwid=c(1.5, 4, 2 ), lhei=c(4.2, 1 ), key.title=NA, key.xlab="Gene Number", cellnote = final)
```

The figure is here:

<img src="images/BarbCarpSynteny_heatmap.jpg" width=500>
