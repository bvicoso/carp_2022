# Get 1:2 and 1:1 sets of homologs between zebrafish and carp

Following Li et al. (2021), we use Orthofinder with:
- the Li et al. (2021) set of carp proteins, and some of their other species
- the translated version of our Barb collapsed transcriptome
- the published zebrafish genome

## Summary

We use a stringent and an exhaustive approach to extract 1:1 and 1:2 orthologs from the orthofinder result, but only the exhaustive was used in further analyses. In the case of 1:2, we keep only trios for which the two carp homeologs are on two homeologous chromosomes (e.g. one on A5 and the other on B5).

**Stringent:** Orthogroups that have 1 gene in barb and 1 (4133) or 2 (6241) in carp were selected. The resulting tables are at:
```
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to1_stringent.txt
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_stringent_correctChrom.txt
```

**Exaustive:** Orthogroups that have 1 gene in barb and 1 (6623) or 2 (7147) in carp were selected. The resulting tables are at:
```
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to1_exhaust.txt
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_exhaust_correctChrom.txt
```

## 1. Map carp transcripts to the two subgenomes

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome


## 2. Run Orthofinder

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/2b-Orthofinder_ZF


## 3. Make final lists

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables

### 3.a Exhaustive list (set of 1:1 and 1:2 orthologs)

**First 1:1 orthologs:**

```
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep -v ', CAF' |  awk '{print $3, $1, $2}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable |  awk '{print $2, $3, $1, $5, $5}' | awk -F' ' -vOFS=' ' '{ gsub("[0-9]", "", $5) ; print }' > Barb_Carp_1to1_exhaust.txt
```

The file looks like (orthogroup, barb, carp, carp_chrom, carp_subgenome):

```
OG0001995 TRINITY_DN14237_c0_g1_i1 CAFS_CC_T_00000006 A19 A
OG0014048 TRINITY_DN3899_c0_g2_i2 CAFS_CC_T_00000036 A19 A
OG0014151 TRINITY_DN103823_c0_g1_i1 CAFS_CC_T_00000061 A19 A
OG0018466 TRINITY_DN6686_c0_g1_i1 CAFS_CC_T_00000099 A19 A
```

**Now 1:2 orthologs:**

To simply see all 1:2 orthologs:

```
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep ',*,' | awk '($5 !~ /CAFS/)' | perl -pi -e 's/, /\t/gi' | head
```

Now with gene locations, so we can pick only those that have one subA and one subB duplicate:

```
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep ',*,' | awk '($5 !~ /CAFS/)' | perl -pi -e 's/, /\t/gi' | awk '{print $3, $1, $2, $4}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $4, $2, $3, $1, $6}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $2, $3, $4, $5, $1, $7}' | head
```

Make final list:

```
#first print those that have subA ortholog first
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep ',*,' | awk '($5 !~ /CAFS/)' | perl -pi -e 's/, /\t/gi' | awk '{print $3, $1, $2, $4}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $4, $2, $3, $1, $6}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $2, $3, $4, $5, $1, $7}' | awk '(($4 ~/A/) && ($6 ~/B/))' > Barb_Carp_1to2_exhaust.txt

#then those that are the other way around
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep ',*,' | awk '($5 !~ /CAFS/)' | perl -pi -e 's/, /\t/gi' | awk '{print $3, $1, $2, $4}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $4, $2, $3, $1, $6}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $2, $3, $4, $5, $1, $7}' | awk '(($4 ~/B/) && ($6 ~/A/))' | awk '{print $1, $2, $5, $6, $3, $4}' >> Barb_Carp_1to2_exhaust.txt
```

The file looks like (orthogroup, barb, carp_A, chrom_carpA, carp_B, chrom_carpB):

```
OG0006008 TRINITY_DN29185_c0_g1_i7 CAFS_CC_T_00043494 A13 CAFS_CC_T_00003855 B13
OG0006072 TRINITY_DN306_c3_g1_i1 CAFS_CC_T_00043498 A13 CAFS_CC_T_00003882 B13
OG0005139 TRINITY_DN103491_c0_g1_i1 CAFS_CC_T_00214005 A15 CAFS_CC_T_00004494 B15
OG0007325 TRINITY_DN25_c2_g1_i5 CAFS_CC_T_00118891 A8 CAFS_CC_T_00007999 B8
OG0004752 TRINITY_DN3509_c0_g1_i1 CAFS_CC_T_00118878 A8 CAFS_CC_T_00008028 B8
```

7147 out of 7241 are in pairs of A and B with the same number, let's select them:

```
cat Barb_Carp_1to2_exhaust.txt | perl -pi -e 's/[AB]//gi' | awk '($4==$6)' | awk '{print $2}' | grep -f /dev/stdin Barb_Carp_1to2_exhaust.txt > Barb_Carp_1to2_exhaust_correctChrom.txt
```
