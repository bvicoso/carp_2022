# Checks and key results from KalSleuth

## Correlations and samples to remove

### Get correlations

Get correlations and heatmaps (do for each tissue/species):

<details><summary>R code</summary>

```
fish<-read.table("~/Documents/Will/Carp/Carp2022/3-Sleuth/2-Orthogroups/SleuthSexBias/Carp_Spleen/Carp_Spleen_SexBiasResults.txt", head=T, sep=" ")
cor(fish[,13:18], method="spearman")
ddt_vals<-fish[,13:18]

#draw boxplot for each column of ddt_vals, after applying a log2 transformation
boxplot(log2(ddt_vals), outline=F, notch=F, col=rainbow(6), ylab="Log2(TPM)") 

##Heatmap
#store list of colors in variable "crazycols" 
crazycols<-c("pink", "pink", " pink", " lightgreen", " lightgreen", " lightgreen")
 
#tell R to use the library gplots
library(gplots)

#draw the heatmap
heatmap.2(cor(ddt_vals, method="spearman"), col= colorRampPalette(c("blue", "white", "red", "firebrick4"))(15), ColSideColors=crazycols, RowSideColors = crazycols, scale="none", symm=T, margins = c(15,15), key=T, trace="none")

#add legend
legend("topright",      # location of the legend on the heatmap plot
    legend = c("female", "male."), # category labels
    col = c("pink", "lightgreen"),  # color key
    lty= 1,             # line style
    lwd = 10            # line width)
)
```
</details>

All the correlations are [here](https://git.ist.ac.at/bvicoso/carp_2022/-/blob/main/KalSleuth_correlations.md).

### Summary of correlations

Some key points:

* Samples Carp_M_Liver_1, Carp_M_Heart_3, Carp_F_Gonad_6 have low correlations (<0.8) with other replicates, and should be removed.
    --> We reran Sleuth without them

* Only the gonads cluster consistently by sex, with replicates having correlations ~0.9, but only ~0.7 to the other sex (using transcripts in orthogroups as the reference, see below). In other tissues, correlations are ~0.9 or higher even between male and female samples, and do not cluster by sex.

<img src="/images/Carp_Gonad.jpg" width="400">
<img src="/images/Barb_Gonad_heatmap.jpg" width="400">

* Correlations between samples are lower when we use all transcripts than when we use only transcripts in orthogroups. 
--> It is sensible to use only transcripts in orthogroups.


## Number of sex-biased genes

<details><summary>R code for counting</summary>

```
fish<-read.table("~/Documents/Will/Carp/Carp2022/3-Sleuth/1-AllTranscripts/SleuthSexBias/Barb_Brain/Barb_Brain_SexBiasResults.txt", head=T, sep=" ")
dim(subset(fish, qval<0.05))
fish$fmean<-rowMeans(fish[,13:15])
fish$mmean<-rowMeans(fish[,16:18])
fb<-subset(fish, rowMeans(fish[,13:15])>rowMeans(fish[,16:18]) & qval<0.05)
mb<-subset(fish, rowMeans(fish[,13:15])<rowMeans(fish[,16:18]) & qval<0.05)
dim(fb)
dim(mb)
```

</details>

1. **All transcripts as reference**

Tissue | Sex-Biased | Female-Biased | Male-Biased
--- | --- | --- | ---
Barb_Brain | 2 | 0 | 2
Barb_Gonad | 32619 | 9668 | 22951
Barb_Heart | 0 | 0 | 0
Barb_Liver | 10 | 8 | 2
Barb_Spleen | 4 | 0 | 4
Carp_Brain | 0 | 0 | 0
Carp_Gonad | 3862 | 1130 | 2732
Carp_Heart | 0 | 0 | 0
Carp_Liver | 0 | 0 | 0
Carp_Spleen | 1 | 0 | 1

2. **Only transcripts in orthogroups as reference** (in parenthesis: carp numbers when Sleuth is run without "bad" samples)

Tissue | Sex-Biased | Female-Biased | Male-Biased
--- | --- | --- | ---
Barb_Brain | 1 | 0 | 1
Barb_Gonad | 19186 | 7211 | 11975
Barb_Heart | 0 | 0 | 0
Barb_Liver | 10 | 7 | 3
Barb_Spleen | 4 | 0 | 4
Carp_Brain | 0 (0) | 0 (0) | 0 (0)
Carp_Gonad | 3712 (30481) | 1116 (11208) | 2596 (19273)
Carp_Heart | 0 (0) | 0 (0) | 0 (0)
Carp_Liver | 0 (0) | 0 (0) | 0 (0)
Carp_Spleen | 1 (1) | 0 (0) | 1 (1)

3. **Only 1:2 trios**

Tissue | Sex-Biased | Female-Biased | Male-Biased
--- | --- | --- | ---
Barb_Brain | 0 | 0 | 0
Barb_Gonad | 6782 | 3005 | 3777
Barb_Heart | 0 | 0 | 0
Barb_Liver | 1 | 0 | 1
Barb_Spleen | 0 | 0 | 0
Carp_Brain | 0 | 0 | 0
Carp_Gonad | 1357 | 524 | 833
Carp_Heart | 0 | 0 | 0
Carp_Liver | 0 | 0 | 0
Carp_Spleen | 0 | 0 | 0

--> Independent of the reference transcriptome, there seems to be a lot of sex bias in the gonad but almost nothing elsewhere. While Sleuth is a bit stringent, this does suggest that somatic tissues of these two species are largely monomorphic at the gene expression level. 

--> Using transcripts in orthogroups seems to not lead to a major loss of sex-biased genes, so again this seems like a reasonable course of action to me. 

--> Removing Carp_F_Gonad_6 leads do a dramatic increase in the number of sex-biased genes in the carp gonad. 

