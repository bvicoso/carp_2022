#!/usr/bin/perl

#modules to load
#module load kallisto (version 0.43.1)
#usage: perl Kallisto_runner.pl ReadsList.txt
#ReadsList.txt is a tab-separated file that has the following columns: species, sex, tissue, sample number, read file location.


my $list = $ARGV[0];
open (INPUT, "$list") or die "can't find $input";

while ($run = <INPUT>) 
	{
	chomp $run;
	($species, $sex, $tissue, $number, $files) = split(/\t/, $run);
	print "$run\n";
	$filename = "$species\_$sex\_$tissue\_$number";
	print "$filename Reads/$files\n";

#        system "java -jar $PICARD SamToFastq I=Reads/$files F=temp1.fq F2=temp2.fq";
#	system "java -jar $PICARD SamToFastq -I temp.bam -F temp1.fq -F2 temp2.fq";
	system "java -jar /nfs/scistore03/vicosgrp/bvicoso/scripts/picard.jar SamToFastq -I Reads/$files -F temp1.fq -F2 temp2.fq";

	if ($species =~ /Barb/)
		{
               #Use barb transcriptome for barb RNA-seq

                system "kallisto quant -t 16 -i 1-AllTranscripts/barb.idx -o 1-AllTranscripts/KalFiles/$filename -b 100 temp1.fq temp2.fq";
                system "kallisto quant -t 16 -i 2-Orthogroups/barb.idx -o 2-Orthogroups/KalFiles/$filename -b 100 temp1.fq temp2.fq";
                system "kallisto quant -t 16 -i 3-Trios/barb.idx -o 3-Trios/KalFiles/$filename -b 100 temp1.fq temp2.fq";

		}	
	elsif ( $species =~ /Carp/)
		{
		#Use carp transcriptome for carp RNA-seq		
		system "kallisto quant -t 16 -i 1-AllTranscripts/carp.idx -o 1-AllTranscripts/KalFiles/$filename -b 100 temp1.fq temp2.fq";
		system "kallisto quant -t 16 -i 2-Orthogroups/carp.idx -o 2-Orthogroups/KalFiles/$filename -b 100 temp1.fq temp2.fq";
		system "kallisto quant -t 16 -i 3-Trios/carp.idx -o 3-Trios/KalFiles/$filename -b 100 temp1.fq temp2.fq";

		}
	else
		{
		print "weird file: $files";
		}
	
	system "rm temp.bam";
        system "rm temp1.fq temp2.fq";
print "removed temp files\n";
	}
