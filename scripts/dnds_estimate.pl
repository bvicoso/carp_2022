#!/usr/bin/perl

#usage: perl dnds_estimate.pl homolog_list.txt speciesa.fasta speciesb.fasta output_directory

#list: space-separated list of pairs of homologs, either subA and subB, or carp and barb
my $list = $ARGV[0];

#fasta for species a (first column in the list)
my $a = $ARGV[1];

#fasta for species b (second column in the list)
my $b = $ARGV[2];

#pick name for output directory
my $out = $ARGV[3];
system "mkdir $out";


###########Make individual fasta files for each pair of genes in the list (either both from carp or one from carp and one from barb

open (LIST, "$list") or die "where are the  best hits?"; 
system "mkdir $out/fasta";

while ($line = <LIST>) 
	{
	($aname, $bname)=split(/\s/, $line);
	open (TEMP1, ">$out/temp1");
	print TEMP1 "$aname\n";
	system "/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk subseq $a $out/temp1 >> $out/fasta/$aname.fasta";
	open (TEMP2, ">$out/temp2");
	print TEMP2 "$bname\n";
	system "/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk subseq $b $out/temp2 >> $out/fasta/$aname.fasta";
	close (TEMP1);
	close (TEMP2);

	}



##########make alignments, axt file and run KaKs_calculator

open (LIST, "$list") or die "where are the  best hits?"; 
system "module load gblocks/0.91b";

system "mkdir $out/translatorX";
system "mkdir $out/KaKs";

while ($line = <LIST>) 
	{
	($aname, $bname)=split(/\s/, $line);
	system "perl -pi -e \'s/ \.\*//gi\' $out/fasta/$aname.fasta";
	system "perl /nfs/scistore03/vicosgrp/bvicoso/translatorx_vLocal.pl -i $out/fasta/$aname.fasta -o $out/translatorX/$aname.tx -t T -g \"-s\"";
	system "perl /nfs/scistore03/vicosgrp/bvicoso/parseFastaIntoAXT.pl $out/translatorX/$aname.tx.nt_cleanali.fasta";
	system " /nfs/scistore03/vicosgrp/bvicoso/KaKs_Calculator2.0/bin/Linux/KaKs_Calculator -i $out/translatorX/$aname.tx.nt_cleanali.fasta.axt -o $out/KaKs/$aname.kaks -m NG -m YN";

	}

