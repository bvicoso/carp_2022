# Correlations between TPMs of different samples

## 1. All transcripts

## 2. Only transcripts in orthogroups

<details><summary>Click here for tables.</summary>

```
	Carp_F_Spleen_1	Carp_F_Spleen_2	Carp_F_Spleen_6	Carp_M_Spleen_1	Carp_M_Spleen_3	Carp_M_Spleen_6
Carp_F_Spleen_1	1.00	0.96	0.92	0.92	0.92	0.92
Carp_F_Spleen_2	0.96	1.00	0.93	0.93	0.93	0.93
Carp_F_Spleen_6	0.92	0.93	1.00	0.94	0.93	0.94
Carp_M_Spleen_1	0.92	0.93	0.94	1.00	0.95	0.95
Carp_M_Spleen_3	0.92	0.93	0.93	0.95	1.00	0.94
Carp_M_Spleen_6	0.92	0.93	0.94	0.95	0.94	1.00
```

```
	Carp_F_Liver_1	Carp_F_Liver_2	Carp_F_Liver_6	Carp_M_Liver_1	Carp_M_Liver_3	Carp_M_Liver_6
Carp_F_Liver_1	1.00	0.94	0.90	0.76	0.90	0.91
Carp_F_Liver_2	0.94	1.00	0.91	0.75	0.90	0.91
Carp_F_Liver_6	0.90	0.91	1.00	0.77	0.92	0.92
Carp_M_Liver_1	0.76	0.75	0.77	1.00	0.76	0.77
Carp_M_Liver_3	0.90	0.90	0.92	0.76	1.00	0.93
Carp_M_Liver_6	0.91	0.91	0.92	0.77	0.93	1.00
```

```    					
	Carp_F_Heart_1	Carp_F_Heart_2	Carp_F_Heart_6	Carp_M_Heart_1	Carp_M_Heart_3	Carp_M_Heart_6
Carp_F_Heart_1	1.00	0.93	0.90	0.90	0.76	0.90
Carp_F_Heart_2	0.93	1.00	0.92	0.92	0.76	0.92
Carp_F_Heart_6	0.90	0.92	1.00	0.94	0.77	0.94
Carp_M_Heart_1	0.90	0.92	0.94	1.00	0.80	0.94
Carp_M_Heart_3	0.76	0.76	0.77	0.80	1.00	0.77
Carp_M_Heart_6	0.90	0.92	0.94	0.94	0.77	1.00
```

```
	Carp_F_Gonad_1	Carp_F_Gonad_2	Carp_F_Gonad_6	Carp_M_Gonad_1	Carp_M_Gonad_3	Carp_M_Gonad_6
Carp_F_Gonad_1	1.00	0.95	0.64	0.76	0.73	0.73
Carp_F_Gonad_2	0.95	1.00	0.65	0.77	0.74	0.73
Carp_F_Gonad_6	0.64	0.65	1.00	0.71	0.65	0.66
Carp_M_Gonad_1	0.76	0.77	0.71	1.00	0.93	0.93
Carp_M_Gonad_3	0.73	0.74	0.65	0.93	1.00	0.95
Carp_M_Gonad_6	0.73	0.73	0.66	0.93	0.95	1.00
```

```
	Carp_F_Brain_1	Carp_F_Brain_2	Carp_F_Brain_6	Carp_M_Brain_1	Carp_M_Brain_3	Carp_M_Brain_6
Carp_F_Brain_1	1.00	0.96	0.90	0.92	0.91	0.93
Carp_F_Brain_2	0.96	1.00	0.91	0.93	0.92	0.93
Carp_F_Brain_6	0.90	0.91	1.00	0.94	0.93	0.94
Carp_M_Brain_1	0.92	0.93	0.94	1.00	0.96	0.95
Carp_M_Brain_3	0.91	0.92	0.93	0.96	1.00	0.95
Carp_M_Brain_6	0.93	0.93	0.94	0.95	0.95	1.00
```

```
	Barb_F_Spleen_21	Barb_F_Spleen_24	Barb_F_Spleen_25	Barb_M_Spleen_23	Barb_M_Spleen_24	Barb_M_Spleen_25
Barb_F_Spleen_21	1.00	0.92	0.92	0.90	0.91	0.92
Barb_F_Spleen_24	0.92	1.00	0.96	0.95	0.94	0.95
Barb_F_Spleen_25	0.92	0.96	1.00	0.96	0.96	0.96
Barb_M_Spleen_23	0.90	0.95	0.96	1.00	0.97	0.96
Barb_M_Spleen_24	0.91	0.94	0.96	0.97	1.00	0.96
Barb_M_Spleen_25	0.92	0.95	0.96	0.96	0.96	1.00
```

```
	Barb_F_Liver_21	Barb_F_Liver_24	Barb_F_Liver_25	Barb_M_Liver_23	Barb_M_Liver_24	Barb_M_Liver_25
Barb_F_Liver_21	1.00	0.92	0.92	0.91	0.90	0.91
Barb_F_Liver_24	0.92	1.00	0.93	0.92	0.92	0.92
Barb_F_Liver_25	0.92	0.93	1.00	0.94	0.95	0.95
Barb_M_Liver_23	0.91	0.92	0.94	1.00	0.95	0.95
Barb_M_Liver_24	0.90	0.92	0.95	0.95	1.00	0.96
Barb_M_Liver_25	0.91	0.92	0.95	0.95	0.96	1.00
```						

```
	Barb_F_Heart_21	Barb_F_Heart_24	Barb_F_Heart_25	Barb_M_Heart_23	Barb_M_Heart_24	Barb_M_Heart_25
Barb_F_Heart_21	1.00	0.92	0.94	0.92	0.93	0.93
Barb_F_Heart_24	0.92	1.00	0.93	0.96	0.93	0.93
Barb_F_Heart_25	0.94	0.93	1.00	0.93	0.96	0.95
Barb_M_Heart_23	0.92	0.96	0.93	1.00	0.93	0.93
Barb_M_Heart_24	0.93	0.93	0.96	0.93	1.00	0.96
Barb_M_Heart_25	0.93	0.93	0.95	0.93	0.96	1.00
```

```						
	Barb_F_Gonad_21	Barb_F_Gonad_24	Barb_F_Gonad_25	Barb_M_Gonad_23	Barb_M_Gonad_24	Barb_M_Gonad_25
Barb_F_Gonad_21	1.00	0.95	0.96	0.69	0.70	0.71
Barb_F_Gonad_24	0.95	1.00	0.98	0.70	0.71	0.72
Barb_F_Gonad_25	0.96	0.98	1.00	0.71	0.72	0.72
Barb_M_Gonad_23	0.69	0.70	0.71	1.00	0.97	0.97
Barb_M_Gonad_24	0.70	0.71	0.72	0.97	1.00	0.98
Barb_M_Gonad_25	0.71	0.72	0.72	0.97	0.98	1.00
```

```
	Barb_F_Brain_21	Barb_F_Brain_24	Barb_F_Brain_25	Barb_M_Brain_23	Barb_M_Brain_24	Barb_M_Brain_25
Barb_F_Brain_21	1.00	0.92	0.95	0.95	0.93	0.94
Barb_F_Brain_24	0.92	1.00	0.93	0.93	0.91	0.92
Barb_F_Brain_25	0.95	0.93	1.00	0.97	0.97	0.97
Barb_M_Brain_23	0.95	0.93	0.97	1.00	0.97	0.97
Barb_M_Brain_24	0.93	0.91	0.97	0.97	1.00	0.97
Barb_M_Brain_25	0.94	0.92	0.97	0.97	0.97	1.00
```

</details>

## 3. Only 1:2 transcripts
