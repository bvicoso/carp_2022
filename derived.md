# What are the current patterns of expression of subA/subB, and tetra/diploid? How do they compare to ancestral expression?


## Load data

Let's merge:
- the list of barb genes that have either 1 or 2 orthologs in carp
- patterns of barb and carp expression in different tissues

<details><summary>R code</summary>

```
#set working directory to directory where "carp2022_upload" is
setwd("~/Documents/Will/Carp/Carp2022/")
library("dplyr")                          # Load dplyr package

 #get set of 1:1 orthologs
orth1<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to1_exhaust.txt")
#get set of 1:2 orthologs
orth2<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to2_exhaust_correctChrom.txt", head=F, sep=" ")
colnames(orth1)<-c("orthogroup", "barbgene", "carpgene", "chr", "subgenome")
colnames(orth2)<-c("orthogroup", "barbgene", "carpA", "chrA", "carpB", "chrB")

#Get expression
bbrain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Brain_DEseq2.txt", head=T, sep=" ")
bgonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Gonad_DEseq2.txt", head=T, sep=" ")
bheart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Heart_DEseq2.txt", head=T, sep=" ")
bliver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Liver_DEseq2.txt", head=T, sep=" ")
bspleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Spleen_DEseq2.txt", head=T, sep=" ")
brain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Brain_DEseq2.txt", head=T, sep=" ")
gonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Gonad_DEseq2.txt", head=T, sep=" ")
heart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Heart_DEseq2.txt", head=T, sep=" ")
liver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Liver_DEseq2.txt", head=T, sep=" ")
spleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Spleen_DEseq2.txt", head=T, sep=" ")

#make brain tables

brain1 <- inner_join(orth1[,1:2], bbrain, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]     
brain1a <- inner_join(orth1[,c(1,3)], brain, by = c("carpgene" = "Gene"))[,c(2,4,5,12,13)]

brain2 <- inner_join(orth2[,1:2], bbrain, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
brain2a <- inner_join(orth2[,c(1,3)], brain, by = c("carpA" = "Gene"))[,c(2,4,5,12,13)]
brain2b <- inner_join(orth2[,c(1,5)], brain, by = c("carpB" = "Gene"))[,c(2,4,5,12,13)]

#make gonad tables

gonad1 <- inner_join(orth1[,1:2], bgonad, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
gonad1a <- inner_join(orth1[,c(1,3)], gonad, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

gonad2 <- inner_join(orth2[,1:2], bgonad, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
gonad2a <- inner_join(orth2[,c(1,3)], gonad, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
gonad2b <- inner_join(orth2[,c(1,5)], gonad, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]

#make heart tables

heart1<-inner_join(orth1[,1:2], bheart, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
heart1a<-inner_join(orth1[,c(1,3)], heart, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

heart2<-inner_join(orth2[,1:2], bheart, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
heart2a<-inner_join(orth2[,c(1,3)], heart, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
heart2b<-inner_join(orth2[,c(1,5)], heart, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]
#make liver tables
liver1<-inner_join(orth1[,1:2], bliver, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
liver1a<-inner_join(orth1[,c(1,3)], liver, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

liver2<-inner_join(orth2[,1:2], bliver, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
liver2a<-inner_join(orth2[,c(1,3)], liver, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
liver2b<-inner_join(orth2[,c(1,5)], liver, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]
#make spleen tables
spleen1<-inner_join(orth1[,1:2], bspleen, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
spleen1a<-inner_join(orth1[,c(1,3)], spleen, by = c("carpgene" = "Gene"))[,c(2,4,5,12,13)]

spleen2<-inner_join(orth2[,1:2], bspleen, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
spleen2a<-inner_join(orth2[,c(1,3)], spleen, by = c("carpA" = "Gene"))[,c(2,4,5,12,13)]
spleen2b<-inner_join(orth2[,c(1,5)], spleen, by = c("carpB" = "Gene"))[,c(2,4,5,12,13)]

```

</details>

## Correlations between all tissues: the two species and subgenomes are well correlated, brain and gonads stand out

<details>

```
all<-(cbind(brain2$meanF, brain2a$meanF, brain2b$meanF, gonad2$meanF, gonad2a$meanF, gonad2b$meanF, heart2$meanF, heart2a$meanF, heart2b$meanF,liver2$meanF, liver2a$meanF, liver2b$meanF, spleen2$meanF, spleen2a$meanF, spleen2b$meanF, brain2$meanM, brain2a$meanM, brain2b$meanM, gonad2$meanM, gonad2a$meanM, gonad2b$meanM, heart2$meanM, heart2a$meanM, heart2b$meanM,liver2$meanM, liver2a$meanM, liver2b$meanM, spleen2$meanM, spleen2a$meanM, spleen2b$meanM))
colnames(all)<-c("barb_brainF", "subA_brainF", "subB_brainF", "barb_gonadF", "subA_gonadF", "subB_gonadF", "barb_heartF", "subA_heartF", "subB_heartF",  "barb_liverF", "subA_liverF", "subB_liverF",  "barb_spleenF", "subA_spleenF", "subB_spleenF", "barb_brainM", "subA_brainM", "subB_brainM", "barb_gonadM", "subA_gonadM", "subB_gonadM", "barb_heartM", "subA_heartM", "subB_heartM",  "barb_liverM", "subA_liverM",  "subB_liverM",  "barb_spleenM", "subA_spleenM", "subB_spleenM")

###Normalize
bolFMat<-as.matrix(all, nrow = nrow(all), ncol = ncol(all))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(all)
colnames(temp2)<-colnames(all)
all <-temp2


##Heatmap
#store list of colors in variable "crazycols" 
crazycols<-c("pink", "pink", " pink", "pink", " pink", "pink", "pink", " pink", "pink", " pink", "pink", "pink", " pink", "pink", " pink", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen")
gencols<-c("#FF7F00", "#A6CEE3", "#B2DF8A", "#FF7F00", "#A6CEE3", "#B2DF8A", "#FF7F00", "#A6CEE3", "#B2DF8A", "#FF7F00", "#A6CEE3", "#B2DF8A", "#FF7F00", "#A6CEE3", "#B2DF8A", "#FF7F00", "#A6CEE3", "#B2DF8A", "#FF7F00", "#A6CEE3", "#B2DF8A", "#FF7F00", "#A6CEE3", "#B2DF8A", "#FF7F00", "#A6CEE3", "#B2DF8A", "#FF7F00", "#A6CEE3", "#B2DF8A")

#tell R to use the library gplots
library(gplots)

#draw the heatmap
heatmap.2(cor(all, method="spearman"), col= colorRampPalette(c("blue", "white", "red", "firebrick4"))(15), ColSideColors=crazycols, RowSideColors = gencols, scale="none", symm=T, margins = c(15,15), key=T, trace="none")

#add legend
legend("topright",      # location of the legend on the heatmap plot
    legend = c("female", "male."), # category labels
    col = c("pink", "lightgreen"),  # color key
    lty= 1,             # line style
    lwd = 10            # line width)
)

###table of correlations
cor(all, method="spearman")
```

</details>

<img src="/images/alltissues_heatmap.jpg" width=700>

**Figure 1:** The Spearman correlation between the normalized expression (averaged accross replicates) of all tissues of barb and carp.

<details><summary>Sanity check! What does this look like using the expression values of Li et al (2021)?</summary>

We can use the TPM values provided in their supplementary material (sheet Fig. 4b-Common carp in the excel file 41588_2021_933_MOESM4_ESM.xlsx) and plot a heatmap of correlations. First we save it as a tab-separated file, then use as input.

```
published<-read.table("~/Documents/Will/Carp/Carp2022/SanityChecks/PublishedTPM_edited.txt", head=T, sep="\t")
head(published)
pubraw<-published[,c(3,4,7,8,11,12,15,16,19,20,23,24,27,28,31,32,35,36)]
head(pubraw)

###Normalize
bolFMat<-as.matrix(pubraw, nrow = nrow(pubraw), ncol = ncol(pubraw))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(pubraw)
colnames(temp2)<-colnames(pubraw)
pubraw <-temp2
##Heatmap
#store list of colors in variable "crazycols" 
crazycols<-c("blue","blue","blue","blue","blue","blue","blue","blue","blue","blue","blue","blue","blue","blue","blue","blue","blue","blue")
#tell R to use the library gplots
library(gplots)

#draw the heatmap
heatmap.2(cor(pubraw, method="spearman"), col= colorRampPalette(c("blue", "white", "red", "firebrick4"))(15), ColSideColors=crazycols, RowSideColors = crazycols, scale="none", symm=T, margins = c(15,15), key=T, trace="none")
###table of correlations
cor(pubraw, method="spearman")
```

--> some tissues cluster by subgenome (liver, kidney, spleen) and others by tissue (brain, muscle, heart), but there are positive correlations for homeologs for all, similar to ours. 

<img src="/images/published_heatmap.jpg" width=400>

Since we're running sanity checks, here is a quick one to verify the correspondence of our homeologs and the published ones:

```
compare<-(merge(orth2[,c(3,5)], published[,c(1,2)], by.x="carpA", by.y="subA"))
compare$b1<-gsub("CAFS_CC_T_", "", compare$carpB)
compare$b2<-gsub("CAFS_CC_T_", "", compare$subB)
table(as.numeric(compare$b1)-as.numeric(compare$b2))
subset(compare, (as.numeric(compare$b1)-as.numeric(compare$b2))!=0)
```
Of the 1356 subA genes that we both assigned to a pair, 1354 have the same SubB homeolog, and 2 are different.

</details>

## Expression of SubA, SubB, and rediploidized genes

Let's start with plotting the expression, after normalizing it by the corresponding barb expression (as we know that diploid and tetraploid genes had different patterns of ancestral expression).

<details><summary>R code for expression plot - female tissues</summary>

```
par(mfrow=c(2,5))
par(mar=c(3,3,3,0.3))
boxcols<-c("#FF7F00", "#A6CEE3", "#B2DF8A")
boxnames<-c("1:1", "A", "B")

#boxplots for each tissue
#####FEMALE TISSUES
#Brain
#1:1
barbcarp1<-cbind(brain1, brain1a)
#subA
barbcarp2<-cbind(brain2, brain2a)
#subB
barbcarp3<-cbind(brain2, brain2b)
#subA+subB
barbcarp4<-rbind(barbcarp2[,-6], barbcarp3[,-6])

boxplot(log2(barbcarp1[,9]/barbcarp1[,4]), log2(barbcarp2[,9]/barbcarp2[,4]), log2(barbcarp3[,9]/barbcarp3[,4]), notch=T, outline=F, names=boxnames, col=boxcols, main="Female Brain")
abline(h=median(log2((barbcarp2[,9]+barbcarp3[,9])/barbcarp3[,4]), na.rm=T), lty=2)

###stats

###1:1 versus A+B
pval<-wilcox.test(log2(barbcarp1[,9]/barbcarp1[,4]), log2((barbcarp2[,9]+barbcarp3[,9])/barbcarp3[,4]))$p.value
pval
if (pval<0.001) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "***", cex=2, col="white")
} else if (pval<0.01) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "**", cex=2, col="white")
} else if ( pval<0.05) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "*", cex=2, col="white")
} else {
###
}

###1:1 versus A and B
pval<-wilcox.test(log2(barbcarp1[,9]/barbcarp1[,4]), log2(barbcarp4[,8]/barbcarp4[,4]))$p.value
pval
#if (pval<0.001) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "***", cex=2, col="black")
#} else if (pval<0.01) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "**", cex=2, col="black")
#} else if ( pval<0.05) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "*", cex=2, col="black")
#} else {
###
#}

###A versus B
pval<-wilcox.test(log2(barbcarp2[,9]/barbcarp2[,4]), log2(barbcarp3[,9]/barbcarp3[,4]))$p.value
pval
if (pval<0.001) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "***", cex=2)
} else if (pval<0.01) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "**", cex=2)
} else if ( pval<0.05) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "*", cex=2)
} else {
###
}

#boxplots for each tissue
#Heart
#1:1
barbcarp1<-cbind(heart1, heart1a)
#subA
barbcarp2<-cbind(heart2, heart2a)
#subB
barbcarp3<-cbind(heart2, heart2b)
#subA+subB
barbcarp4<-rbind(barbcarp2[,-6], barbcarp3[,-6])
boxplot(log2(barbcarp1[,9]/barbcarp1[,4]), log2(barbcarp2[,9]/barbcarp2[,4]), log2(barbcarp3[,9]/barbcarp3[,4]), notch=T, outline=F, names=boxnames, col=boxcols, main="Female Heart")
abline(h=median(log2((barbcarp2[,9]+barbcarp3[,9])/barbcarp3[,4]), na.rm=T), lty=2)

###stats

###1:1 versus A+B
pval<-wilcox.test(log2(barbcarp1[,9]/barbcarp1[,4]), log2((barbcarp2[,9]+barbcarp3[,9])/barbcarp3[,4]))$p.value
pval
if (pval<0.001) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "***", cex=2, col="white")
} else if (pval<0.01) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "**", cex=2, col="white")
} else if ( pval<0.05) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "*", cex=2, col="white")
} else {
###
}

###1:1 versus A and B
pval<-wilcox.test(log2(barbcarp1[,9]/barbcarp1[,4]), log2(barbcarp4[,8]/barbcarp4[,4]))$p.value
pval
#if (pval<0.001) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "***", cex=2, col="black")
#} else if (pval<0.01) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "**", cex=2, col="black")
#} else if ( pval<0.05) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "*", cex=2, col="black")
#} else {
###
#}

###A versus B
pval<-wilcox.test(log2(barbcarp2[,9]/barbcarp2[,4]), log2(barbcarp3[,9]/barbcarp3[,4]))$p.value
pval
if (pval<0.001) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "***", cex=2)
} else if (pval<0.01) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "**", cex=2)
} else if ( pval<0.05) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "*", cex=2)
} else {
###
}
#boxplots for each tissue
#Spleen
#1:1
barbcarp1<-cbind(spleen1, spleen1a)
#subA
barbcarp2<-cbind(spleen2, spleen2a)
#subB
barbcarp3<-cbind(spleen2, spleen2b)
#subA+subB
barbcarp4<-rbind(barbcarp2[,-6], barbcarp3[,-6])
boxplot(log2(barbcarp1[,9]/barbcarp1[,4]), log2(barbcarp2[,9]/barbcarp2[,4]), log2(barbcarp3[,9]/barbcarp3[,4]), notch=T, outline=F, names=boxnames, col=boxcols, main="Female Spleen")
abline(h=median(log2((barbcarp2[,9]+barbcarp3[,9])/barbcarp3[,4]), na.rm=T), lty=2)

###stats

###1:1 versus A+B
pval<-wilcox.test(log2(barbcarp1[,9]/barbcarp1[,4]), log2((barbcarp2[,9]+barbcarp3[,9])/barbcarp3[,4]))$p.value
pval
if (pval<0.001) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "***", cex=2, col="white")
} else if (pval<0.01) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "**", cex=2, col="white")
} else if ( pval<0.05) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "*", cex=2, col="white")
} else {
###
}

###1:1 versus A and B
pval<-wilcox.test(log2(barbcarp1[,9]/barbcarp1[,4]), log2(barbcarp4[,8]/barbcarp4[,4]))$p.value
pval
#if (pval<0.001) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "***", cex=2, col="black")
#} else if (pval<0.01) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "**", cex=2, col="black")
#} else if ( pval<0.05) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "*", cex=2, col="black")
#} else {
###
#}

###A versus B
pval<-wilcox.test(log2(barbcarp2[,9]/barbcarp2[,4]), log2(barbcarp3[,9]/barbcarp3[,4]))$p.value
pval
if (pval<0.001) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "***", cex=2)
} else if (pval<0.01) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "**", cex=2)
} else if ( pval<0.05) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "*", cex=2)
} else {
###
}
#boxplots for each tissue
#Liver
#1:1
barbcarp1<-cbind(liver1, liver1a)
#subA
barbcarp2<-cbind(liver2, liver2a)
#subB
barbcarp3<-cbind(liver2, liver2b)
#subA+subB
barbcarp4<-rbind(barbcarp2[,-6], barbcarp3[,-6])
boxplot(log2(barbcarp1[,9]/barbcarp1[,4]), log2(barbcarp2[,9]/barbcarp2[,4]), log2(barbcarp3[,9]/barbcarp3[,4]), notch=T, outline=F, names=boxnames, col=boxcols, main="Female Liver")
abline(h=median(log2((barbcarp2[,9]+barbcarp3[,9])/barbcarp3[,4]), na.rm=T), lty=2)

###stats

###1:1 versus A+B
pval<-wilcox.test(log2(barbcarp1[,9]/barbcarp1[,4]), log2((barbcarp2[,9]+barbcarp3[,9])/barbcarp3[,4]))$p.value
pval
if (pval<0.001) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "***", cex=2, col="white")
} else if (pval<0.01) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "**", cex=2, col="white")
} else if ( pval<0.05) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "*", cex=2, col="white")
} else {
###
}

###1:1 versus A and B
pval<-wilcox.test(log2(barbcarp1[,9]/barbcarp1[,4]), log2(barbcarp4[,8]/barbcarp4[,4]))$p.value
pval
#if (pval<0.001) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "***", cex=2, col="black")
#} else if (pval<0.01) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "**", cex=2, col="black")
#} else if ( pval<0.05) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "*", cex=2, col="black")
#} else {
###
#}

###A versus B
pval<-wilcox.test(log2(barbcarp2[,9]/barbcarp2[,4]), log2(barbcarp3[,9]/barbcarp3[,4]))$p.value
pval
if (pval<0.001) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "***", cex=2)
} else if (pval<0.01) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "**", cex=2)
} else if ( pval<0.05) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "*", cex=2)
} else {
###
}

#boxplots for each tissue
#gonad
#1:1
barbcarp1<-cbind(gonad1, gonad1a)
#subA
barbcarp2<-cbind(gonad2, gonad2a)
#subB
barbcarp3<-cbind(gonad2, gonad2b)
#subA+subB
barbcarp4<-rbind(barbcarp2[,-6], barbcarp3[,-6])
boxplot(log2(barbcarp1[,9]/barbcarp1[,4]), log2(barbcarp2[,9]/barbcarp2[,4]), log2(barbcarp3[,9]/barbcarp3[,4]), notch=T, outline=F, names=boxnames, col=boxcols, main="Female Gonad")
abline(h=median(log2((barbcarp2[,9]+barbcarp3[,9])/barbcarp3[,4]), na.rm=T), lty=2)

###stats

###1:1 versus A+B
pval<-wilcox.test(log2(barbcarp1[,9]/barbcarp1[,4]), log2((barbcarp2[,9]+barbcarp3[,9])/barbcarp3[,4]))$p.value
pval
if (pval<0.001) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "***", cex=2, col="white")
} else if (pval<0.01) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "**", cex=2, col="white")
} else if ( pval<0.05) {
text(1, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+0.5, "*", cex=2, col="white")
} else {
###
}

###1:1 versus A and B
pval<-wilcox.test(log2(barbcarp1[,9]/barbcarp1[,4]), log2(barbcarp4[,8]/barbcarp4[,4]))$p.value
pval
#if (pval<0.001) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "***", cex=2, col="black")
#} else if (pval<0.01) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "**", cex=2, col="black")
#} else if ( pval<0.05) {
#text(2, median(log2(barbcarp1[,9]/barbcarp1[,4]), na.rm=T)+4, "*", cex=2, col="black")
#} else {
###
#}

###A versus B
pval<-wilcox.test(log2(barbcarp2[,9]/barbcarp2[,4]), log2(barbcarp3[,9]/barbcarp3[,4]))$p.value
pval
if (pval<0.001) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "***", cex=2)
} else if (pval<0.01) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "**", cex=2)
} else if ( pval<0.05) {
text(2.5, median(log2(barbcarp4[,8]/barbcarp4[,4]), na.rm=T)+2, "*", cex=2)
} else {
###
}
#####Male TISSUES
```

</details>

Let's add male tissues (in another block so it's not so much code in one go)

<details><summary>R code for male tissues</summary>

```
######################################
######################################
######################################
######################################

par(mar=c(3,3,3,0.3))
boxcols<-c("#FF7F00", "#A6CEE3", "#B2DF8A")

#boxplots for each tissue
#####MALE TISSUES
#Brain
#1:1
barbcarp1<-cbind(brain1, brain1a)
#subA
barbcarp2<-cbind(brain2, brain2a)
#subB
barbcarp3<-cbind(brain2, brain2b)
#subA+subB
barbcarp4<-rbind(barbcarp2[,-6], barbcarp3[,-6])

boxplot(log2(barbcarp1[,10]/barbcarp1[,5]), log2(barbcarp2[,10]/barbcarp2[,5]), log2(barbcarp3[,10]/barbcarp3[,5]), notch=T, outline=F, names=boxnames, col=boxcols, main="Male Brain")
abline(h=median(log2((barbcarp2[,10]+barbcarp3[,10])/barbcarp2[,5]), na.rm=T), lty=2)

###stats

###1:1 versus A+B
pval<-wilcox.test(log2(barbcarp1[,10]/barbcarp1[,5]), log2((barbcarp2[,10]+barbcarp3[,10])/barbcarp2[,5]))$p.value
pval
if (pval<0.001) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+0.5, "***", cex=2, col="white")
} else if (pval<0.01) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+ 0.5, "**", cex=2, col="white")
} else if ( pval<0.05) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+ 0.5, "*", cex=2, col="white")
} else {
###
}

###1:1 versus A and B
pval<-wilcox.test(log2(barbcarp1[,10]/barbcarp1[,5]), log2(barbcarp4[,9]/barbcarp4[,5]))$p.value
pval
###A versus B
pval<-wilcox.test(log2(barbcarp2[,10]/barbcarp2[,5]), log2(barbcarp3[,10]/barbcarp3[,5]))$p.value
pval
if (pval<0.001) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "***", cex=2)
} else if (pval<0.01) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "**", cex=2)
} else if ( pval<0.05) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "*", cex=2)
} else {
###
}

#boxplots for each tissue
#Heart
#1:1
barbcarp1<-cbind(heart1, heart1a)
#subA
barbcarp2<-cbind(heart2, heart2a)
#subB
barbcarp3<-cbind(heart2, heart2b)
#subA+subB
barbcarp4<-rbind(barbcarp2[,-6], barbcarp3[,-6])

boxplot(log2(barbcarp1[,10]/barbcarp1[,5]), log2(barbcarp2[,10]/barbcarp2[,5]), log2(barbcarp3[,10]/barbcarp3[,5]), notch=T, outline=F, names=boxnames, col=boxcols, main="Male Heart")
abline(h=median(log2((barbcarp2[,10]+barbcarp3[,10])/barbcarp2[,5]), na.rm=T), lty=2)

###stats

###1:1 versus A+B
pval<-wilcox.test(log2(barbcarp1[,10]/barbcarp1[,5]), log2((barbcarp2[,10]+barbcarp3[,10])/barbcarp2[,5]))$p.value
pval
if (pval<0.001) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+0.5, "***", cex=2, col="white")
} else if (pval<0.01) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+ 0.5, "**", cex=2, col="white")
} else if ( pval<0.05) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+ 0.5, "*", cex=2, col="white")
} else {
###
}

###1:1 versus A and B
pval<-wilcox.test(log2(barbcarp1[,10]/barbcarp1[,5]), log2(barbcarp4[,9]/barbcarp4[,5]))$p.value
pval
###A versus B
pval<-wilcox.test(log2(barbcarp2[,10]/barbcarp2[,5]), log2(barbcarp3[,10]/barbcarp3[,5]))$p.value
pval
if (pval<0.001) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "***", cex=2)
} else if (pval<0.01) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "**", cex=2)
} else if ( pval<0.05) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "*", cex=2)
} else {
###
}

#boxplots for each tissue
#Spleen
#1:1
barbcarp1<-cbind(spleen1, spleen1a)
#subA
barbcarp2<-cbind(spleen2, spleen2a)
#subB
barbcarp3<-cbind(spleen2, spleen2b)
#subA+subB
barbcarp4<-rbind(barbcarp2[,-6], barbcarp3[,-6])

boxplot(log2(barbcarp1[,10]/barbcarp1[,5]), log2(barbcarp2[,10]/barbcarp2[,5]), log2(barbcarp3[,10]/barbcarp3[,5]), notch=T, outline=F, names=boxnames, col=boxcols, main="Male Spleen")
abline(h=median(log2((barbcarp2[,10]+barbcarp3[,10])/barbcarp2[,5]), na.rm=T), lty=2)

###stats

###1:1 versus A+B
pval<-wilcox.test(log2(barbcarp1[,10]/barbcarp1[,5]), log2((barbcarp2[,10]+barbcarp3[,10])/barbcarp2[,5]))$p.value
pval
if (pval<0.001) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+0.5, "***", cex=2, col="white")
} else if (pval<0.01) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+ 0.5, "**", cex=2, col="white")
} else if ( pval<0.05) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+ 0.5, "*", cex=2, col="white")
} else {
###
}

###1:1 versus A and B
pval<-wilcox.test(log2(barbcarp1[,10]/barbcarp1[,5]), log2(barbcarp4[,9]/barbcarp4[,5]))$p.value
pval
###A versus B
pval<-wilcox.test(log2(barbcarp2[,10]/barbcarp2[,5]), log2(barbcarp3[,10]/barbcarp3[,5]))$p.value
pval
if (pval<0.001) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "***", cex=2)
} else if (pval<0.01) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "**", cex=2)
} else if ( pval<0.05) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "*", cex=2)
} else {
###
}

#boxplots for each tissue
#Liver
#1:1
barbcarp1<-cbind(liver1, liver1a)
#subA
barbcarp2<-cbind(liver2, liver2a)
#subB
barbcarp3<-cbind(liver2, liver2b)
#subA+subB
barbcarp4<-rbind(barbcarp2[,-6], barbcarp3[,-6])

boxplot(log2(barbcarp1[,10]/barbcarp1[,5]), log2(barbcarp2[,10]/barbcarp2[,5]), log2(barbcarp3[,10]/barbcarp3[,5]), notch=T, outline=F, names=boxnames, col=boxcols, main="Male Liver")
abline(h=median(log2((barbcarp2[,10]+barbcarp3[,10])/barbcarp2[,5]), na.rm=T), lty=2)

###stats

###1:1 versus A+B
pval<-wilcox.test(log2(barbcarp1[,10]/barbcarp1[,5]), log2((barbcarp2[,10]+barbcarp3[,10])/barbcarp2[,5]))$p.value
pval
if (pval<0.001) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+0.5, "***", cex=2, col="white")
} else if (pval<0.01) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+ 0.5, "**", cex=2, col="white")
} else if ( pval<0.05) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+ 0.5, "*", cex=2, col="white")
} else {
###
}

###1:1 versus A and B
pval<-wilcox.test(log2(barbcarp1[,10]/barbcarp1[,5]), log2(barbcarp4[,9]/barbcarp4[,5]))$p.value
pval
###A versus B
pval<-wilcox.test(log2(barbcarp2[,10]/barbcarp2[,5]), log2(barbcarp3[,10]/barbcarp3[,5]))$p.value
pval
if (pval<0.001) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "***", cex=2)
} else if (pval<0.01) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "**", cex=2)
} else if ( pval<0.05) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "*", cex=2)
} else {
###
}
#boxplots for each tissue
#gonad
#1:1
barbcarp1<-cbind(gonad1, gonad1a)
#subA
barbcarp2<-cbind(gonad2, gonad2a)
#subB
barbcarp3<-cbind(gonad2, gonad2b)
#subA+subB
barbcarp4<-rbind(barbcarp2[,-6], barbcarp3[,-6])

boxplot(log2(barbcarp1[,10]/barbcarp1[,5]), log2(barbcarp2[,10]/barbcarp2[,5]), log2(barbcarp3[,10]/barbcarp3[,5]), notch=T, outline=F, names=boxnames, col=boxcols, main="Male Gonad")
abline(h=median(log2((barbcarp2[,10]+barbcarp3[,10])/barbcarp2[,5]), na.rm=T), lty=2)

###stats

###1:1 versus A+B
pval<-wilcox.test(log2(barbcarp1[,10]/barbcarp1[,5]), log2((barbcarp2[,10]+barbcarp3[,10])/barbcarp2[,5]))$p.value
pval
if (pval<0.001) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+0.5, "***", cex=2, col="white")
} else if (pval<0.01) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+ 0.5, "**", cex=2, col="white")
} else if ( pval<0.05) {
text(1, median(log2(barbcarp1[,10]/barbcarp1[,5]), na.rm=T)+ 0.5, "*", cex=2, col="white")
} else {
###
}

###1:1 versus A and B
pval<-wilcox.test(log2(barbcarp1[,10]/barbcarp1[,5]), log2(barbcarp4[,9]/barbcarp4[,5]))$p.value
pval
###A versus B
pval<-wilcox.test(log2(barbcarp2[,10]/barbcarp2[,5]), log2(barbcarp3[,10]/barbcarp3[,5]))$p.value
pval
if (pval<0.001) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "***", cex=2)
} else if (pval<0.01) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "**", cex=2)
} else if ( pval<0.05) {
text(2.5, median(log2(barbcarp4[,9]/barbcarp4[,5]), na.rm=T)+3, "*", cex=2)
} else {
###
}
```

</details>

<img src="/images/derivedoverancestral_boxplots.jpg" width=700>

**Figure 2:** Log2 ( Carp_TPM/Barb_TPM) for 1:1 genes, subA and subB genes.  In almost all tissues, subA has a significantly lower TPM than SubB (Wilcoxon test p-values shown with black asterisks, *p<0.05, **p<0.01, ***p<0.001). 1:1 genes are expressed at higher levels (relative to ancestral expression) than 1:2 genes, consistent with some form of buffering/dosage compensation. The dotted lines shows the median of log2(TPM_SubA + TPM_SubB), and the white asterisks the significance of the difference between the distribution of 1:1 genes and the sum of subA and subB. 1:1 genes have significantly lower TPM than A+B, suggesting that compensation does not reach ancestral levels of expression.


## Subdominance: do we see it when we look at diffentially expressed homeologs? (yes, similar to Li et al 2021)

Let's count how many cases of SubA>SubB and SubB>SubA we have in each tissue, and plot one versus the other.

<details><summary>R code</summary>

```
#set working directory to directory where "carp2022_upload" is
setwd("~/Documents/Will/Carp/Carp2022/")
library("dplyr")                          # Load dplyr package

 #get DEseq output
brain<-read.table("Carp2022_upload/3-HomeoDiff/2-Orthogroups/Carp_Brain_exhaust_AvsBdeseq.txt", head=T, sep=" ")
 #make table
#get counts of A>B+M, B>A_M, A>B_F, B>A_F
brain_counts<-c(dim(subset(brain, Log2.B.A._M<0 & padj_M<0.05))[1], dim(ba_brain_M<-subset(brain, Log2.B.A._M>0 & padj_M<0.05))[1],  dim(subset(brain, Log2.B.A._F<0 & padj_F<0.05))[1],  dim(ba_brain_F<-subset(brain, Log2.B.A._F>0 & padj_F<0.05))[1]  )

 #get DEseq output
heart<-read.table("Carp2022_upload/3-HomeoDiff/2-Orthogroups/Carp_Heart_exhaust_AvsBdeseq.txt", head=T, sep=" ")
 #make table
#get counts of A>B+M, B>A_M, A>B_F, B>A_F
heart_counts<-c(dim(subset(heart, Log2.B.A._M<0 & padj_M<0.05))[1], dim(ba_heart_M<-subset(heart, Log2.B.A._M>0 & padj_M<0.05))[1],  dim(subset(heart, Log2.B.A._F<0 & padj_F<0.05))[1],  dim(ba_heart_F<-subset(heart, Log2.B.A._F>0 & padj_F<0.05))[1]  )

#get DEseq output
spleen<-read.table("Carp2022_upload/3-HomeoDiff/2-Orthogroups/Carp_Spleen_exhaust_AvsBdeseq.txt", head=T, sep=" ")
 #make table
#get counts of A>B+M, B>A_M, A>B_F, B>A_F
spleen_counts<-c(dim(subset(spleen, Log2.B.A._M<0 & padj_M<0.05))[1], dim(ba_spleen_M<-subset(spleen, Log2.B.A._M>0 & padj_M<0.05))[1],  dim(subset(spleen, Log2.B.A._F<0 & padj_F<0.05))[1],  dim(ba_spleen_F<-subset(spleen, Log2.B.A._F>0 & padj_F<0.05))[1]  )

 #get DEseq output
liver<-read.table("Carp2022_upload/3-HomeoDiff/2-Orthogroups/Carp_Liver_exhaust_AvsBdeseq.txt", head=T, sep=" ")
 #make table
#get counts of A>B+M, B>A_M, A>B_F, B>A_F
liver_counts<-c(dim(subset(liver, Log2.B.A._M<0 & padj_M<0.05))[1], dim(ba_liver_M<-subset(liver, Log2.B.A._M>0 & padj_M<0.05))[1],  dim(subset(liver, Log2.B.A._F<0 & padj_F<0.05))[1],  dim(ba_liver_F<-subset(liver, Log2.B.A._F>0 & padj_F<0.05))[1]  )


 #get DEseq output
gonad<-read.table("Carp2022_upload/3-HomeoDiff/2-Orthogroups/Carp_Gonad_exhaust_AvsBdeseq.txt", head=T, sep=" ")
 #make table
#get counts of A>B+M, B>A_M, A>B_F, B>A_F
gonad_counts<-c(dim(subset(gonad, Log2.B.A._M<0 & padj_M<0.05))[1], dim(ba_gonad_M<-subset(gonad, Log2.B.A._M>0 & padj_M<0.05))[1],  dim(subset(gonad, Log2.B.A._F<0 & padj_F<0.05))[1],  dim(ba_gonad_F<-subset(gonad, Log2.B.A._F>0 & padj_F<0.05))[1]  )

allcounts<-c(brain_counts, heart_counts, spleen_counts, liver_counts, gonad_counts)

#plot
par(mfrow=c(1,1))
par(mar=c(4,4,0.5,0.5))
plot(allcounts[c(1,3,5,7,9, 11, 13, 15, 17, 19)], allcounts[c(2,4,6,8,10,12,14,16,18,20)], xlim=c(1000, 2000), ylim=c(1000,2000), col="#1F78B4", pch=20, xlab="# SubA>SubB pairs", ylab="# SubB>SubA pairs")
 abline(a=0, b=1, lty=2)
 wilcox.test(allcounts[c(1,3,5,7,9, 11, 13, 15, 17, 19)], allcounts[c(2,4,6,8,10,12,14,16,18,20)], paired=T)
```

</details>

<img src="/images/AB_vs_BA_plot.jpg" width=500>

Figure 3: The number of homeolog pairs for which the SubA homeolog is significantly more expressed that the SubB homeolog, versus the number for which SubB>SubA. In all tissues, the number of SubB>SubA is larger than SubA>SubB (p=0.001953 with a paired Wilcoxon test).
