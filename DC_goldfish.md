# Run Orthofinder with Goldfish to infer ancient and new rediploidizations

## 1. Run Orthofinder again

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/2c-OrthofinderGoldF

```
ln -s ../../Cyprinus_carpio-protein.fa .
ln -s ../../Puntius_tetrazona-protein.fa .
ln -s ../../Paracanthobrama_guichenoti-protein.fa .
ln -s ../../Danio_rerio_Longestprot.fa .
ln ../2-Orthofinder/Barb_prot.fa .
ln -s ../../Carassius_auratus-protein.fa
```

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/

```
#run commands on SLURM's srun
/nfs/scistore03/vicosgrp/melkrewi/orthofinder/OrthoFinder/orthofinder -f 2c-OrthofinderGoldF
```

## 2. Make lists of 1:1:2 (recent) and 1:1:1 (ancient) rediploidized genes

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables

Let's output a table with the number of goldfish homologs for each barb gene:

```
cat ../2c-OrthofinderGoldF/OrthoFinder/Results_Jan12/Orthologues/Orthologues_Barb_prot/Barb_prot__v__Carassius_auratus-protein.tsv | awk '{for(i=1;i<=NF;i++)if($i ~ /^CAFS/)x++;print $2, x;x=0}' | sort > Barb_NgoldfishHomologs.joinable
```

This table looks like:

```
TRINITY_DN7404_c0_g2_i2 1
TRINITY_DN5284_c2_g1_i1 2
TRINITY_DN9167_c0_g1_i1 1
TRINITY_DN9963_c0_g1_i1 1
TRINITY_DN1575_c1_g1_i1 4
TRINITY_DN12148_c0_g1_i1 6
TRINITY_DN1905_c0_g1_i12 5
TRINITY_DN103038_c0_g1_i3 1
TRINITY_DN13907_c0_g2_i1, 1
```
