# Sex and tissue subfunctionalization

## Load data

We need:
- table with all tissue expression
- p-values between male and female

<details><summary>R code for loading data</summary>

```
#set working directory to directory where "carp2022_upload" is
setwd("~/Documents/Will/Carp/Carp2022/")
library("dplyr")                          # Load dplyr package

 #get set of 1:1 orthologs
orth1<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to1_exhaust.txt")
#get set of 1:2 orthologs
orth2<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to2_exhaust_correctChrom.txt", head=F, sep=" ")
colnames(orth1)<-c("orthogroup", "barbgene", "carpgene", "chr", "subgenome")
colnames(orth2)<-c("orthogroup", "barbgene", "carpA", "chrA", "carpB", "chrB")

#Get expression
bbrain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Brain_DEseq2.txt", head=T, sep=" ")
bgonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Gonad_DEseq2.txt", head=T, sep=" ")
bheart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Heart_DEseq2.txt", head=T, sep=" ")
bliver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Liver_DEseq2.txt", head=T, sep=" ")
bspleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Spleen_DEseq2.txt", head=T, sep=" ")
brain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Brain_DEseq2.txt", head=T, sep=" ")
gonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Gonad_DEseq2.txt", head=T, sep=" ")
heart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Heart_DEseq2.txt", head=T, sep=" ")
liver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Liver_DEseq2.txt", head=T, sep=" ")
spleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Spleen_DEseq2.txt", head=T, sep=" ")

#make brain tables

brain1 <- inner_join(orth1[,1:2], bbrain, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]     
brain1a <- inner_join(orth1[,c(1,3)], brain, by = c("carpgene" = "Gene"))[,c(2,4,5,12,13)]

brain2 <- inner_join(orth2[,1:2], bbrain, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
brain2a <- inner_join(orth2[,c(1,3)], brain, by = c("carpA" = "Gene"))[,c(2,4,5,12,13)]
brain2b <- inner_join(orth2[,c(1,5)], brain, by = c("carpB" = "Gene"))[,c(2,4,5,12,13)]

#make gonad tables

gonad1 <- inner_join(orth1[,1:2], bgonad, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
gonad1a <- inner_join(orth1[,c(1,3)], gonad, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

gonad2 <- inner_join(orth2[,1:2], bgonad, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
gonad2a <- inner_join(orth2[,c(1,3)], gonad, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
gonad2b <- inner_join(orth2[,c(1,5)], gonad, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]

#make heart tables

heart1<-inner_join(orth1[,1:2], bheart, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
heart1a<-inner_join(orth1[,c(1,3)], heart, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

heart2<-inner_join(orth2[,1:2], bheart, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
heart2a<-inner_join(orth2[,c(1,3)], heart, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
heart2b<-inner_join(orth2[,c(1,5)], heart, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]
#make liver tables
liver1<-inner_join(orth1[,1:2], bliver, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
liver1a<-inner_join(orth1[,c(1,3)], liver, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

liver2<-inner_join(orth2[,1:2], bliver, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
liver2a<-inner_join(orth2[,c(1,3)], liver, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
liver2b<-inner_join(orth2[,c(1,5)], liver, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]
#make spleen tables
spleen1<-inner_join(orth1[,1:2], bspleen, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
spleen1a<-inner_join(orth1[,c(1,3)], spleen, by = c("carpgene" = "Gene"))[,c(2,4,5,12,13)]

spleen2<-inner_join(orth2[,1:2], bspleen, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
spleen2a<-inner_join(orth2[,c(1,3)], spleen, by = c("carpA" = "Gene"))[,c(2,4,5,12,13)]
spleen2b<-inner_join(orth2[,c(1,5)], spleen, by = c("carpB" = "Gene"))[,c(2,4,5,12,13)]

```

</details>


<details><summary>R code for making normalized expression table with all tissues/species</summary>

```
all<-(cbind(brain2$meanF, brain2a$meanF, brain2b$meanF, gonad2$meanF, gonad2a$meanF, gonad2b$meanF, heart2$meanF, heart2a$meanF, heart2b$meanF,liver2$meanF, liver2a$meanF, liver2b$meanF, spleen2$meanF, spleen2a$meanF, spleen2b$meanF, brain2$meanM, brain2a$meanM, brain2b$meanM, gonad2$meanM, gonad2a$meanM, gonad2b$meanM, heart2$meanM, heart2a$meanM, heart2b$meanM,liver2$meanM, liver2a$meanM, liver2b$meanM, spleen2$meanM, spleen2a$meanM, spleen2b$meanM))
colnames(all)<-c("barb_brainF", "subA_brainF", "subB_brainF", "barb_gonadF", "subA_gonadF", "subB_gonadF", "barb_heartF", "subA_heartF", "subB_heartF",  "barb_liverF", "subA_liverF", "subB_liverF",  "barb_spleenF", "subA_spleenF", "subB_spleenF", "barb_brainM", "subA_brainM", "subB_brainM", "barb_gonadM", "subA_gonadM", "subB_gonadM", "barb_heartM", "subA_heartM", "subB_heartM",  "barb_liverM", "subA_liverM",  "subB_liverM",  "barb_spleenM", "subA_spleenM", "subB_spleenM")

###Normalize
bolFMat<-as.matrix(all, nrow = nrow(all), ncol = ncol(all))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(all)
colnames(temp2)<-colnames(all)
all <-as.data.frame(temp2)
rownames(all)<-brain2$barbgene

all1<-(cbind(brain1$meanF, brain1a$meanF, gonad1$meanF, gonad1a$meanF, heart1$meanF, heart1a$meanF, liver1$meanF, liver1a$meanF, spleen1$meanF, spleen1a$meanF, brain1$meanM, brain1a$meanM, gonad1$meanM, gonad1a$meanM, heart1$meanM, heart1a$meanM, liver1$meanM, liver1a$meanM, spleen1$meanM, spleen1a$meanM))
colnames(all1)<-c("barb_brainF", "subA_brainF",  "barb_gonadF", "subA_gonadF", "barb_heartF", "subA_heartF",  "barb_liverF", "subA_liverF", "barb_spleenF", "subA_spleenF", "barb_brainM", "subA_brainM",  "barb_gonadM", "subA_gonadM", "barb_heartM", "subA_heartM", "barb_liverM", "subA_liverM",  "barb_spleenM", "subA_spleenM")


###Normalize
bolFMat<-as.matrix(all1, nrow = nrow(all1), ncol = ncol(all1))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(all1)
colnames(temp2)<-colnames(all1)
all1 <-as.data.frame(temp2)
rownames(all1)<-brain1$barbgene

```

</details>


## Plot for each tissue

<details><summary>R code for making plot with all tissues</summary>

```
par(mfrow=c(2,3))
par(mar=c(4,4,4,1))
nscol<-"#A6CEE3"
sigcol<-"#E31A1C"

###brain
plot(log2(all$subA_brainF/all$subA_brainM), log2(all$subB_brainF/all$subB_brainM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=nscol, main="Brain")
abline(h=0, lty=2)
abline(v=0, lty=2)

deseq<-as.data.frame(cbind(brain2a$log2FoldChange, brain2a$padj, brain2b$log2FoldChange, brain2b$padj))
colnames(deseq)<-c("logA", "pA", "logB", "pB")
rownames(deseq)<-brain2$barbgene
head(deseq)
sexantag<-merge(subset(deseq, pA<0.05 & pB<0.05 & (logA*logB)<0), all, by=0)
dim(sexantag)
points(log2(sexantag$subA_brainF/sexantag$subA_brainM), log2(sexantag$subB_brainF/sexantag$subB_brainM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=sigcol)

###heart
plot(log2(all$subA_heartF/all$subA_heartM), log2(all$subB_heartF/all$subB_heartM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=nscol, main="Heart")
abline(h=0, lty=2)
abline(v=0, lty=2)

deseq<-as.data.frame(cbind(heart2a$log2FoldChange, heart2a$padj, heart2b$log2FoldChange, heart2b$padj))
colnames(deseq)<-c("logA", "pA", "logB", "pB")
rownames(deseq)<-heart2$barbgene
head(deseq)
sexantag<-merge(subset(deseq, pA<0.05 & pB<0.05 & (logA*logB)<0), all, by=0)
dim(sexantag)
points(log2(sexantag$subA_heartF/sexantag$subA_heartM), log2(sexantag$subB_heartF/sexantag$subB_heartM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=sigcol)

###spleen
plot(log2(all$subA_spleenF/all$subA_spleenM), log2(all$subB_spleenF/all$subB_spleenM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=nscol, main="Spleen")
abline(h=0, lty=2)
abline(v=0, lty=2)

deseq<-as.data.frame(cbind(spleen2a$log2FoldChange, spleen2a$padj, spleen2b$log2FoldChange, spleen2b$padj))
colnames(deseq)<-c("logA", "pA", "logB", "pB")
rownames(deseq)<-spleen2$barbgene
head(deseq)
sexantag<-merge(subset(deseq, pA<0.05 & pB<0.05 & (logA*logB)<0), all, by=0)
dim(sexantag)
points(log2(sexantag$subA_spleenF/sexantag$subA_spleenM), log2(sexantag$subB_spleenF/sexantag$subB_spleenM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=sigcol)

###liver
plot(log2(all$subA_liverF/all$subA_liverM), log2(all$subB_liverF/all$subB_liverM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=nscol, main="Liver")
abline(h=0, lty=2)
abline(v=0, lty=2)

deseq<-as.data.frame(cbind(liver2a$log2FoldChange, liver2a$padj, liver2b$log2FoldChange, liver2b$padj))
colnames(deseq)<-c("logA", "pA", "logB", "pB")
rownames(deseq)<-liver2$barbgene
head(deseq)
sexantag<-merge(subset(deseq, pA<0.05 & pB<0.05 & (logA*logB)<0), all, by=0)
dim(sexantag)
points(log2(sexantag$subA_liverF/sexantag$subA_liverM), log2(sexantag$subB_liverF/sexantag$subB_liverM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=sigcol)
###gonad
plot(log2(all$subA_gonadF/all$subA_gonadM), log2(all$subB_gonadF/all$subB_gonadM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=nscol, main="Gonad")
abline(h=0, lty=2)
abline(v=0, lty=2)

deseq<-as.data.frame(cbind(gonad2a$log2FoldChange, gonad2a$padj, gonad2b$log2FoldChange, gonad2b$padj))
colnames(deseq)<-c("logA", "pA", "logB", "pB")
rownames(deseq)<-gonad2$barbgene
head(deseq)
sexantag<-merge(subset(deseq, pA<0.05 & pB<0.05 & (logA*logB)<0), all, by=0)
dim(sexantag)
points(log2(sexantag$subA_gonadF/sexantag$subA_gonadM), log2(sexantag$subB_gonadF/sexantag$subB_gonadM), pch=20, col=sigcol)
```

</details>

<img src="/images/Subf_eachtissue.jpg" width=600>


## Plot with all tissues

Can we figure out whether the amount of subfunctionalization that we see is more extensive than we would expect given the differences between the tissues?

Let's use Limma to try and assess this. For each pair of tissues, we can get differential expression for:
- log2(TPM+1) --> one value per gene
- log2([A+1]/[B+1])) --> one value per pair

We can then plot one against the other, to see if gonads simply follow the trend. 

<details><summary>Step 1: Load the tables</summary>

```
############BRAIN
brain2a_reps <- inner_join(orth2[,c(1,3)], brain, by = c("carpA" = "Gene"))[,c(6:11)]
brain2b_reps <- inner_join(orth2[,c(1,5)], brain, by = c("carpB" = "Gene"))[,c(6:11)]
brain_allF<-log2(brain[,grep("_F_", colnames(brain))]+1)
brain_allM<-log2(brain[,grep("_M_", colnames(brain))]+1)

brain_abratio<-log2((brain2a_reps+1)/(brain2b_reps+1))
brain_abratio_F<-brain_abratio[,grep("_F_", colnames(brain_abratio))]
head(brain_abratio_F)
brain_abratio_M<-brain_abratio[,grep("_M_", colnames(brain_abratio))]
head(brain_abratio_M)

##############GONAD
gonad2a_reps <- inner_join(orth2[,c(1,3)], gonad, by = c("carpA" = "Gene"))[,c(6:11)]
gonad2b_reps <- inner_join(orth2[,c(1,5)], gonad, by = c("carpB" = "Gene"))[,c(6:11)]
gonad_allF<-log2(gonad[,grep("_F_", colnames(gonad))]+1)
gonad_allM<-log2(gonad[,grep("_M_", colnames(gonad))]+1)


gonad_abratio<-log2((gonad2a_reps+1)/(gonad2b_reps+1))
head(gonad_abratio)
gonad_abratio_F<-gonad_abratio[,grep("_F_", colnames(gonad_abratio))]
head(gonad_abratio_F)
gonad_abratio_M<-gonad_abratio[,grep("_M_", colnames(gonad_abratio))]
head(gonad_abratio_M)

##############LIVER
liver2a_reps <- inner_join(orth2[,c(1,3)], liver, by = c("carpA" = "Gene"))[,c(6:11)]
liver2b_reps <- inner_join(orth2[,c(1,5)], liver, by = c("carpB" = "Gene"))[,c(6:11)]
liver_allF<-log2(liver[,grep("_F_", colnames(liver))]+1)
liver_allM<-log2(liver[,grep("_M_", colnames(liver))]+1)

liver_abratio<-log2((liver2a_reps+1)/(liver2b_reps+1))
head(liver_abratio)
liver_abratio_F<-liver_abratio[,grep("_F_", colnames(liver_abratio))]
head(liver_abratio_F)
liver_abratio_M<-liver_abratio[,grep("_M_", colnames(liver_abratio))]
head(liver_abratio_M)

##############SPLEEN
spleen2a_reps <- inner_join(orth2[,c(1,3)], spleen, by = c("carpA" = "Gene"))[,c(6:11)]
spleen2b_reps <- inner_join(orth2[,c(1,5)], spleen, by = c("carpB" = "Gene"))[,c(6:11)]
spleen_allF<-log2(spleen[,grep("_F_", colnames(spleen))]+1)
spleen_allM<-log2(spleen[,grep("_M_", colnames(spleen))]+1)

spleen_abratio<-log2((spleen2a_reps+1)/(spleen2b_reps+1))
head(spleen_abratio)
spleen_abratio_F<-spleen_abratio[,grep("_F_", colnames(spleen_abratio))]
head(spleen_abratio_F)
spleen_abratio_M<-spleen_abratio[,grep("_M_", colnames(spleen_abratio))]
head(spleen_abratio_M)

##############HEART
heart2a_reps <- inner_join(orth2[,c(1,3)], heart, by = c("carpA" = "Gene"))[,c(6:11)]
heart2b_reps <- inner_join(orth2[,c(1,5)], heart, by = c("carpB" = "Gene"))[,c(6:11)]
heart_allF<-log2(heart[,grep("_F_", colnames(heart))]+1)
heart_allM<-log2(heart[,grep("_M_", colnames(heart))]+1)


heart_abratio<-log2((heart2a_reps+1)/(heart2b_reps+1))
head(heart_abratio)
heart_abratio_F<-heart_abratio[,grep("_F_", colnames(heart_abratio))]
head(heart_abratio_F)
heart_abratio_M<-heart_abratio[,grep("_M_", colnames(heart_abratio))]
head(heart_abratio_M)

```

</details>

<details><summary>Step 2: Run Limma (do for each pair of tissues, using either all genes or ratios)</summary>

```



################Limma 
library(limma)

####Pick tissues to analyse, and whether to look for subfunctionalization or differential expression
### Table with Log2([subA+1]/[subB+1] for females and female
### e.g. heart_abratio_F and heart_abratio_M
### or tissue 1 and tissue 2
### e.g.  heart_abratio_F and spleen_abratio_F
### Or do the same for all genes to find differential expression
### e.g.  heart_allF and spleen_allF
t1<-heart_abratio_F
t2<-spleen_abratio_F

exp_matrix<-as.matrix(cbind(t1, t2))
###Normalize
bolFMat<-as.matrix(exp_matrix, nrow = nrow(exp_matrix), ncol = ncol(exp_matrix))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(exp_matrix)
colnames(temp2)<-colnames(exp_matrix)
exp_matrix <-temp2


#check that there are no issues
head(exp_matrix)
min(cor(exp_matrix))


 # Create a design matrix
#### tells Limma that the first xx columns are t1, the last xx t2
t1cols<-rep.int(1, ncol(t1))
t2cols<-rep.int(2, ncol(t2))
design <- model.matrix(~ 0+factor(c(t1cols, t2cols)))
# assign the column names
colnames(design) <- c("t1", "t2")
design

# tell Limma what to compare
cont_matrix <- makeContrasts(t2vst1 = t2-t1, levels=design)

####Run Limma
# Fit the expression matrix to a linear model
fit <- lmFit(exp_matrix, design)
# Compute contrast
fit_contrast <- contrasts.fit(fit, cont_matrix)
# Bayes statistics of differential expression
# *There are several options to tweak!*
fit_contrast <- eBayes(fit_contrast)

####View/get results
# Generate a vocalno plot to visualize differential expression
volcanoplot(fit_contrast)
# Generate a list of differentially expressed genes
top_genes <- topTable(fit_contrast, p.value=0.05, adjust = "BH", number=nrow(exp_matrix))
# Summary of results (number of differentially expressed genes in both directions)
result <- decideTests(fit_contrast)
summary(result)


```

</details>

The results for each pair of tissues are summarized [here](https://docs.google.com/spreadsheets/d/15FpotoffsF5gWjJpMzgvGZyrfMTf7RIOFsKgwfAepv8/edit?usp=sharing). We made a text file out of sheet three, and use it to plot the number of subfunctionalized genes as a function of the number of differentially expressed genes for any two tissues (either female vs male, or different tissues within one sex). 

<details><summary>Plot # subfunctionalized vs # differentially expressed</summary>

```
subf<-read.table("~/Documents/Will/Carp/Carp2022/SunfunctTables/Table_Dec7_2021.txt", sep="\t", head=T)
subf
#set up linear model
lm_subf <- lm(Ratio ~ All, data = subf)
summary(lm_subf)

#Plot data and model
par(mar=c(4,4,0.2,1))
#empty plot with regression line
plot(subf$All, subf$Ratio, col="white", xlab="Number of DE genes", ylab="Number of subfunctionalized pairs")
abline(lm_subf, col="lightblue")

#set range of x value for which to plot confidence interval
newx <- seq(min(subf$All), max(subf$All), by=100)

conf_interval <- predict(lm_subf, newdata=data.frame(All=newx), interval="prediction",
                         level = 0.95)
lines(newx, conf_interval[,2], col="lightblue", lty=2)
lines(newx, conf_interval[,3], col="lightblue", lty=2)

#replot points
gonad_f<-subset(subf, comparison=="F" & gonad=="yes")
gonad_m<-subset(subf, comparison=="M" & gonad=="yes")
gonad_fm<-subset(subf, comparison=="FM" & gonad=="yes")
soma_f<-subset(subf, comparison=="F" & gonad=="no")
soma_m<-subset(subf, comparison=="M" & gonad=="no")
soma_fm<-subset(subf, comparison=="FM" & gonad=="no")
fcol<-"#FB9A99"
mcol<-"#A6CEE3"
fmcol<-"#B2DF8A"
pchsoma<-21
pchgonad<-23

points(gonad_f$All, gonad_f$Ratio, col="black", bg=fcol, pch=pchgonad)
points(gonad_m$All, gonad_m$Ratio, col="black", bg=mcol, pch=pchgonad)
points(gonad_fm$All, gonad_fm$Ratio, col="black", bg=fmcol, pch=pchgonad)

points(soma_f$All, soma_f$Ratio, col="black", bg=fcol, pch=pchsoma)
points(soma_m$All, soma_m$Ratio, col="black", bg=mcol, pch=pchsoma)
points(soma_fm$All, soma_fm$Ratio, col="black", bg=fmcol, pch=pchsoma)

#calculate cooks distance and (optional) plot it to check that gonad_fm is an outlier
cooksd <- cooks.distance(lm_subf)
 #plot cook's distance to get a sense of how many outliers
# plot(cooksd, pch="*", cex=2, main="Influential Obs by Cooks distance")  # plot cook's distance
# abline(h = 4*mean(cooksd, na.rm=T), col="red")  # add cutoff line
#  text(x=1:length(cooksd)+1, y=cooksd, labels=ifelse(cooksd>4*mean(cooksd, na.rm=T),names(cooksd),""), col="red")  # add labels
 #Bonferroni Outlier Test 
 car::outlierTest(lm_subf)
#text(subf[5,3], subf[5,2]+90, "p = 0.03")
```

</details>

<img src="/images/Subf_alltissuesplot.jpg" width=300>

**Figure 2**: The gonad is a hotspot for subfunctionalization. For each pair of tissues, the number of subfunctionalized homeolog pairs (pairs for which the log2(A/B) is significantly different between two tissues) is shown as a function of the total number of genes that are differentially expressed between the tissues. Circles show comparisons involving only somatic tissues, while diamonds show comparisons involving the gonad. Blue and pink refer to comparisons between male and female tissues respectively (e.g. male liver versus male heart), while green shows comparisons between male and female tissues (e.g. male versus female gonad). The full line shows the fitted linear model, and the dotted lines the 95% prediction interval. The data point for female versus male gonad (green diamond) is the only one flagged as a significant outlier with a Bonferroni outlier test (p=0.03).

Ours is a conservative approach, as changes in the gonad will lead to more subfuncionalized genes in the gonad-soma comparisons even if they are driven by sexual antagonism. To illustrate this, we can use only soma-soma comparisons to fit the linear model.

<details><summary>Plot model using only soma-soma comparisons</summary>

```
soma<-rbind(soma_f,soma_m, soma_fm)
soma

#set up linear model
lm_subf <- lm(Ratio ~ All, data = soma)
summary(lm_subf)

#Plot data and model
par(mar=c(4,4,0.2,1))
#empty plot with regression line
plot(subf$All, subf$Ratio, col="white", xlab="Number of DE genes", ylab="Number of subfunctionalized pairs")
abline(lm_subf, col="lightblue")

#set range of x value for which to plot confidence interval
newx <- seq(min(subf$All), max(subf$All), by=100)

conf_interval <- predict(lm_subf, newdata=data.frame(All=newx), interval="prediction",
                         level = 0.95)
lines(newx, conf_interval[,2], col="lightblue", lty=2)
lines(newx, conf_interval[,3], col="lightblue", lty=2)

#replot points
gonad_f<-subset(subf, comparison=="F" & gonad=="yes")
gonad_m<-subset(subf, comparison=="M" & gonad=="yes")
gonad_fm<-subset(subf, comparison=="FM" & gonad=="yes")
soma_f<-subset(subf, comparison=="F" & gonad=="no")
soma_m<-subset(subf, comparison=="M" & gonad=="no")
soma_fm<-subset(subf, comparison=="FM" & gonad=="no")
fcol<-"#FB9A99"
mcol<-"#A6CEE3"
fmcol<-"#B2DF8A"
pchsoma<-21
pchgonad<-23

points(gonad_f$All, gonad_f$Ratio, col="lightgrey", bg="lightgrey", pch=pchgonad)
points(gonad_m$All, gonad_m$Ratio, col="lightgrey", bg="lightgrey", pch=pchgonad)
points(gonad_fm$All, gonad_fm$Ratio, col="black", bg=fmcol, pch=pchgonad)

points(soma_f$All, soma_f$Ratio, col="black", bg=fcol, pch=pchsoma)
points(soma_m$All, soma_m$Ratio, col="black", bg=mcol, pch=pchsoma)
points(soma_fm$All, soma_fm$Ratio, col="black", bg=fmcol, pch=pchsoma)

#calculate cooks distance and (optional) plot it to check that gonad_fm is an outlier
cooksd <- cooks.distance(lm_subf)
 #plot cook's distance to get a sense of how many outliers
# plot(cooksd, pch="*", cex=2, main="Influential Obs by Cooks distance")  # plot cook's distance
# abline(h = 4*mean(cooksd, na.rm=T), col="red")  # add cutoff line
#  text(x=1:length(cooksd)+1, y=cooksd, labels=ifelse(cooksd>4*mean(cooksd, na.rm=T),names(cooksd),""), col="red")  # add labels
 #Bonferroni Outlier Test 
 car::outlierTest(lm_subf)
#text(subf[5,3], subf[5,2]+90, "p = 0.03")
```

</details>

<img src="/images/Subf_alltissuesplot_onlySoma.jpg" width=300>

**Figure 3**: Same as before, but comparisons involving gonads (diamonds, green for male versus female, grey for soma-gonad comparisons within each sex) are not used to fit the linear model.  
