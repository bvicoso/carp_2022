# Puntius titteya transcriptome assembly

## Read trimming

For each fastq pair of files (because the reads are paired end), the following Trimmomatic command was run:

```
java -jar /nfs/scistore03/vicosgrp/wgammerd/sw/Trimmomatic-0.39/trimmomatic-0.39.jar PE -threads 16 Puntius_titteya_RNAseq_reads_Male_25_Heart_1.fastq Puntius_titteya_RNAseq_reads_Male_25_Heart_2.fastq Puntius_titteya_RNAseq_reads_Male_25_Heart_1P.fq Puntius_titteya_RNAseq_reads_Male_25_Heart_1U.fq Puntius_titteya_RNAseq_reads_Male_25_Heart_2P.fq Puntius_titteya_RNAseq_reads_Male_25_Heart_2U.fq ILLUMINACLIP:/nfs/scistore03/vicosgrp/wgammerd/sw/Trimmomatic-0.39/adapters/TruSeq3-PE-2.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
```

## Trinity transcriptome assembly

All trimmed reads were used for the transcriptome assembly:

```
Trinity --seqType fq --samples_file sample_file.txt --output trinity_output/ --CPU 20 --max_memory 60G
```

