#!/usr/bin/perl

#modules to load
#module load R
#usage: perl Sleuth_SexBias_runner.pl ReadsList.txt
#ReadsList.txt is a tab-separated file that has the following columns: species, sex, tissue, sample number, read file location.

system "mkdir SleuthSexBias";


my $list = $ARGV[0];
open (INPUT, "$list") or die "can't find $input";


#first create config files and 
while ($run = <INPUT>) 
	{
	chomp $run;
	($species, $sex, $tissue, $number, $files) = split(/\t/, $run);
	print "$run\n";
	$filename = "$species\_$sex\_$tissue\_$number";
	print $filename;
	$analysis = "$species\_$tissue";


	if ( $species eq "Aib" || $species eq "UrmAs" || $species eq "Ata")
		{
		#do nothing since these are asexual, too lazy to change this
		}	
	else
		{
		#Otherwise we have a sexual species

		system "mkdir SleuthSexBias/$analysis";
		system "mkdir SleuthSexBias/$analysis/KalFiles";


		#write info to config file
		system "echo \'$filename $sex\' >> SleuthSexBias/$analysis/SleuthConfig_temp.txt";

		#copy Kallisto files to corresponding folder

		system "cp -r /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/1-AllTranscripts/KalFiles/$filename SleuthSexBias/$analysis/KalFiles";
#                system "cp -r /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/2-Orthogroups/KalFiles/$filename SleuthSexBias/$analysis/KalFiles";
#                system "cp -r /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/3-Trios/KalFiles/$filename SleuthSexBias/$analysis/KalFiles";

		push(@analyses, $analysis);


		}

	}

#get unique set of analyses to perform
%seen = ();
foreach $analysis (@analyses) {
    push(@uniq, $analysis) unless $seen{$analysis}++;
}

print "@uniq";

#Now let's produce the R script for each analysis
foreach (@uniq)
        {
	#first sort and add header to the configuration file ("run_accession condition")
	system "sort SleuthSexBias/$_/SleuthConfig_temp.txt | sed \'1i run_accession condition\' > SleuthSexBias/$_/SleuthConfig.txt";
	
	#then produce R script for running Sleuth
	open (RSCRIPT, ">SleuthSexBias/$_/Rscript.txt");



print RSCRIPT "#These are the R commands for running Sleuth

#go to correct directory for the analysis
setwd(\"/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/3-Sleuth/1-AllTranscripts/SleuthSexBias/$_/\")
#setwd(\"/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/3-Sleuth/2-Orthogroups/SleuthSexBias/$_/\")
#setwd(\"/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/3-Sleuth/3-Trios/SleuthSexBias/$_/\")


#First, load the pre-installed Sleuth Library
library(\"sleuth\")

#set directory of Kallisto Results (change the path to your group's directory)
base_dir <- \"/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/3-Sleuth/1-AllTranscripts/SleuthSexBias/$_/\"
#base_dir <- \"/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/3-Sleuth/2-Orthogroups/SleuthSexBias/$_/\"
#base_dir <- \"/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/3-Sleuth/3-Trios/SleuthSexBias/$_/\"


sample_id <- dir(file.path(base_dir, \"KalFiles\"))
kal_dirs <- sapply(sample_id, function(id) file.path(base_dir, \"KalFiles\", id))

#give sample information</n>
s2c <- read.table(file.path(base_dir, \"SleuthConfig.txt\"), header = TRUE, stringsAsFactors=FALSE)
s2c <- dplyr::select(s2c, sample = run_accession, condition)
s2c <- dplyr::mutate(s2c, path = kal_dirs)
print(s2c)

#run Sleuth
so <- sleuth_prep(s2c, extra_bootstrap_summary = TRUE)
so <- sleuth_fit(so, ~condition, \'full\')
so <- sleuth_fit(so, ~1, \'reduced\')
so <- sleuth_lrt(so, \'reduced\', \'full\')

#Output the results to tables to be used later
#Sleuth produces two kinds of tables, one with q-values, another with the summary of expression values
sleuth_table <- sleuth_results(so, \'reduced:full\', test_type = \'lrt\')
SummaryKallisto_table<- sleuth_to_matrix(so, \"obs_norm\", \"tpm\")
SummaryTPM <- cbind(rownames(SummaryKallisto_table), data.frame(SummaryKallisto_table, row.names=NULL))
colnames(SummaryTPM)[1] <- \"transcript\"
final<-merge(sleuth_table, SummaryTPM, by.x=\"target_id\", by.y=\"transcript\")


#print these tables to text files
write.table(final, file = \"$_\_SexBiasResults.txt\", quote = F, row.names = F)";


	#Finally Run Rscript
	system "Rscript SleuthSexBias/$_/Rscript.txt";


	}
