# R code for Figure 2 - dosage upregulation of 1:1 genes


## Load data

We need:
- table with all tissue expression for 1:1 genes
- table with all tissue expression for 1:2 genes

<details><summary>R code for loading data</summary>

```
#set working directory to directory where "carp2022_upload" is
setwd("~/Documents/Will/Carp/Carp2022/")
library("dplyr")                          # Load dplyr package

 #get set of 1:1 orthologs
orth1<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to1_exhaust.txt")
#get set of 1:2 orthologs
orth2<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to2_exhaust_correctChrom.txt", head=F, sep=" ")
colnames(orth1)<-c("orthogroup", "barbgene", "carpgene", "chr", "subgenome")
colnames(orth2)<-c("orthogroup", "barbgene", "carpA", "chrA", "carpB", "chrB")

#Get expression
bbrain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Brain_DEseq2.txt", head=T, sep=" ")
bgonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Gonad_DEseq2.txt", head=T, sep=" ")
bheart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Heart_DEseq2.txt", head=T, sep=" ")
bliver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Liver_DEseq2.txt", head=T, sep=" ")
bspleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Spleen_DEseq2.txt", head=T, sep=" ")
brain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Brain_DEseq2.txt", head=T, sep=" ")
gonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Gonad_DEseq2.txt", head=T, sep=" ")
heart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Heart_DEseq2.txt", head=T, sep=" ")
liver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Liver_DEseq2.txt", head=T, sep=" ")
spleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Spleen_DEseq2.txt", head=T, sep=" ")

#make brain tables

brain1 <- inner_join(orth1[,1:2], bbrain, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]     
brain1a <- inner_join(orth1[,c(1,3)], brain, by = c("carpgene" = "Gene"))[,c(2,4,5,12,13)]

brain2 <- inner_join(orth2[,1:2], bbrain, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
brain2a <- inner_join(orth2[,c(1,3)], brain, by = c("carpA" = "Gene"))[,c(2,4,5,12,13)]
brain2b <- inner_join(orth2[,c(1,5)], brain, by = c("carpB" = "Gene"))[,c(2,4,5,12,13)]

#make gonad tables

gonad1 <- inner_join(orth1[,1:2], bgonad, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
gonad1a <- inner_join(orth1[,c(1,3)], gonad, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

gonad2 <- inner_join(orth2[,1:2], bgonad, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
gonad2a <- inner_join(orth2[,c(1,3)], gonad, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
gonad2b <- inner_join(orth2[,c(1,5)], gonad, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]

#make heart tables

heart1<-inner_join(orth1[,1:2], bheart, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
heart1a<-inner_join(orth1[,c(1,3)], heart, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

heart2<-inner_join(orth2[,1:2], bheart, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
heart2a<-inner_join(orth2[,c(1,3)], heart, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
heart2b<-inner_join(orth2[,c(1,5)], heart, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]
#make liver tables
liver1<-inner_join(orth1[,1:2], bliver, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
liver1a<-inner_join(orth1[,c(1,3)], liver, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

liver2<-inner_join(orth2[,1:2], bliver, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
liver2a<-inner_join(orth2[,c(1,3)], liver, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
liver2b<-inner_join(orth2[,c(1,5)], liver, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]
#make spleen tables
spleen1<-inner_join(orth1[,1:2], bspleen, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
spleen1a<-inner_join(orth1[,c(1,3)], spleen, by = c("carpgene" = "Gene"))[,c(2,4,5,12,13)]

spleen2<-inner_join(orth2[,1:2], bspleen, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
spleen2a<-inner_join(orth2[,c(1,3)], spleen, by = c("carpA" = "Gene"))[,c(2,4,5,12,13)]
spleen2b<-inner_join(orth2[,c(1,5)], spleen, by = c("carpB" = "Gene"))[,c(2,4,5,12,13)]

```

</details>


<details><summary>R code for making UNNORMALIZED expression table with all tissues/species</summary>

```
all<-as.data.frame(cbind(brain2$meanF, brain2a$meanF, brain2b$meanF, gonad2$meanF, gonad2a$meanF, gonad2b$meanF, heart2$meanF, heart2a$meanF, heart2b$meanF,liver2$meanF, liver2a$meanF, liver2b$meanF, spleen2$meanF, spleen2a$meanF, spleen2b$meanF, brain2$meanM, brain2a$meanM, brain2b$meanM, gonad2$meanM, gonad2a$meanM, gonad2b$meanM, heart2$meanM, heart2a$meanM, heart2b$meanM,liver2$meanM, liver2a$meanM, liver2b$meanM, spleen2$meanM, spleen2a$meanM, spleen2b$meanM))
colnames(all)<-c("barb_brainF", "subA_brainF", "subB_brainF", "barb_gonadF", "subA_gonadF", "subB_gonadF", "barb_heartF", "subA_heartF", "subB_heartF",  "barb_liverF", "subA_liverF", "subB_liverF",  "barb_spleenF", "subA_spleenF", "subB_spleenF", "barb_brainM", "subA_brainM", "subB_brainM", "barb_gonadM", "subA_gonadM", "subB_gonadM", "barb_heartM", "subA_heartM", "subB_heartM",  "barb_liverM", "subA_liverM",  "subB_liverM",  "barb_spleenM", "subA_spleenM", "subB_spleenM")

###Normalize
#bolFMat<-as.matrix(all, nrow = nrow(all), ncol = ncol(all))
#library(NormalyzerDE)
#temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
#rownames(temp2)<-rownames(all)
#colnames(temp2)<-colnames(all)
#all <-as.data.frame(temp2)
#rownames(all)<-brain2$barbgene

all1<-as.data.frame(cbind(brain1$meanF, brain1a$meanF, gonad1$meanF, gonad1a$meanF, heart1$meanF, heart1a$meanF, liver1$meanF, liver1a$meanF, spleen1$meanF, spleen1a$meanF, brain1$meanM, brain1a$meanM, gonad1$meanM, gonad1a$meanM, heart1$meanM, heart1a$meanM, liver1$meanM, liver1a$meanM, spleen1$meanM, spleen1a$meanM))
colnames(all1)<-c("barb_brainF", "subA_brainF",  "barb_gonadF", "subA_gonadF", "barb_heartF", "subA_heartF",  "barb_liverF", "subA_liverF", "barb_spleenF", "subA_spleenF", "barb_brainM", "subA_brainM",  "barb_gonadM", "subA_gonadM", "barb_heartM", "subA_heartM", "barb_liverM", "subA_liverM",  "barb_spleenM", "subA_spleenM")


###Normalize
#bolFMat<-as.matrix(all1, nrow = nrow(all1), ncol = ncol(all1))
#library(NormalyzerDE)
#temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
#rownames(temp2)<-rownames(all1)
#colnames(temp2)<-colnames(all1)
#all1 <-as.data.frame(temp2)
#rownames(all1)<-brain1$barbgene

```

</details>

## Plot for each tissue

Get mean Log2(carp/barb) for subA, subB, A+B, 1:1:

```
 #subA
all$meanA_F<-rowMeans(cbind(log2(all$subA_brainF/all$barb_brainF), log2(all$subA_gonadF/all$barb_gonadF), log2(all$subA_heartF/all$barb_heartF), log2(all$subA_liverF/all$barb_liverF), log2(all$subA_spleenF/all$barb_spleenF)))
all$meanA_M<-rowMeans(cbind(log2(all$subA_brainM/all$barb_brainM), log2(all$subA_gonadM/all$barb_gonadM), log2(all$subA_heartM/all$barb_heartM), log2(all$subA_liverM/all$barb_liverM), log2(all$subA_spleenM/all$barb_spleenM)))
#subB
all$meanB_F<-rowMeans(cbind(log2(all$subB_brainF/all$barb_brainF), log2(all$subB_gonadF/all$barb_gonadF), log2(all$subB_heartF/all$barb_heartF), log2(all$subB_liverF/all$barb_liverF), log2(all$subB_spleenF/all$barb_spleenF)))
all$meanB_M<-rowMeans(cbind(log2(all$subB_brainM/all$barb_brainM), log2(all$subB_gonadM/all$barb_gonadM), log2(all$subB_heartM/all$barb_heartM), log2(all$subB_liverM/all$barb_liverM), log2(all$subB_spleenM/all$barb_spleenM)))

#subA+B
all$meanAB_F<-rowMeans(cbind(log2((all$subA_brainF+all$subB_brainF)/all$barb_brainF), log2((all$subA_gonadF+all$subB_gonadF)/all$barb_gonadF), log2((all$subA_heartF+all$subB_heartF)/all$barb_heartF), log2((all$subA_liverF+all$subB_liverF)/all$barb_liverF), log2((all$subA_spleenF+all$subB_spleenF)/all$barb_spleenF)))
all$meanAB_M<-rowMeans(cbind(log2((all$subA_brainM+all$subB_brainM)/all$barb_brainM), log2((all$subA_gonadM+all$subB_gonadM)/all$barb_gonadM), log2((all$subA_heartM+all$subB_heartM)/all$barb_heartM), log2((all$subA_liverM+all$subB_liverM)/all$barb_liverM), log2((all$subA_spleenM+all$subB_spleenM)/all$barb_spleenM)))

#1:1
all1$meanA_F<-rowMeans(cbind(log2(all1$subA_brainF/all1$barb_brainF), log2(all1$subA_gonadF/all1$barb_gonadF), log2(all1$subA_heartF/all1$barb_heartF), log2(all1$subA_liverF/all1$barb_liverF), log2(all1$subA_spleenF/all1$barb_spleenF)))
all1$meanA_M<-rowMeans(cbind(log2(all1$subA_brainM/all1$barb_brainM), log2(all1$subA_gonadM/all1$barb_gonadM), log2(all1$subA_heartM/all1$barb_heartM), log2(all1$subA_liverM/all1$barb_liverM), log2(all1$subA_spleenM/all1$barb_spleenM)))

```

Plot the distributions:

```
dev.new(width=6, height=5)
 par(mar=c(3,3,2,0.2))
par(mgp=c(2,1,0))

par(mfrow=c(1,2))
boxcols<-c( "#A6CEE3", "#B2DF8A", "#1F78B4", "#FF7F00")
boxnames<-c("subA", "subB", "A+B", "1:1")
boxplot(all$meanA_F, all$meanB_F, all$meanAB_F, all1$meanA_F, notch=T, outline=F, names=boxnames, col=boxcols, main="A. Female tissues", ylab="Mean Log2(Carp/Barb)")
abline(h=median(all$meanAB_F, na.rm=T), lty=2)

ycoord<-(quantile(all1$meanA_F, 0.75, na.rm=T)-quantile(all1$meanA_F, 0.25, na.rm=T))*1.5+quantile(all1$meanA_F, 0.75, na.rm=T)-0.03
segments(1, ycoord, 4, ycoord, col="darkgrey")
#add stats
ptable<-cbind(wilcox.test(all$meanA_F, all1$meanA_F)$p.value, wilcox.test(all$meanB_F, all1$meanA_F)$p.value, wilcox.test(all$meanAB_F, all1$meanA_F)$p.value)
ptable 
ptable[ ptable <0.001 ] <- "***"
ptable[ ptable > 0 & ptable <0.01 ] <- "**"
ptable[ ptable > 0 & ptable <0.05 ] <- "*"
ptable[ ptable > 0 & ptable >0.05 ] <- " "
ptable 

text(1,ycoord-0.35, ptable[1], srt = 90, cex=1.3)
text(2,ycoord-0.35, ptable[2], srt = 90, cex=1.3)
text(3,ycoord-0.35, ptable[3], srt = 90, cex=1.3)
boxplot(all$meanA_M, all$meanB_M, all$meanAB_M, all1$meanA_M, notch=T, outline=F, names=boxnames, col=boxcols, main="B. Male tissues", ylab="Mean Log2(Carp/Barb)")
abline(h=median(all$meanAB_M, na.rm=T), lty=2)
ycoord<-(quantile(all1$meanA_M, 0.75, na.rm=T)-quantile(all1$meanA_M, 0.25, na.rm=T))*1.5+quantile(all1$meanA_M, 0.75, na.rm=T)-0.01
segments(1, ycoord, 4, ycoord, col="darkgrey")
#add stats
ptable<-cbind(wilcox.test(all$meanA_M, all1$meanA_M)$p.value, wilcox.test(all$meanB_M, all1$meanA_M)$p.value, wilcox.test(all$meanAB_M, all1$meanA_M)$p.value)
ptable 
ptable[ ptable <0.001 ] <- "***"
ptable[ ptable > 0 & ptable <0.01 ] <- "**"
ptable[ ptable > 0 & ptable <0.05 ] <- "*"
ptable[ ptable > 0 & ptable >0.05 ] <- " "
ptable 

text(1,ycoord-0.35, ptable[1], srt = 90, cex=1.3)
text(2,ycoord-0.35, ptable[2], srt = 90, cex=1.3)
text(3,ycoord-0.35, ptable[3], srt = 90, cex=1.3)
```
