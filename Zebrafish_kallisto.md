# Estimate gene expression for zebrafish genes

WD: /nfs/scistore18/vicosgrp/bvicoso/FishFun/Carp2022/6-Zebrafish/1-Expression

## Get and clean transcriptome to match orthofinder input (ADD TO "FILES" PAGE BEFORE SUBMISSION)

```
wget http://ftp.ensembl.org/pub/release-104/fasta/danio_rerio/cds/Danio_rerio.GRCz11.cds.all.fa.gz


gzip -d Danio_rerio.GRCz11.cds.all.fa.gz

cat Danio_rerio.GRCz11.cds.all.fa | perl -pi -e 's/>.*gene:/>/gi' | perl -pi -e 's/ .*//gi' | perl -pi -e 's/\n/ /gi' | perl -pi -e 's/>/\n>/gi' | sort | perl -pi -e 's/ /\n/gi' | perl -pi -e 's/^\n//gi' > Danio_rerio_cleanCDS.fa
perl ~/GetLongestCDS.pl Danio_rerio_cleanprot.fa

#first > missing
perl -pi -e 's/ENSDARG00000000001.6/>ENSDARG00000000001.6/gi' Danio_rerio_cleanCDS.fa.longestCDS 
mv Danio_rerio_cleanCDS.fa.longestCDS Danio_rerio_CDSref.fa
```

Get only transcripts that are included in orthogroups:

```
cat /nfs/scistore18/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthogroups/Orthogroups.tsv | perl -pi -e 's/\t/\n/gi' | perl -pi -e 's/, /\n/gi' | grep 'ENSDARG' | sort > Drer_genes_in_ortho.joinable
/nfs/scistore18/vicosgrp/bvicoso/seqtk/seqtk subseq ~/FishFun/Carp2022/6-Zebrafish/1-Expression/Danio_rerio_CDSref.fa Drer_genes_in_ortho.joinable > Drerio_orthoRNA.fa
```



## Run Kallisto

### Get automated 

```
cp ~/MatZygX_2022/5-Daphnia/2-Expression/Siliconer_Daph.pl .
cp ~/MatZygX_2022/5-Daphnia/2-Expression/RunSiliconer_Daph.sh .
```

Make Kalfiles directory:

```
mkdir KalFiles
```

### Make lists of reads to get expression for:

ReadsList1.txt (contains male and female data):

```
SRR10868193
SRR10868194
SRR10868195
SRR10868196
SRR10868197
SRR10868207
SRR10868208
SRR10868209
SRR10868210
SRR8286602
SRR8286603
SRR8286604
SRR8286605
```

ReadsList2.txt (unsexed):

```
DRR162467
DRR162468
DRR162469
DRR162470
DRR162471
DRR162487
DRR162488
DRR162489
DRR162490
DRR162491
DRR162502
DRR162503
DRR162504
DRR162505
DRR162506
DRR162521
DRR162522
DRR162523
DRR162524
DRR162525
DRR162541
DRR162542
DRR162543
DRR162544
DRR162545
```

### Get TPM

```
#load any module you need here
module load kallisto

#run commands on SLURM's srun
kallisto index -i Drer.idx Drerio_orthoRNA.fa
```


```
#load any module you need here
module load kallisto
module load SRA-Toolkit


#run commands on SLURM's srun
perl Siliconer_Drer.pl ReadsList1.txt
```


```
#load any module you need here
module load kallisto
module load SRA-Toolkit


#run commands on SLURM's srun
perl Siliconer_Drer.pl ReadsList2.txt
```
