# Patterns of ancestral expression of diploidized and tetraploid genes

## Load data

Let's merge:
- the list of barb genes that have either 1 or 2 orthologs in carp
- patterns of barb expression in different tissues

<details><summary>R code</summary>

```
#set working directory to directory where "carp2022_upload" is
setwd("~/Documents/Will/Carp/Carp2022/")

 #get set of 1:1 orthologs
orth1<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to1_exhaust.txt")
#get set of 1:2 orthologs
orth2<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to2_exhaust_correctChrom.txt", head=F, sep=" ")
colnames(orth1)<-c("orthogroup", "barbgene", "carpgene", "chr", "subgenome")
colnames(orth2)<-c("orthogroup", "barbgene", "carpA", "chrA", "carpB", "chrB")

#Get expression
brain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Brain_DEseq2.txt", head=T, sep=" ")
gonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Gonad_DEseq2.txt", head=T, sep=" ")
heart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Heart_DEseq2.txt", head=T, sep=" ")
liver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Liver_DEseq2.txt", head=T, sep=" ")
spleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Spleen_DEseq2.txt", head=T, sep=" ")

#make brain tables
brain1<-merge(orth1[,1:2], brain, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
brain2<-merge(orth2[,1:2], brain, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
#make gonad tables
gonad1<-merge(orth1[,1:2], gonad, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
gonad2<-merge(orth2[,1:2], gonad, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
#make heart tables
heart1<-merge(orth1[,1:2], heart, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
heart2<-merge(orth2[,1:2], heart, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
#make liver tables
liver1<-merge(orth1[,1:2], liver, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
liver2<-merge(orth2[,1:2], liver, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
#make spleen tables
spleen1<-merge(orth1[,1:2], spleen, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
spleen2<-merge(orth2[,1:2], spleen, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
```

</details>

Now plot!

## 1. Ancestral patterns of expression 

<details>

<summary>R code</summary>

```
#boxplot ancestral expression
par(mfrow=c(2,6))
par(mar=c(4,3,4,0))
boxnames=c("1:1", "1:2")
boxcols=c("#B2DF8A", "#33A02C")
#brain
boxplot(log2(brain1$meanF), log2(brain2$meanF), outline=F, names=boxnames, notch=T, col=boxcols, main="Brain F")
pval<-wilcox.test(log2(brain1$meanF), log2(brain2$meanF))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}

boxplot(log2(brain1$meanM), log2(brain2$meanM), outline=F, names=boxnames, notch=T, col=boxcols, main="Brain M")
pval<-wilcox.test(log2(brain1$meanM), log2(brain2$meanM))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}
#heart
boxplot(log2(heart1$meanF), log2(heart2$meanF), outline=F, names=boxnames, notch=T, col=boxcols, main="Heart F")
pval<-wilcox.test(log2(heart1$meanM), log2(heart2$meanM))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}
boxplot(log2(heart1$meanM), log2(heart2$meanM), outline=F, names=boxnames, notch=T, col=boxcols, main="Heart M")
pval<-wilcox.test(log2(heart1$meanM), log2(heart2$meanM))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}
#spleen
boxplot(log2(spleen1$meanF), log2(spleen2$meanF), outline=F, names=boxnames, notch=T, col=boxcols, main="Spleen F")
pval<-wilcox.test(log2(spleen1$meanM), log2(spleen2$meanM))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}
boxplot(log2(spleen1$meanM), log2(spleen2$meanM), outline=F, names=boxnames, notch=T, col=boxcols, main="Spleen M")
pval<-wilcox.test(log2(spleen1$meanM), log2(spleen2$meanM))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}
#liver
boxplot(log2(liver1$meanF), log2(liver2$meanF), outline=F, names=boxnames, notch=T, col=boxcols, main="Liver F")
pval<-wilcox.test(log2(liver1$meanM), log2(liver2$meanM))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}
boxplot(log2(liver1$meanM), log2(liver2$meanM), outline=F, names=boxnames, notch=T, col=boxcols, main="Liver M")
pval<-wilcox.test(log2(liver1$meanM), log2(liver2$meanM))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}
#gonad
boxplot(log2(gonad1$meanF), log2(gonad2$meanF), outline=F, names=boxnames, notch=T, col=boxcols, main="Gonad F")
pval<-wilcox.test(log2(gonad1$meanM), log2(gonad2$meanM))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}
boxplot(log2(gonad1$meanM), log2(gonad2$meanM), outline=F, names=boxnames, notch=T, col=boxcols, main="Gonad M")
pval<-wilcox.test(log2(gonad1$meanM), log2(gonad2$meanM))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}
```
</details>

### Key results

1:1 genes have lower expression than 1:2 genes in most (brain, heart, spleen) but not all (gonad, liver) tissues. 

<img src="/images/ancestralexpression.jpg" width=700>

**Figure:** The distribution of expression (in Log2(TPM)) for genes with 1 and 2 orthologs in carp. Asterisks denote significant differences between the two categories (*p<0.05, **p<0.01, ***p<0.001).

## 2. Ancestral patterns of sex bias 

### Code 

<details>

<summary>R code</summary>

```
#boxplot ancestral expression
par(mfrow=c(1,5))
par(mar=c(4,3,4,0))
boxnames=c("1:1", "1:2")
boxcols=c("#B2DF8A", "#33A02C")
#brain
boxplot(abs(brain1$log2FoldChange), abs(brain2$log2FoldChange), outline=F, names=boxnames, notch=T, col=boxcols, main="Brain F")
pval<-wilcox.test(abs(brain1$log2FoldChange), abs(brain2$log2FoldChange))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}


#heart
boxplot(abs(heart1$log2FoldChange), abs(heart2$log2FoldChange), outline=F, names=boxnames, notch=T, col=boxcols, main="Heart F")
pval<-wilcox.test(abs(heart1$log2FoldChange), abs(heart2$log2FoldChange))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}


#spleen
boxplot(abs(spleen1$log2FoldChange), abs(spleen2$log2FoldChange), outline=F, names=boxnames, notch=T, col=boxcols, main="Spleen F")
pval<-wilcox.test(abs(spleen1$log2FoldChange), abs(spleen2$log2FoldChange))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}


#liver
boxplot(abs(liver1$log2FoldChange), abs(liver2$log2FoldChange), outline=F, names=boxnames, notch=T, col=boxcols, main="Liver F")
pval<-wilcox.test(abs(liver1$log2FoldChange), abs(liver2$log2FoldChange))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}


#gonad
boxplot(abs(gonad1$log2FoldChange), abs(gonad2$log2FoldChange), outline=F, names=boxnames, notch=T, col=boxcols, main="Gonad F")
pval<-wilcox.test(abs(gonad1$log2FoldChange), abs(gonad2$log2FoldChange))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}


```
</details>

### Key results

1:1 genes have higher sex-bias than 1:2 genes in every tissue

<img src="/images/ancestralsexbias.jpg" width=700>

**Figure:** The distribution of sex-bias (estimated as the absolute value of log2(M/F)) for genes with 1 and 2 orthologs in carp. Asterisks denote significant differences between the two categories (*p<0.05, **p<0.01, ***p<0.001).


## 3. Ancestral patterns of tissue specificity

We need to make a table that has the mean expression (and log2M:F) for all tissues, then calculate tau:

```
(sum[i from 1 to n](1-Norm_i))/(n-1)
```

Where: 

    * xi is the expression in tissue i
    * n is the number of tissues
    * Norm_i = xi/(max(xi))

According to [this paper](https://academic.oup.com/bib/article/18/2/205/2562739): _In general, quantile normalization has no influence on the results of calculation of tissue specificity (Supplementary Figures S74 and S75). Expectedly, removing log-transformation has a greater influence on all parameters, in the direction of detecting more tissue-specificity, sometimes losing completely the signal of broad expression, e.g. Tau (Supplementary Figure S68). Moreover, in the absence of log-transformation, the correlations between subsets of tissues or between species are in general weaker (Supplementary Figures S69–S70)._

So let's calculate Tau based on log2(TPM), but normalize anyway to make calculations of mean expression more meaningful.

### Code

<details><summary>R code for calculating Tau</summary>

```
#simplify expression tables and use log2(tpm)

sbrain<-brain[,c(1,3,11,12)]
sbrain$meanF<-log2(sbrain$meanF)
sbrain$meanM<-log2(sbrain$meanM)
colnames(sbrain)<-c("Gene", "log2brain", "Fbrain", "Mbrain")

sheart<-heart[,c(1,3,11,12)]
sheart$meanF<-log2(sheart$meanF)
sheart$meanM<-log2(sheart$meanM)
colnames(sheart)<-c("Gene", "log2heart", "Fheart", "Mheart")

sspleen<-spleen[,c(1,3,11,12)]
sspleen$meanF<-log2(sspleen$meanF)
sspleen$meanM<-log2(sspleen$meanM)
colnames(sspleen)<-c("Gene", "log2spleen", "Fspleen", "Mspleen")

sliver<-liver[,c(1,3,11,12)]
sliver$meanF<-log2(sliver$meanF)
sliver$meanM<-log2(sliver$meanM)
colnames(sliver)<-c("Gene", "log2liver", "Fliver", "Mliver")

sgonad<-gonad[,c(1,3,11,12)]
sgonad$meanF<-log2(sgonad$meanF)
sgonad$meanM<-log2(sgonad$meanM)
colnames(sgonad)<-c("Gene", "log2gonad", "Fgonad", "Mgonad")

#Merge tables
big1<-merge(sbrain, sheart, by.x="Gene", by.y="Gene")
big2<-merge(big1, sspleen, by.x="Gene", by.y="Gene")
big3<-merge(big2, sliver, by.x="Gene", by.y="Gene")
big4<-merge(big3, sgonad, by.x="Gene", by.y="Gene")

#####select F tissues and calculate tau 
#select female tissues
fem<-big4[,grep("F", colnames(big4))]
rownames(fem)<-big4$Gene

###Normalize
bolFMat<-as.matrix(fem, nrow = nrow(fem), ncol = ncol(fem))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(fem)
colnames(temp2)<-colnames(fem)
fem<-temp2

#replace negative numbers with 0 (see tau paper)
fem[fem < 0] <- 0
fem<-as.data.frame(fem)
#find maximum expression for each gene
fem$max<-pmax(fem$Fbrain, fem$Fheart, fem$Fspleen, fem$Fliver, fem$Fgonad)
#normalize the expression by the maximum expression of each gene
fem2<-fem/fem$max 
#Get rid of last column
fem2<-fem2[,1:(ncol(fem2)-1)]
#Calculate tau 
fem2<-(1-fem2)
fem2$tau<-rowSums(fem2)/(5-1)
fem$tau<-fem2$tau


#####select M tissues and calculate tau 
#select male tissues
male<-big4[,grep("M", colnames(big4))]
rownames(male)<-big4$Gene

###Normalize
bolFMat<-as.matrix(male, nrow = nrow(male), ncol = ncol(male))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(male)
colnames(temp2)<-colnames(male)
male<-temp2

#replace negative numbers with 0 (see tau paper)
male[male < 0] <- 0
male<-as.data.frame(male)
#find maximum expression for each gene
male$max<-pmax(male$Mbrain, male$Mheart, male$Mspleen, male$Mliver, male$Mgonad)
#normalize the expression by the maximum expression of each gene
male2<-male/male$max 
#Get rid of last column
male2<-male2[,1:(ncol(male2)-1)]
#Calculate tau 
male2<-(1-male2)
male2$tau<-rowSums(male2)/(5-1)
male$tau<-male2$tau
```

</details>

As a sanity check, we can plot the correlations between expression in all tissues and make a heatmap. Everything makes sense (respective male and female tissues cluster, with cor>0.9 between M&F for somatic tissues but only 0.7 for gonads) so the figure (and code) is hidden below. 

<details><summary>R code for heatmap and table of correlations</summary>

```
##Heatmap
#store list of colors in variable "crazycols" 
crazycols<-c("pink", "pink", " pink", "pink", " pink", " lightgreen", " lightgreen", " lightgreen", " lightgreen", " lightgreen")
#tell R to use the library gplots
library(gplots)

#draw the heatmap
heatmap.2(cor(cor(cbind(fem[,1:5], male[,1:5])), method="spearman"), col= colorRampPalette(c("blue", "white", "red", "firebrick4"))(15), ColSideColors=crazycols, RowSideColors = crazycols, scale="none", symm=T, margins = c(15,15), key=T, trace="none")

#add legend
legend("topright",      # location of the legend on the heatmap plot
    legend = c("female", "male."), # category labels
    col = c("pink", "lightgreen"),  # color key
    lty= 1,             # line style
    lwd = 10            # line width)
)

###table of correlations
cor(cbind(fem[,1:5], male[,1:5]), method="spearman")
```

<img src="/images/heatmap_barb_tissues.jpg" width=600>

</details>

Now plot!

<details><summary>Code for plotting</summary>

```
#make tau tables
ftau1<-merge(orth1[,1:2], fem, by.x="barbgene", by.y=0)
ftau2<-merge(orth2[,1:2], fem, by.x="barbgene", by.y=0)
#make tau tables
mtau1<-merge(orth1[,1:2], male, by.x="barbgene", by.y=0)
mtau2<-merge(orth2[,1:2], male, by.x="barbgene", by.y=0)

#boxplot ancestral tau
par(mfrow=c(1,2))
par(mar=c(4,3,4,0.2))
boxnames=c("1:1", "1:2")
#female
boxcols=c("#FB9A99", "#E31A1C")
boxplot(abs(ftau1$tau), abs(ftau2$tau), outline=F, names=boxnames, notch=T, col=boxcols, main="Female tissues")
pval<-wilcox.test(abs(ftau1$tau), abs(ftau2$tau))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}
#male
boxcols=c("#A6CEE3", "#1F78B4")
boxplot(abs(mtau1$tau), abs(mtau2$tau), outline=F, names=boxnames, notch=T, col=boxcols, main="Male tissues")
pval<-wilcox.test(abs(mtau1$tau), abs(mtau2$tau))$p.value
if (pval<0.001) {
mtext("***", side=3, cex=2, padj=0.4)
} else if (pval<0.01) {
mtext("**", side=3, cex=2, padj=0.4)
} else if ( pval<0.05) {
mtext("*", side=3, cex=2, padj=0.4)
} else {
###
}

```

</details>

### Key results

1:1 genes have higher tau than 1:2 genes. 

<img src="/images/ancestraltau.jpg" width=200>

**Figure:** The distribution of Tau for genes with 1 and 2 orthologs in carp. Asterisks denote significant differences between the two categories (*p<0.05, **p<0.01, ***p<0.001).

## 4. The influence of specific tissues on rediploidization

First, let's figure out which male and female tissue genes are primarily expressed in:

<details><summary>Classifying tissues by which tissue they are primarily expressed in</summary>

```
 #male
 male2<-male/(male$max)
 male2$maxtissue<-"NA"
male2$tau<-male$tau


 maleb<-subset(male2, Mbrain==1)
 maleb$maxtissue<-"brain"
 maleh<-subset(male2, Mheart==1)
 maleh$maxtissue<-"heart"
 males<-subset(male2, Mspleen==1)
 males$maxtissue<-"spleen"
 malel<-subset(male2, Mliver==1)
 malel$maxtissue<-"liver"
 maleg<-subset(male2, Mgonad==1)
 maleg$maxtissue<-"gonad"

male3<-rbind(maleb, maleh, males, malel, maleg)


#female
fem2<-fem/(fem$max)
 fem2$maxtissue<-"NA"
fem2$tau<-fem$tau

 femb<-subset(fem2, Fbrain==1)
 femb$maxtissue<-"brain"
 femh<-subset(fem2, Fheart==1)
 femh$maxtissue<-"heart"
 fems<-subset(fem2, Fspleen==1)
 fems$maxtissue<-"spleen"
 feml<-subset(fem2, Fliver==1)
 feml$maxtissue<-"liver"
 femg<-subset(fem2, Fgonad==1)
 femg$maxtissue<-"gonad"

fem3<-rbind(femb, femh, fems, feml, femg)
```

</details>

<details><summary>Add ploidy information and break up by tau</summary>

```
#make tau tables
ftau1<-merge(orth1[,1:2], fem3, by.x="barbgene", by.y=0)
ftau2<-merge(orth2[,1:2], fem3, by.x="barbgene", by.y=0)
#make tau tables
mtau1<-merge(orth1[,1:2], male3, by.x="barbgene", by.y=0)
mtau2<-merge(orth2[,1:2], male3, by.x="barbgene", by.y=0)

#for each tau check number of genes primarily expressed in each tissue
#male
mtau1_1<-subset(mtau1, tau<0.2)
 mtau1_2<-subset(mtau1, tau>0.2 & tau<0.4)
 mtau1_3<-subset(mtau1, tau>0.4 & tau<0.6)
 mtau1_4<-subset(mtau1, tau>0.6 & tau<0.8)
 mtau1_5<-subset(mtau1, tau>0.8)

mtau2_1<-subset(mtau2, tau<0.2)
 mtau2_2<-subset(mtau2, tau>0.2 & tau<0.4)
 mtau2_3<-subset(mtau2, tau>0.4 & tau<0.6)
 mtau2_4<-subset(mtau2, tau>0.6 & tau<0.8)
 mtau2_5<-subset(mtau2, tau>0.8)

maletable1<-rbind(table(mtau1_1$maxtissue),
table(mtau1_2$maxtissue),
table(mtau1_3$maxtissue),
table(mtau1_4$maxtissue),
table(mtau1_5$maxtissue))

maletable2<-rbind(table(mtau2_1$maxtissue),
table(mtau2_2$maxtissue),
table(mtau2_3$maxtissue),
table(mtau2_4$maxtissue),
table(mtau2_5$maxtissue))

#female
ftau1_1<-subset(ftau1, tau<0.2)
 ftau1_2<-subset(ftau1, tau>0.2 & tau<0.4)
 ftau1_3<-subset(ftau1, tau>0.4 & tau<0.6)
 ftau1_4<-subset(ftau1, tau>0.6 & tau<0.8)
 ftau1_5<-subset(ftau1, tau>0.8)

ftau2_1<-subset(ftau2, tau<0.2)
 ftau2_2<-subset(ftau2, tau>0.2 & tau<0.4)
 ftau2_3<-subset(ftau2, tau>0.4 & tau<0.6)
 ftau2_4<-subset(ftau2, tau>0.6 & tau<0.8)
 ftau2_5<-subset(ftau2, tau>0.8)

femtable1<-rbind(table(ftau1_1$maxtissue),
table(ftau1_2$maxtissue),
table(ftau1_3$maxtissue),
table(ftau1_4$maxtissue),
table(ftau1_5$maxtissue))

femtable2<-rbind(table(ftau2_1$maxtissue),
table(ftau2_2$maxtissue),
table(ftau2_3$maxtissue),
table(ftau2_4$maxtissue),
table(ftau2_5$maxtissue))

#% rediploidized as a function of Tau
femlost<-t(femtable1/(femtable1+femtable2))
colnames(femlost)<-c("<0.2", "0.2-0.4", "0.4-0.6", "0.6-0.8", ">0.8")
malelost<-t(maletable1/(maletable1+maletable2))
colnames(malelost)<-c("<0.2", "0.2-0.4", "0.4-0.6", "0.6-0.8", ">0.8")
```
</details>

<details><summary>Plot % rediploidized as a function of tau</summary>

```
par(mfrow=c(1,2))
par(mar=c(4,4,3,0.5))
plot(malelost[1,], ylim=c(0.3,max(rbind(malelost, femlost))+0.1), ylab="% rediploidized", col="white", xlab="tau category", main="Male tissues", cex.axis=1, cex.lab=1.2, col.axis="white", xlim=c(1,5.2))
 lines(malelost[1,], col="#1F78B4", lwd=3)
 lines(malelost[2,], col="#E31A1C", lwd=3)
 lines(malelost[3,], col="#B2DF8A", lwd=3)
 lines(malelost[4,], col="#FDBF6F", lwd=3)
 lines(malelost[5,], col="#CAB2D6", lwd=3)
axis(1, at=c(1,2,3,4,5),labels=c("<0.2","0.2-0.4", "0.4-0.6", "0.6-0.8", ">0.8"), col.axis="black", las=1)
axis(2, at=c(0.1,0.2,0.3,0.4,0.5,0.6,0.7),labels=c("0.1","0.2","0.3","0.4","0.5","0.6","0.7"), col.axis="black", las=1)
#plot(0,type='n',axes=FALSE,ann=FALSE)
legend("topleft", legend=rownames(femlost),
       col=c("#1F78B4", "#E31A1C", "#B2DF8A", "#FDBF6F", "#CAB2D6"), lwd=3, cex=1, bty = "n")

#test for significance with 2x5 chi-square
for (val in 1:5)
{
  pval<-chisq.test(rbind(maletable1[val,], maletable2[val,]))$p.value
print(pval)
if (pval<0.001) {
text(val, 0.35, "***", cex=1.5)
} else if (pval<0.01) {
text(val, 0.35, "**", cex=1.5)
} else if ( pval<0.05) {
text(val, 0.35, "**", cex=1.5)
} else {
###
}
}

plot(femlost[1,], ylim=c(0.3,max(rbind(malelost, femlost))+0.1), ylab="", col="white", xlab="tau category", main="Female tissues", cex.axis=1, cex.lab=1.2, col.axis="white", xlim=c(1,5.2))
 lines(femlost[1,], col="#1F78B4", lwd=3)
 lines(femlost[2,], col="#E31A1C", lwd=3)
 lines(femlost[3,], col="#B2DF8A", lwd=3)
 lines(femlost[4,], col="#FDBF6F", lwd=3)
  lines(femlost[5,], col="#CAB2D6", lwd=3)
axis(1, at=c(1,2,3,4,5),labels=c("<0.2","0.2-0.4", "0.4-0.6", "0.6-0.8", ">0.8"), col.axis="black", las=1)
axis(2, at=c(0.1,0.2,0.3,0.4,0.5,0.6,0.7),labels=c("0.1","0.2","0.3","0.4","0.5","0.6","0.7"), col.axis="black", las=1)

#plot(0,type='n',axes=FALSE,ann=FALSE)
legend("topleft", legend=rownames(femlost),
       col=c("#1F78B4", "#E31A1C", "#B2DF8A", "#FDBF6F", "#CAB2D6"), lwd=3, cex=1, bty = "n")
#test for significance with 2x5 chi-square
for (val in 1:5)
{
  pval<-chisq.test(rbind(femtable1[val,], femtable2[val,]))$p.value
print(pval)
if (pval<0.001) {
text(val, 0.35, "***", cex=1.5)
} else if (pval<0.01) {
text(val, 0.35, "**", cex=1.5)
} else if ( pval<0.05) {
text(val, 0.35, "**", cex=1.5)
} else {
###
}
}
```

</details>

### Key results 

While genes with higher tau in most tissues are more likely to be rediploidized, this is very heterogeneous across tissues. Brain-biased genes tend to be maintained in two copies even when they are very specific, whereas liver and testis-biased genes often become single copy.

<img src="/images/specifictissues.jpg" width=700>

**Figure:** The percentage of genes that became rediploidized as a function of tau, for genes that are expressed primarily in each of the 5 tissues. 


