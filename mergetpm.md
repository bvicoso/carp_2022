# Make tables with both barb and carp expression

Since these tables will be different for each analysis, let's not output them but just set up code for merging expression in general. 

For each tissue, we will need:
* the DEseq2 output file (with TPMs from Kallisto/Sleuth), e.g.: 
```
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/3b-Sleuth_noBadSamples/2-Orthogroups/SleuthSexBias/Barb_Brain/Barb_Brain_DEseq2.txt
```

* a set of 1:1 orthologs:
```
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to1_exhaust.txt
or
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to1_stringent.txt
```

* a set of 1:2 orthologs:
```
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_exhaust_correctChrom.txt
or
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_stringent_correctChrom.txt
```
