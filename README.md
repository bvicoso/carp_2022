# Pipelines for Gammerdinger et al. 2022

## Files
The list of the files that we use is [here](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/files.md).

## Data processing
1. [Barb (Puntius titteya) transcriptome assembly](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/Transcriptome.md)
2. Get set of 1:2 and 1:1 homologs between barb and carp.
    * [Pipeline](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/trios.md)
    * [Some results](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/Trios_key_results.md)
3. Estimate gene expression
    * [Kallisto pipeline](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/Kallisto.md)
4. Get p-value of expression difference between males and females for each gene/tissue.
    * [Sleuth pipeline](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/Sleuth_sexbias.md) (used to extract the TPM tables)
        * Kallisto/Sleuth [key results](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/KalSleuth_results.md) 
    * [tximport and DEseq2](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/tmx_deseq2.md) (used to find differentially expressed genes)
5. Get p-value of expression difference between A and B for each tissue.
    * [tximport and DEseq2](https://gita.ist.ac.at/bvicoso/carp_2022/-/blob/main/deseq_AvsB.md)

## Data analysis
The input files used in the analyses below can be downloaded [here](https://seafile.ista.ac.at/d/08f6ebce88dc452286b3/).
1. [Ancestral characteristics of tetraploid and rediploidized genes](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/ancestral.md)
    * [Code for Figure 1](https://gita.ist.ac.at/bvicoso/carp_2022/-/blob/main/Fig1_ancestralredip.md)
2. [Derived characteristics of tetraploid and rediploidized genes](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/derived.md)
    * [Code for Figure 2](https://gita.ist.ac.at/bvicoso/carp_2022/-/blob/main/Fig2_DC.md)
3. [Classify genes as conserved, neofunctionalized, subfunctionalized.](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/neofunctionalization.md)
    * [Code for Figure 3](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/Fig3_neosub.md)
4. [Plots for tissue and sex subfunctionalization.](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/Sex_sub.md) 
    * [Code for Figure 4](https://git.ista.ac.at/bvicoso/carp_2022/-/blob/main/Fig4_sexsubf.md)

## Colors for plotting

<details><summary>R code for finding nice colors</summary>

```
library("RColorBrewer")
display.brewer.all(n=NULL, type="all", select=NULL, exact.n=TRUE, 
colorblindFriendly=T)
brewer.pal(12,"Paired")
```

</details>

We use blue for male, red for female, green when no sex highlighted and/or ratios, light for diploid genes, dark for tetraploid: 
* light blue: "#A6CEE3"
* dark blue: "#1F78B4"
* light green: "#B2DF8A" 
* dark green: "#33A02C" 
* light red: "#FB9A99" 
* dark red: "#E31A1C"
