# Test for differential expression between males and females

wd: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/3-Sleuth

NB: this currently uses:
R version 4.1.0 (2021-05-18)
sleuth Version 0.30.0

Let's run Sleuth using the three different transcriptomes as the reference (only 2-Orthogroups used for further analysis):

```
mkdir 1-AllTranscripts
mkdir 2-Orthogroups
mkdir 3-Trios
```

Put ReadsList.txt in each:

```
cp ../2-Kallisto/ReadsList.txt 1-AllTranscripts/
cp ../2-Kallisto/ReadsList.txt 2-Orthogroups/
cp ../2-Kallisto/ReadsList.txt 3-Trios/
```

Then run Sleuth_runner.pl in the three directories (had to be modified for each so they copy/output files from/to the correct directories):

```
module load R/4.1.0
perl Sleuth_SexBias_runner1.pl ReadsList.txt
```

Script [here](https://git.ist.ac.at/bvicoso/carp_2022/-/blob/main/scripts/Sleuth_SexBias_runner1.pl).

```
module load R/4.1.0
perl Sleuth_SexBias_runner2.pl ReadsList.txt
```

Script [here](https://git.ist.ac.at/bvicoso/carp_2022/-/blob/main/scripts/Sleuth_SexBias_runner2.pl).

```
module load R/4.1.0
perl Sleuth_SexBias_runner3.pl ReadsList.txt
```

Script [here](https://git.ist.ac.at/bvicoso/carp_2022/-/blob/main/scripts/Sleuth_SexBias_runner2.pl).
