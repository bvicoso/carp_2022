# Check relationship between tau and haploinsufficiency

## Files

Tau values are provided in Sup. Table 2 of:
https://www.aging-us.com/article/202648/text

Insuffiency scores are provided here:
https://github.com/HAShihab/HIPred/blob/master/HIPred.tsv

Explanation: "Predictions are in the range [0, 1]: values above 0.5 are predicted to be haploinsufficient, while those below 0.5 are predicted to be haplosufficient. Scores close to the extremes are the highest-confidence predictions that yield the highest accuracy."

Sadly the first has ensembl IDs, the second gene names, so let's get correspondence from Ensembl Biomart (9/03/2022):

(Get also Carp stable ID and homology type)


Now same using the original scores of Huang (2010)

HI scores are in dataset S2 of https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1001154

Clean it:

```
cat /Users/bvicoso/Documents/Will/Carp2022/HI_tau/Huang.txt | perl -pi -e 's/\|.*%\t/\t/gi' > /Users/bvicoso/Documents/Will/Carp2022/HI_tau/Huang_clean.txt
```


## Compare

```
setwd("~/Downloads/HI_tau")
par(mfrow=c(2,2))
par(mar=c(3,4,4,1))

###HiPred
tau<-read.table("tau.txt", head=T, sep="\t")
HI<-read.table("HIPred.tsv.txt", head=T, sep="\t")
mart<-read.table("mart_export-3.txt", head=T, sep="\t")

big1<-merge(mart[,c(1,2)], HI, by.x="Gene.name", by.y="Gene")
big2<-merge(big1, tau, by.x="Gene.stable.ID", by.y="Gene")

HI<-subset(big2, Prob>0.5)
HS<-subset(big2, Prob<0.5)
boxplot(HI$Tau, HS$Tau, col=c("indianred", "lightblue"), names=c("HaploInsufficient", "HaploSufficient"), ylab="Tau", main="Shihab (2017) - Tissue-Specificity")
wilcox.test(HI$Tau, HS$Tau)
text(1.5, 0.95, "p-value < 2.2e-16")

tau<-read.table("exp.txt", head=T, sep="\t")
HI<-read.table("HIPred.tsv.txt", head=T, sep="\t")
mart<-read.table("mart_export-3.txt", head=T, sep="\t")

big1<-merge(mart[,c(1,2)], HI, by.x="Gene.name", by.y="Gene")
big2<-merge(big1, tau, by.x="Gene.stable.ID", by.y="Gene")

HI<-subset(big2, Prob>0.5)
HS<-subset(big2, Prob<0.5)
boxplot(colMeans(HI[,-c(1,2,3)]), colMeans(HS[,-c(1,2,3)]), col=c("indianred", "lightblue"), names=c("HaploInsufficient", "HaploSufficient"), ylab="Log2(TPM)", outline=F, main="Shihab (2017) - Mean expression")
wilcox.test(colMeans(HI[,-c(1,2,3)]), colMeans(HS[,-c(1,2,3)]))
text(1.8, 4, "p-value = 7.61e-16")

###Huang
tau<-read.table("tau.txt", head=T, sep="\t")
HI<-read.table("Huang_clean.txt", head=T, sep="\t")[,c(4,5)]
mart<-read.table("mart_export-3.txt", head=T, sep="\t")

big1<-merge(mart[,c(1,2)], HI, by.x="Gene.name", by.y="Gene")
big2<-merge(big1, tau, by.x="Gene.stable.ID", by.y="Gene")

HI<-subset(big2, Prob>0.5)
HS<-subset(big2, Prob<0.5)
boxplot(HI$Tau, HS$Tau, col=c("indianred", "lightblue"), names=c("HaploInsufficient", "HaploSufficient"), ylab="Tau", main="Huang (2010) - Tissue-Specificity")
wilcox.test(HI$Tau, HS$Tau)
text(1.5, 0.95, "p-value < 2.2e-16")

tau<-read.table("exp.txt", head=T, sep="\t")
HI<-read.table("Huang_clean.txt", head=T, sep="\t")[,c(4,5)]
mart<-read.table("mart_export-3.txt", head=T, sep="\t")

big1<-merge(mart[,c(1,2)], HI, by.x="Gene.name", by.y="Gene")
big2<-merge(big1, tau, by.x="Gene.stable.ID", by.y="Gene")

HI<-subset(big2, Prob>0.5)
HS<-subset(big2, Prob<0.5)
boxplot(colMeans(HI[,-c(1,2,3)]), colMeans(HS[,-c(1,2,3)]), col=c("indianred", "lightblue"), names=c("HaploInsufficient", "HaploSufficient"), ylab="Log2(TPM)", outline=F, main="Huang (2010) - Mean expression")
wilcox.test(colMeans(HI[,-c(1,2,3)]), colMeans(HS[,-c(1,2,3)]))
text(1.8, 4, "p-value = 2.753e-09")
```
