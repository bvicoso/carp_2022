# Call differential expression between males and females using tmx/DEseq2

## Install tmximport

In: R/4.1.0

```
module load R/4.1.0
```

```
if (!requireNamespace("BiocManager", quietly = TRUE))
install.packages("BiocManager")
BiocManager::install("tximport")
```

## Import data with TMXimport

NB: to get list of directories to run this on:

```
ls ~/FishFun/Carp2022/3b-Sleuth_noBadSamples/*/SleuthSexBias/* | grep 'Fish'
```

Pick directory of the analysis to perform (specifies the reference transcriptome and the tissue in question), and import the data.

```
####PICK KALLISTO RUN TO ANALYZE
dir <- ("/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/3b-Sleuth_noBadSamples/2-Orthogroups/SleuthSexBias/Barb_Brain/")
list.files(dir)

#load necessary packages
library("tximport")
library("rhdf5") #it was already there

#prepare table with sample description
samples <- read.table(file.path(dir, "SleuthConfig.txt"), header = TRUE, sep=" ")
colnames(samples)<-c("sample", "sex")
samples$run<-gsub (" ", "", paste("KalFiles/", samples$sample))

#import kallisto data
files <- file.path(dir, samples$run, "abundance.h5")
#names(files) <- paste0("sample", 1:6)
txi.kallisto <- tximport(files, type = "kallisto", txOut = TRUE)
counttable<-txi.kallisto$counts 
colnames(counttable)<-samples$sample
head(counttable)

#export counts table
write.table(counttable, file.path(dir, "counts.txt"), sep=" ")
```


## Run DEseq2 

Then run the DEseq2 analysis (NB: the resulting log2FoldChange is log2(M/F)):

```
####Format sample info to match DEseq2 run
sampletable<-samples[,1:2]
rownames(sampletable)<-sampletable$sample
colnames(sampletable)<-c("Samplename", "Status")
sampletable


########create the DESeq object
library("DESeq2") # existed
		# countData is the matrix containing the counts
		# sampletable is the sample sheet / metadata we created
		# design is how we wish to model the data: what we want to measure here is the difference between the treatment times
count_matrix<-round(counttable, digits = 0)
se_star_matrix <- DESeqDataSetFromMatrix(countData = count_matrix , colData = sampletable, design = ~ Status)

#########optional: filter out genes with a sum of counts below 10
#se_star_matrix <- se_star_matrix[rowSums(counts(se_star_matrix)) > 10, ]
dds <- DESeq(se_star_matrix)
resultsNames(dds) # lists the coefficients, so you know what to write for "coef=" in the next command

#####Standard differential expression analysis
sexbiased <- results(dds, name="Status_M_vs_F", pAdjustMethod = "BH", alpha=0.05)
```

Let's add TPM from Kallisto so we can also use this for plotting. 

```
tpmname<-gsub("\\/", "", gsub(".*SleuthSexBias\\/", "", dir))
tpm<-read.table(file.path(dir, paste(tpmname, "_SexBiasResults.txt", sep="")), header = TRUE, sep=" ")
tpmdata<-tpm[,c(1,13:ncol(tpm))]

dedata<-sexbiased[,c(1,2,6)]
comb<-merge(as.data.frame(dedata), tpmdata, by.x=0, by.y="target_id")
colnames(comb)<-gsub("Row\\.names", "Gene", colnames(comb))
comb$meanF<-(rowMeans(comb[grep("_F_", colnames(comb))]))
comb$meanM<-(rowMeans(comb[grep("_M_", colnames(comb))]))

write.table(comb, file.path(dir, paste(tpmname, "_DEseq2.txt", sep="")))
```


## Number of differentially expressed genes / tissue

Using Kallisto output with bad samples removed, and only transcripts in orthogroups as the reference, we get:

Tissue | #Degenes
--- | ---
Carp_Spleen | 959
Carp_Liver | 603
Carp_Heart | 268
Carp_Gonad | 23126
Carp_Brain | 665
Barb_Spleen | 512
Barb_Liver | 783
Barb_Heart | 55
Barb_Gonad | 16062
Barb_Brain | 69

