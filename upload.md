# Select files to upload for reproduceable data analysis

The data processing was done in /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022 , but contains many intermediate files. 

Let's keep list of files that get used here and copy them to /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022_upload as we go along:

```
#Sets of orthologs
mkdir Carp2022_upload/1-Trios
cp Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to1* Carp2022_upload/1-Trios/
cp Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_*Chrom.txt Carp2022_upload/1-Trios/

#Expression OrthoTables

#all transcripts as reference
mkdir Carp2022_upload/2-Expression
mkdir Carp2022_upload/2-Expression/1-AllTranscripts
cp Carp2022/3b-Sleuth_noBadSamples/1-AllTranscripts/SleuthSexBias/*/*_DEseq2.txt Carp2022_upload/2-Expression/1-AllTranscripts/

#only orthogroups
mkdir Carp2022_upload/2-Expression/2-Orthogroups
cp Carp2022/3b-Sleuth_noBadSamples/2-Orthogroups/SleuthSexBias/*/*_DEseq2.txt Carp2022_upload/2-Expression/2-Orthogroups/

#only orthogroups
mkdir Carp2022_upload/2-Expression/3-Trios
cp Carp2022/3b-Sleuth_noBadSamples/3-Trios/SleuthSexBias/*/*_DEseq2.txt Carp2022_upload/2-Expression/3-Trios/

#Differential expression between homeologs
mkdir Carp2022_upload/3-HomeoDiff
mkdir Carp2022_upload/3-HomeoDiff/1-AllTranscripts
mkdir Carp2022_upload/3-HomeoDiff/2-Orthogroups
mkdir Carp2022_upload/3-HomeoDiff/3-Trios
#cp Carp2022/3b-Sleuth_noBadSamples/1-AllTranscripts/SleuthSexBias/*/*_AvsBdeseq.txt Carp2022_upload/3-HomeoDiff/1-AllTranscripts
cp Carp2022/3b-Sleuth_noBadSamples/2-Orthogroups/SleuthSexBias/*/*_AvsBdeseq.txt Carp2022_upload/3-HomeoDiff/2-Orthogroups
#cp Carp2022/3b-Sleuth_noBadSamples/3-Trios/SleuthSexBias/*/*_AvsBdeseq.txt Carp2022_upload/3-HomeoDiff/3-Trios
#did not run this for other transcriptome refs, can be done if needed

#number of goldfish orthologs of barb genes
cp Carp2022/1-Trios/3-OrthoTables/Barb_NgoldfishHomologs.joinable Carp2022_upload/1-Trios/
```

And then just upload directory on desktop when files are added:

```
scp -r -o ProxyJump=bvicoso@login.ist.ac.at bvicoso@bea81:/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022_upload /Users/bvicoso/Documents/Will/Carp/Carp2022

#or in office computer
scp -r bvicoso@bea81:/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022_upload /Users/bvicoso/Documents/Will/Carp2022

```


