# Kallisto estimation of gene expression

## 1. Get / make reference transcriptomes

Since the reference transcriptome could influence the overall patterns, let's try three things:

    1. All available transcripts for each species
    2. Only transcripts assigned to an orthogroup (to make the two transcriptomes a bit more comparable)
    3. Only 1:2 trios (exhaustive classification)

1. **All transcripts**

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/1-AllTranscripts

```
ln -s ../../Barb.trinity.fasta.long.500bp_min_unwrapped.fasta .
ln -s ../../Cyprinus_carpio-mRNA.fa .
```

2. **Transcripts in Orthogroups** (21541 in barb, 42016 in carp)

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/2-Orthogroups

Get list of transcripts to extract:

```
cat /nfs/scistore03/vicosgrp/bvicoso//FishFun/Carp2022/1-Trios/2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthogroups/Orthogroups.tsv | perl -pi -e 's/\t/\n/gi' | perl -pi -e 's/, /\n/gi' | grep 'CAFS_CC' | sort > Carp_genes_in_ortho.joinable
cat /nfs/scistore03/vicosgrp/bvicoso//FishFun/Carp2022/1-Trios/2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthogroups/Orthogroups.tsv | perl -pi -e 's/\t/\n/gi' | perl -pi -e 's/, /\n/gi' | grep 'TRINI' | sort > Barb_genes_in_ortho.joinable
```

Extract transcript sequences:

```
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk subseq ~/FishFun/Carp2022/Cyprinus_carpio-mRNA.fa Carp_genes_in_ortho.joinable > Carp_orthoRNA.fa
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk subseq ~/FishFun/Carp2022/Barb.trinity.fasta.long.500bp_min_unwrapped.fasta  Barb_genes_in_ortho.joinable > Barb_orthoRNA.fa
```

3. **1:2 trios (exhaustive classification)**

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/3-Trios

Get list of transcripts to extract:

```
cat ~/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_exhaust_correctChrom.txt | awk '{print $2}' | sort > Barb_genes_in_exTrios.joinable
cat ~/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_exhaust_correctChrom.txt | awk '{print $3, $5}' | perl -pi -e 's/ /\n/gi' | sort > Carp_genes_in_exTrios.joinable
```

Extract transcript sequences:

```
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk subseq ~/FishFun/Carp2022/Cyprinus_carpio-mRNA.fa Carp_genes_in_exTrios.joinable > Carp_TrioRNA.fa
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk subseq ~/FishFun/Carp2022/Barb.trinity.fasta.long.500bp_min_unwrapped.fasta Barb_genes_in_exTrios.joinable > Barb_TrioRNA.fa
```

## 2. Index the transcriptomes

```
module load kallisto/0.46.2 
srun kallisto index -i carp.idx /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/1-AllTranscripts/Cyprinus_carpio-mRNA.fa
srun kallisto index -i barb.idx /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/1-AllTranscripts/Barb.trinity.fasta.long.500bp_min_unwrapped.fasta
```

```
module load kallisto/0.46.2 
srun kallisto index -i carp.idx /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/2-Orthogroups/Carp_orthoRNA.fa
srun kallisto index -i barb.idx /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/2-Orthogroups/Barb_orthoRNA.fa
```

```
module load kallisto/0.46.2 
srun kallisto index -i carp.idx /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/3-Trios/Carp_TrioRNA.fa
srun kallisto index -i barb.idx /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto/3-Trios/Barb_TrioRNA.fa
```

## 3. Run Kallisto

WD: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/2-Kallisto

Get reads from archive (SLURM no longer has access to archive files):

```
cp /archive3/group/vicosgrp/shared/Cyprinus_carpio_RNAseq_reads/*/*/* Reads
cp /archive3/group/vicosgrp/shared/Puntius_titteya_RNAseq_reads/*/*/* Reads
```

First, setup the correct directory structure:

```
mkdir 1-AllTranscripts/KalFiles
mkdir 2-Orthogroups/KalFiles
mkdir 3-Trios/KalFiles
```

Then create a tab-separated file with the following columns: species, sex, tissue, sample number, read file location.

```
ls /archive3/group/vicosgrp/shared/Cyprinus_carpio_RNAseq_reads/*/*/* | perl -pi -e 's/\/archive3.*reads\///gi' | perl -pi -e 's/\// /gi' | perl -pi -e 's/ale_/ale /gi' | perl -pi -e 's/Female/F/gi' | perl -pi -e 's/Male/M/gi' | awk '{print "Carp "$1, $3, $2, $4}' | perl -pi -e 's/ /\t/gi' > ReadsList.txt 
ls /archive3/group/vicosgrp/shared/Puntius_titteya_RNAseq_reads/*/*/* | perl -pi -e 's/\/archive3.*reads\///gi' | perl -pi -e 's/\// /gi' | perl -pi -e 's/ale_/ale /gi' | perl -pi -e 's/Female/F/gi' | perl -pi -e 's/Male/M/gi' | awk '{print "Barb "$1, $3, $2, $4}' | perl -pi -e 's/ /\t/gi' >> ReadsList.txt 
```

**_NB: If we have to rerun, let's replace "Ovary" and "Testis" with "Gonad" in ReadsList.txt, as it makes it easier to automate Sleuth later on._** 

Finally, run the Kallisto wrapper (script [here](https://git.ist.ac.at/bvicoso/carp_2022/-/blob/main/scripts/Kallisto_runner.pl)):
```
#load any module you need here
module load kallisto/0.46.2
module load picard/2.25.6

#run commands on SLURM's srun
perl Kallisto_runner.pl ReadsList.txt
```

To make it easier to run Sleuth/DEseq2 later on, let's replace "Ovary" and "Testis" with "Gonad":

```
rename 's/Testes/Gonad/' */KalFiles/*
rename 's/Ovary/Gonad/' */KalFiles/*
perl -pi -e 's/Ovary/Gonad/gi' ReadsList.txt 
perl -pi -e 's/Testes/Gonad/gi' ReadsList.txt 
```
