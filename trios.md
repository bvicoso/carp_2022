# Get 1:2 and 1:1 sets of homologs between barb and carp

Following Li et al. (2021), we use Orthofinder with:
- the Li et al. (2021) set of carp proteins, and some of their other species
- the translated version of our Barb collapsed transcriptome
- the published zebrafish genome

## Summary

We use a stringent and an exhaustive approach to extract 1:1 and 1:2 orthologs from the orthofinder result, but only the exhaustive was used in further analyses. In the case of 1:2, we keep only trios for which the two carp homeologs are on two homeologous chromosomes (e.g. one on A5 and the other on B5).

**Stringent:** Orthogroups that have 1 gene in barb and 1 (4133) or 2 (6241) in carp were selected. The resulting tables are at:
```
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to1_stringent.txt
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_stringent_correctChrom.txt
```

**Exaustive:** Orthogroups that have 1 gene in barb and 1 (6623) or 2 (7147) in carp were selected. The resulting tables are at:
```
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to1_exhaust.txt
/nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables/Barb_Carp_1to2_exhaust_correctChrom.txt
```

## 1. Map carp transcripts to the two subgenomes

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome

Get files:
```
ln -s ../../GWHACFI00000000.genome.fasta .
ln -s ../../Cyprinus_carpio-mRNA.fa .
```

Set up blat:

```
#load any module you need here
module load blat
#run commands on SLURM's srun
blat GWHACFI00000000.genome.fasta Cyprinus_carpio-mRNA.fa Ccarp_RNA_vs_genome.blat
```

Keep only best hit:

```
sort -k 10 Ccarp_RNA_vs_genome.blat > Ccarp_RNA_vs_genome.blat.sorted
perl ~/2-besthitblat.pl Ccarp_RNA_vs_genome.blat.sorted
rm Ccarp_RNA_vs_genome.blat
rm Ccarp_RNA_vs_genome.blat.sorted
```

Make final location table:

```
grep '>' GWHACFI00000000.genome.fasta | perl -pi -e 's/>//gi' | perl -pi -e 's/OriSeqID=//gi' | awk '{print $1, $2}' | sort > Scaffold_chromosome.joinable
cat Ccarp_RNA_vs_genome.blat.sorted.besthit |grep -v 'match' |  awk '(($2/($1+$2))<0.001)' | awk '{print $14, $10}' | sort | join /dev/stdin Scaffold_chromosome.joinable | awk '{print $2, $1, $3}' | sort > Carp_GeneScafChrom.joinable
```

## 2. Run Orthofinder

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/2b-Orthofinder_ZF

Get files:
```
ln -s ../../Cyprinus_carpio-protein.fa .
ln -s ../../Puntius_tetrazona-protein.fa .
ln -s ../../Paracanthobrama_guichenoti-protein.fa .
ln -s ../../Danio_rerio_Longestprot.fa .
ln ../2-Orthofinder/Barb_prot.fa .
```

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/

```
#run commands on SLURM's srun
/nfs/scistore03/vicosgrp/melkrewi/orthofinder/OrthoFinder/orthofinder -f 2b-Orthofinder_ZF
```

## 3. Make final lists

In: /nfs/scistore03/vicosgrp/bvicoso/FishFun/Carp2022/1-Trios/3-OrthoTables

### 3.a Exhaustive list (set of 1:1 and 1:2 orthologs)

**First 1:1 orthologs:**

```
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep -v ', CAF' |  awk '{print $3, $1, $2}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable |  awk '{print $2, $3, $1, $5, $5}' | awk -F' ' -vOFS=' ' '{ gsub("[0-9]", "", $5) ; print }' > Barb_Carp_1to1_exhaust.txt
```

The file looks like (orthogroup, barb, carp, carp_chrom, carp_subgenome):

```
OG0001995 TRINITY_DN14237_c0_g1_i1 CAFS_CC_T_00000006 A19 A
OG0014048 TRINITY_DN3899_c0_g2_i2 CAFS_CC_T_00000036 A19 A
OG0014151 TRINITY_DN103823_c0_g1_i1 CAFS_CC_T_00000061 A19 A
OG0018466 TRINITY_DN6686_c0_g1_i1 CAFS_CC_T_00000099 A19 A
```

**Now 1:2 orthologs:**

To simply see all 1:2 orthologs:

```
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep ',*,' | awk '($5 !~ /CAFS/)' | perl -pi -e 's/, /\t/gi' | head
```

Now with gene locations, so we can pick only those that have one subA and one subB duplicate:

```
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep ',*,' | awk '($5 !~ /CAFS/)' | perl -pi -e 's/, /\t/gi' | awk '{print $3, $1, $2, $4}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $4, $2, $3, $1, $6}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $2, $3, $4, $5, $1, $7}' | head
```

Make final list:

```
#first print those that have subA ortholog first
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep ',*,' | awk '($5 !~ /CAFS/)' | perl -pi -e 's/, /\t/gi' | awk '{print $3, $1, $2, $4}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $4, $2, $3, $1, $6}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $2, $3, $4, $5, $1, $7}' | awk '(($4 ~/A/) && ($6 ~/B/))' > Barb_Carp_1to2_exhaust.txt

#then those that are the other way around
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthologues/Orthologues_Barb_prot/Barb_prot__v__Cyprinus_carpio-protein.tsv | grep -v ', TRINI' | grep ',*,' | awk '($5 !~ /CAFS/)' | perl -pi -e 's/, /\t/gi' | awk '{print $3, $1, $2, $4}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $4, $2, $3, $1, $6}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $2, $3, $4, $5, $1, $7}' | awk '(($4 ~/B/) && ($6 ~/A/))' | awk '{print $1, $2, $5, $6, $3, $4}' >> Barb_Carp_1to2_exhaust.txt
```

The file looks like (orthogroup, barb, carp_A, chrom_carpA, carp_B, chrom_carpB):

```
OG0006008 TRINITY_DN29185_c0_g1_i7 CAFS_CC_T_00043494 A13 CAFS_CC_T_00003855 B13
OG0006072 TRINITY_DN306_c3_g1_i1 CAFS_CC_T_00043498 A13 CAFS_CC_T_00003882 B13
OG0005139 TRINITY_DN103491_c0_g1_i1 CAFS_CC_T_00214005 A15 CAFS_CC_T_00004494 B15
OG0007325 TRINITY_DN25_c2_g1_i5 CAFS_CC_T_00118891 A8 CAFS_CC_T_00007999 B8
OG0004752 TRINITY_DN3509_c0_g1_i1 CAFS_CC_T_00118878 A8 CAFS_CC_T_00008028 B8
```

7147 out of 7241 are in pairs of A and B with the same number, let's select them:

```
cat Barb_Carp_1to2_exhaust.txt | perl -pi -e 's/[AB]//gi' | awk '($4==$6)' | awk '{print $2}' | grep -f /dev/stdin Barb_Carp_1to2_exhaust.txt > Barb_Carp_1to2_exhaust_correctChrom.txt
```


### 3.b Stringent list (set of 1:1 and 1:2 orthogroups)

Select correct Orthogroups:

```
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthogroups/Orthogroups.GeneCount.tsv | awk '($2=="1" && $3=="2")' | cut -f 1 | sort > Orthogroups_1to2.joinable
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthogroups/Orthogroups.GeneCount.tsv | awk '($2=="1" && $3=="1")' | cut -f 1 | sort > Orthogroups_1to1.joinable
```

And fix the format of Orthogroups.txt:
```
~/scripts/flip.linux-static -u ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthogroups/Orthogroups.txt
```

**1:1 table:**

```
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthogroups/Orthogroups.txt | perl -pi -e 's/ /\n/gi' | awk '($1 ~ /OG/ || $1 ~ /CAFS_CC/ || $1 ~/TRINITY/)' | perl -pi -e 's/\n/\t/gi' | perl -pi -e 's/OG/\nOG/gi' | perl -pi -e 's/://gi' | sort | join /dev/stdin Orthogroups_1to1.joinable | awk '{print $2, $1, $3}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $2, $3, $1, $5, $5}' | awk -F' ' -vOFS=' ' '{ gsub("[0-9]", "", $5) ; print }' > Barb_Carp_1to1_stringent.txt
```

The table looks like (orthogroup, barb, carp, carp_chrom, carp_subgenome):

```
OG0014048 TRINITY_DN3899_c0_g2_i2 CAFS_CC_T_00000036 A19 A
OG0014151 TRINITY_DN103823_c0_g1_i1 CAFS_CC_T_00000061 A19 A
OG0018466 TRINITY_DN6686_c0_g1_i1 CAFS_CC_T_00000099 A19 A
OG0012591 TRINITY_DN281_c2_g1_i1 CAFS_CC_T_00000114 A19 A
```

**1:2 table:**

Check that the carp orthologs appear before the barb ones in the orthogroups.txt file (it was the case for this run).

```
cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthogroups/Orthogroups.txt | perl -pi -e 's/ /\n/gi' | awk '($1 ~ /OG/ || $1 ~ /CAFS_CC/ || $1 ~/TRINITY/)' | perl -pi -e 's/\n/\t/gi' | perl -pi -e 's/OG/\nOG/gi' | perl -pi -e 's/://gi' | sort | join /dev/stdin Orthogroups_1to2.joinable | awk '{print $2, $1, $4, $3}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $4, $2, $3, $1, $6}'  | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $2, $3, $4, $5, $1, $7}' | awk '(($4 ~/A/) && ($6 ~/B/))' > Barb_Carp_1to2_stringent.txt

cat ../2b-Orthofinder_ZF/OrthoFinder/Results_Oct31/Orthogroups/Orthogroups.txt | perl -pi -e 's/ /\n/gi' | awk '($1 ~ /OG/ || $1 ~ /CAFS_CC/ || $1 ~/TRINITY/)' | perl -pi -e 's/\n/\t/gi' | perl -pi -e 's/OG/\nOG/gi' | perl -pi -e 's/://gi' | sort | join /dev/stdin Orthogroups_1to2.joinable | awk '{print $2, $1, $4, $3}' | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $4, $2, $3, $1, $6}'  | sort | join /dev/stdin ~/FishFun/Carp2022/1-Trios/1-Carp_GeneSubgenome/Carp_GeneScafChrom.joinable | awk '{print $2, $3, $4, $5, $1, $7}' | awk '(($4 ~/B/) && ($6 ~/A/))'  | awk '{print $1, $2, $5, $6, $3, $4}' >> Barb_Carp_1to2_stringent.txt
```

The table looks like (orthogroup, barb, carp_A, chrom_carpA, carp_B, chrom_carpB):

```
OG0008364 TRINITY_DN13857_c0_g1_i1 CAFS_CC_T_00038930 A17 CAFS_CC_T_00039197 B17
OG0008729 TRINITY_DN538_c0_g4_i4 CAFS_CC_T_00038933 A17 CAFS_CC_T_00039201 B17
OG0005122 TRINITY_DN126888_c0_g1_i1 CAFS_CC_T_00038939 A17 CAFS_CC_T_00039206 B17
OG0006103 TRINITY_DN5929_c0_g1_i3 CAFS_CC_T_00038949 A17 CAFS_CC_T_00039223 B17
```

6241/6315 are in A and B chromosomes with the same number, let's select them:

```
cat Barb_Carp_1to2_stringent.txt | perl -pi -e 's/[AB]//gi' | awk '($4==$6)' | awk '{print $1}' | grep -f /dev/stdin Barb_Carp_1to2_stringent.txt > Barb_Carp_1to2_stringent_correctChrom.txt
```
