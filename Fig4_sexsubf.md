# R code for Figure 4

## Load data

We need:
- table with all tissue expression
- p-values between male and female

<details><summary>R code for loading data</summary>

```
#set working directory to directory where "carp2022_upload" is
setwd("~/Documents/Will/Carp/Carp2022/")
library("dplyr")                          # Load dplyr package

 #get set of 1:1 orthologs
orth1<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to1_exhaust.txt")
#get set of 1:2 orthologs
orth2<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to2_exhaust_correctChrom.txt", head=F, sep=" ")
colnames(orth1)<-c("orthogroup", "barbgene", "carpgene", "chr", "subgenome")
colnames(orth2)<-c("orthogroup", "barbgene", "carpA", "chrA", "carpB", "chrB")

#Get expression
bbrain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Brain_DEseq2.txt", head=T, sep=" ")
bgonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Gonad_DEseq2.txt", head=T, sep=" ")
bheart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Heart_DEseq2.txt", head=T, sep=" ")
bliver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Liver_DEseq2.txt", head=T, sep=" ")
bspleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Spleen_DEseq2.txt", head=T, sep=" ")
brain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Brain_DEseq2.txt", head=T, sep=" ")
gonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Gonad_DEseq2.txt", head=T, sep=" ")
heart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Heart_DEseq2.txt", head=T, sep=" ")
liver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Liver_DEseq2.txt", head=T, sep=" ")
spleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Spleen_DEseq2.txt", head=T, sep=" ")

#make brain tables

brain1 <- inner_join(orth1[,1:2], bbrain, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]     
brain1a <- inner_join(orth1[,c(1,3)], brain, by = c("carpgene" = "Gene"))[,c(2,4,5,12,13)]

brain2 <- inner_join(orth2[,1:2], bbrain, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
brain2a <- inner_join(orth2[,c(1,3)], brain, by = c("carpA" = "Gene"))[,c(2,4,5,12,13)]
brain2b <- inner_join(orth2[,c(1,5)], brain, by = c("carpB" = "Gene"))[,c(2,4,5,12,13)]

#make gonad tables

gonad1 <- inner_join(orth1[,1:2], bgonad, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
gonad1a <- inner_join(orth1[,c(1,3)], gonad, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

gonad2 <- inner_join(orth2[,1:2], bgonad, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
gonad2a <- inner_join(orth2[,c(1,3)], gonad, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
gonad2b <- inner_join(orth2[,c(1,5)], gonad, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]

#make heart tables

heart1<-inner_join(orth1[,1:2], bheart, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
heart1a<-inner_join(orth1[,c(1,3)], heart, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

heart2<-inner_join(orth2[,1:2], bheart, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
heart2a<-inner_join(orth2[,c(1,3)], heart, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
heart2b<-inner_join(orth2[,c(1,5)], heart, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]
#make liver tables
liver1<-inner_join(orth1[,1:2], bliver, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
liver1a<-inner_join(orth1[,c(1,3)], liver, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

liver2<-inner_join(orth2[,1:2], bliver, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
liver2a<-inner_join(orth2[,c(1,3)], liver, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
liver2b<-inner_join(orth2[,c(1,5)], liver, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]
#make spleen tables
spleen1<-inner_join(orth1[,1:2], bspleen, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
spleen1a<-inner_join(orth1[,c(1,3)], spleen, by = c("carpgene" = "Gene"))[,c(2,4,5,12,13)]

spleen2<-inner_join(orth2[,1:2], bspleen, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
spleen2a<-inner_join(orth2[,c(1,3)], spleen, by = c("carpA" = "Gene"))[,c(2,4,5,12,13)]
spleen2b<-inner_join(orth2[,c(1,5)], spleen, by = c("carpB" = "Gene"))[,c(2,4,5,12,13)]

```

</details>


<details><summary>R code for making normalized expression table with all tissues/species</summary>

```
all<-(cbind(brain2$meanF, brain2a$meanF, brain2b$meanF, gonad2$meanF, gonad2a$meanF, gonad2b$meanF, heart2$meanF, heart2a$meanF, heart2b$meanF,liver2$meanF, liver2a$meanF, liver2b$meanF, spleen2$meanF, spleen2a$meanF, spleen2b$meanF, brain2$meanM, brain2a$meanM, brain2b$meanM, gonad2$meanM, gonad2a$meanM, gonad2b$meanM, heart2$meanM, heart2a$meanM, heart2b$meanM,liver2$meanM, liver2a$meanM, liver2b$meanM, spleen2$meanM, spleen2a$meanM, spleen2b$meanM))
colnames(all)<-c("barb_brainF", "subA_brainF", "subB_brainF", "barb_gonadF", "subA_gonadF", "subB_gonadF", "barb_heartF", "subA_heartF", "subB_heartF",  "barb_liverF", "subA_liverF", "subB_liverF",  "barb_spleenF", "subA_spleenF", "subB_spleenF", "barb_brainM", "subA_brainM", "subB_brainM", "barb_gonadM", "subA_gonadM", "subB_gonadM", "barb_heartM", "subA_heartM", "subB_heartM",  "barb_liverM", "subA_liverM",  "subB_liverM",  "barb_spleenM", "subA_spleenM", "subB_spleenM")

###Normalize
bolFMat<-as.matrix(all, nrow = nrow(all), ncol = ncol(all))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(all)
colnames(temp2)<-colnames(all)
all <-as.data.frame(temp2)
rownames(all)<-brain2$barbgene

all1<-(cbind(brain1$meanF, brain1a$meanF, gonad1$meanF, gonad1a$meanF, heart1$meanF, heart1a$meanF, liver1$meanF, liver1a$meanF, spleen1$meanF, spleen1a$meanF, brain1$meanM, brain1a$meanM, gonad1$meanM, gonad1a$meanM, heart1$meanM, heart1a$meanM, liver1$meanM, liver1a$meanM, spleen1$meanM, spleen1a$meanM))
colnames(all1)<-c("barb_brainF", "subA_brainF",  "barb_gonadF", "subA_gonadF", "barb_heartF", "subA_heartF",  "barb_liverF", "subA_liverF", "barb_spleenF", "subA_spleenF", "barb_brainM", "subA_brainM",  "barb_gonadM", "subA_gonadM", "barb_heartM", "subA_heartM", "barb_liverM", "subA_liverM",  "barb_spleenM", "subA_spleenM")


###Normalize
bolFMat<-as.matrix(all1, nrow = nrow(all1), ncol = ncol(all1))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(all1)
colnames(temp2)<-colnames(all1)
all1 <-as.data.frame(temp2)
rownames(all1)<-brain1$barbgene

```

</details>

## Plot for each tissue

<details><summary>R code for making plot with all tissues</summary>

```
dev.new(width=6, height=2.3)
par(mfrow=c(1,3))
par(mar=c(3,3,2,0.2))
par(mgp=c(2,1,0))
nscol<-"#A6CEE3"
sigcol<-"#E31A1C"

###brain
plot(log2(all$subA_brainF/all$subA_brainM), log2(all$subB_brainF/all$subB_brainM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=nscol, main="A. Brain")
abline(h=0, lty=2)
abline(v=0, lty=2)

deseq<-as.data.frame(cbind(brain2a$log2FoldChange, brain2a$padj, brain2b$log2FoldChange, brain2b$padj))
colnames(deseq)<-c("logA", "pA", "logB", "pB")
rownames(deseq)<-brain2$barbgene
head(deseq)
sexantag<-merge(subset(deseq, pA<0.05 & pB<0.05 & (logA*logB)<0), all, by=0)
dim(sexantag)
points(log2(sexantag$subA_brainF/sexantag$subA_brainM), log2(sexantag$subB_brainF/sexantag$subB_brainM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=sigcol)

###gonad
plot(log2(all$subA_gonadF/all$subA_gonadM), log2(all$subB_gonadF/all$subB_gonadM), xlab="Log2(subA_F/subA_M)", ylab="Log2(subB_F/subB_M)", pch=20, col=nscol, main="B. Gonad")
abline(h=0, lty=2)
abline(v=0, lty=2)

deseq<-as.data.frame(cbind(gonad2a$log2FoldChange, gonad2a$padj, gonad2b$log2FoldChange, gonad2b$padj))
colnames(deseq)<-c("logA", "pA", "logB", "pB")
rownames(deseq)<-gonad2$barbgene
head(deseq)
sexantag<-merge(subset(deseq, pA<0.05 & pB<0.05 & (logA*logB)<0), all, by=0)
dim(sexantag)
points(log2(sexantag$subA_gonadF/sexantag$subA_gonadM), log2(sexantag$subB_gonadF/sexantag$subB_gonadM), pch=20, col=sigcol)

```

</details>


<details><summary>Plot # subfunctionalized vs # differentially expressed</summary>

```
subf<-read.table("~/Documents/Will/Carp/Carp2022/SunfunctTables/Table_Dec7_2021.txt", sep="\t", head=T)
subf
#set up linear model
lm_subf <- lm(Ratio ~ All, data = subf)
summary(lm_subf)

#Plot data and model
#empty plot with regression line
plot(subf$All, subf$Ratio, col="white", xlab="Number of DE genes", ylab="Number of subfunctionalized pairs", main="C. Subfunct. vs D.E.")
abline(lm_subf, col="lightblue")

#set range of x value for which to plot confidence interval
newx <- seq(min(subf$All), max(subf$All), by=100)

conf_interval <- predict(lm_subf, newdata=data.frame(All=newx), interval="prediction",
                         level = 0.95)
lines(newx, conf_interval[,2], col="lightblue", lty=2)
lines(newx, conf_interval[,3], col="lightblue", lty=2)

#replot points
gonad_f<-subset(subf, comparison=="F" & gonad=="yes")
gonad_m<-subset(subf, comparison=="M" & gonad=="yes")
gonad_fm<-subset(subf, comparison=="FM" & gonad=="yes")
soma_f<-subset(subf, comparison=="F" & gonad=="no")
soma_m<-subset(subf, comparison=="M" & gonad=="no")
soma_fm<-subset(subf, comparison=="FM" & gonad=="no")
fcol<-"#FB9A99"
mcol<-"#A6CEE3"
fmcol<-"#B2DF8A"
pchsoma<-21
pchgonad<-23

points(gonad_f$All, gonad_f$Ratio, col="black", bg=fcol, pch=pchgonad)
points(gonad_m$All, gonad_m$Ratio, col="black", bg=mcol, pch=pchgonad)
points(gonad_fm$All, gonad_fm$Ratio, col="black", bg=fmcol, pch=pchgonad)

points(soma_f$All, soma_f$Ratio, col="black", bg=fcol, pch=pchsoma)
points(soma_m$All, soma_m$Ratio, col="black", bg=mcol, pch=pchsoma)
points(soma_fm$All, soma_fm$Ratio, col="black", bg=fmcol, pch=pchsoma)

#calculate cooks distance and (optional) plot it to check that gonad_fm is an outlier
cooksd <- cooks.distance(lm_subf)
 #plot cook's distance to get a sense of how many outliers
# plot(cooksd, pch="*", cex=2, main="Influential Obs by Cooks distance")  # plot cook's distance
# abline(h = 4*mean(cooksd, na.rm=T), col="red")  # add cutoff line
#  text(x=1:length(cooksd)+1, y=cooksd, labels=ifelse(cooksd>4*mean(cooksd, na.rm=T),names(cooksd),""), col="red")  # add labels
 #Bonferroni Outlier Test 
 car::outlierTest(lm_subf)
#text(subf[5,3], subf[5,2]+90, "p = 0.03")

 # Add a legend
legend("topleft", 
  legend = c("Male tissues", "Female tissues", "Male vs. female"), 
  col = c(fcol, mcol, fmcol), 
  pch = c(15, 15, 15), 
  bty = "n", 
  pt.cex = 1, 
  cex = 1, 
  text.col = "black", 
  horiz = F , 
  inset = c(0, 0))

 # Add a legend
legend("bottomright", 
  legend = c("Gonad included", "Somatic only"), 
  col = c("black", "black"), 
  pch = c(pchgonad, pchsoma), 
  bty = "n", 
  pt.cex = 1, 
  cex = 1, 
  text.col = "black", 
  horiz = F , 
  inset = c(0, 0))

```

</details>




