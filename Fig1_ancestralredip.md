# R code for making the final figure 1

## Load data

Let's merge:
- the list of barb genes that have either 1 or 2 orthologs in carp
- patterns of barb expression in different tissues

<details><summary>R code</summary>

```
#set working directory to directory where "carp2022_upload" is
setwd("~/Documents/Will/Carp/Carp2022/")

 #get set of 1:1 orthologs
orth1<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to1_exhaust.txt")
#get set of 1:2 orthologs
orth2<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to2_exhaust_correctChrom.txt", head=F, sep=" ")
colnames(orth1)<-c("orthogroup", "barbgene", "carpgene", "chr", "subgenome")
colnames(orth2)<-c("orthogroup", "barbgene", "carpA", "chrA", "carpB", "chrB")

#Get expression
brain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Brain_DEseq2.txt", head=T, sep=" ")
gonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Gonad_DEseq2.txt", head=T, sep=" ")
heart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Heart_DEseq2.txt", head=T, sep=" ")
liver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Liver_DEseq2.txt", head=T, sep=" ")
spleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Spleen_DEseq2.txt", head=T, sep=" ")

#make brain tables
brain1<-merge(orth1[,1:2], brain, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
brain2<-merge(orth2[,1:2], brain, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
#make gonad tables
gonad1<-merge(orth1[,1:2], gonad, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
gonad2<-merge(orth2[,1:2], gonad, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
#make heart tables
heart1<-merge(orth1[,1:2], heart, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
heart2<-merge(orth2[,1:2], heart, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
#make liver tables
liver1<-merge(orth1[,1:2], liver, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
liver2<-merge(orth2[,1:2], liver, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
#make spleen tables
spleen1<-merge(orth1[,1:2], spleen, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
spleen2<-merge(orth2[,1:2], spleen, by.x="barbgene", by.y="Gene")[,c(1,4,5,12,13)]
```

</details>

## Make big table with all expression 

<details><summary>R code for big table</summary>

```
#simplify expression tables and use log2(tpm)

sbrain<-brain[,c(1,3,11,12)]
sbrain$meanF<-(sbrain$meanF)
sbrain$meanM<-(sbrain$meanM)
colnames(sbrain)<-c("Gene", "log2brain", "Fbrain", "Mbrain")

sheart<-heart[,c(1,3,11,12)]
sheart$meanF<-(sheart$meanF)
sheart$meanM<-(sheart$meanM)
colnames(sheart)<-c("Gene", "log2heart", "Fheart", "Mheart")

sspleen<-spleen[,c(1,3,11,12)]
sspleen$meanF<-(sspleen$meanF)
sspleen$meanM<-(sspleen$meanM)
colnames(sspleen)<-c("Gene", "log2spleen", "Fspleen", "Mspleen")

sliver<-liver[,c(1,3,11,12)]
sliver$meanF<-(sliver$meanF)
sliver$meanM<-(sliver$meanM)
colnames(sliver)<-c("Gene", "log2liver", "Fliver", "Mliver")

sgonad<-gonad[,c(1,3,11,12)]
sgonad$meanF<-(sgonad$meanF)
sgonad$meanM<-(sgonad$meanM)
colnames(sgonad)<-c("Gene", "log2gonad", "Fgonad", "Mgonad")

#Merge tables
big1<-merge(sbrain, sheart, by.x="Gene", by.y="Gene")
big2<-merge(big1, sspleen, by.x="Gene", by.y="Gene")
big3<-merge(big2, sliver, by.x="Gene", by.y="Gene")
big4<-merge(big3, sgonad, by.x="Gene", by.y="Gene")
```

</details>


## Calculate Mean expression, mean sex bias and Tau

<details><summary>R code for calculating mean expression and mean sex-bias</summary>

```
big5<-big4[,grep("F|M", colnames(big4))]

###Normalize
bolFMat<-as.matrix(big5, nrow = nrow(big5), ncol = ncol(big5))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(big5)
colnames(temp2)<-colnames(big5)
big5 <-as.data.frame(temp2)

big5$meanF<-rowMeans(big5[,grep("F", colnames(big5))])
big5$meanM<-rowMeans(big5[,grep("M", colnames(big5))])

big5$meanLog<-abs(log2(big5$meanF/big5$meanM))
rownames(big5)<-big4$Gene
```

</details>

<details><summary>R code for calculating Tau</summary>

```
#simplify expression tables and use log2(tpm)

sbrain<-brain[,c(1,3,11,12)]
sbrain$meanF<-log2(sbrain$meanF)
sbrain$meanM<-log2(sbrain$meanM)
colnames(sbrain)<-c("Gene", "log2brain", "Fbrain", "Mbrain")

sheart<-heart[,c(1,3,11,12)]
sheart$meanF<-log2(sheart$meanF)
sheart$meanM<-log2(sheart$meanM)
colnames(sheart)<-c("Gene", "log2heart", "Fheart", "Mheart")

sspleen<-spleen[,c(1,3,11,12)]
sspleen$meanF<-log2(sspleen$meanF)
sspleen$meanM<-log2(sspleen$meanM)
colnames(sspleen)<-c("Gene", "log2spleen", "Fspleen", "Mspleen")

sliver<-liver[,c(1,3,11,12)]
sliver$meanF<-log2(sliver$meanF)
sliver$meanM<-log2(sliver$meanM)
colnames(sliver)<-c("Gene", "log2liver", "Fliver", "Mliver")

sgonad<-gonad[,c(1,3,11,12)]
sgonad$meanF<-log2(sgonad$meanF)
sgonad$meanM<-log2(sgonad$meanM)
colnames(sgonad)<-c("Gene", "log2gonad", "Fgonad", "Mgonad")

#Merge tables
big1<-merge(sbrain, sheart, by.x="Gene", by.y="Gene")
big2<-merge(big1, sspleen, by.x="Gene", by.y="Gene")
big3<-merge(big2, sliver, by.x="Gene", by.y="Gene")
big4<-merge(big3, sgonad, by.x="Gene", by.y="Gene")

#####select F tissues and calculate tau 
#select female tissues
fem<-big4[,grep("F", colnames(big4))]
rownames(fem)<-big4$Gene

###Normalize
bolFMat<-as.matrix(fem, nrow = nrow(fem), ncol = ncol(fem))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(fem)
colnames(temp2)<-colnames(fem)
fem<-temp2

#replace negative numbers with 0 (see tau paper)
fem[fem < 0] <- 0
fem<-as.data.frame(fem)
#find maximum expression for each gene
fem$max<-pmax(fem$Fbrain, fem$Fheart, fem$Fspleen, fem$Fliver, fem$Fgonad)
#normalize the expression by the maximum expression of each gene
fem2<-fem/fem$max 
#Get rid of last column
fem2<-fem2[,1:(ncol(fem2)-1)]
#Calculate tau 
fem2<-(1-fem2)
fem2$tau<-rowSums(fem2)/(5-1)
fem$tau<-fem2$tau


#####select M tissues and calculate tau 
#select male tissues
male<-big4[,grep("M", colnames(big4))]
rownames(male)<-big4$Gene

###Normalize
bolFMat<-as.matrix(male, nrow = nrow(male), ncol = ncol(male))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(male)
colnames(temp2)<-colnames(male)
male<-temp2

#replace negative numbers with 0 (see tau paper)
male[male < 0] <- 0
male<-as.data.frame(male)
#find maximum expression for each gene
male$max<-pmax(male$Mbrain, male$Mheart, male$Mspleen, male$Mliver, male$Mgonad)
#normalize the expression by the maximum expression of each gene
male2<-male/male$max 
#Get rid of last column
male2<-male2[,1:(ncol(male2)-1)]
#Calculate tau 
male2<-(1-male2)
male2$tau<-rowSums(male2)/(5-1)
male$tau<-male2$tau

```

</details>


## Plotting

Let's have the figure be:
* Mean expression in male and female tissues
* Mean sex-bias 
* Tau in male and female tissues
* tissue-specificity and tau 

<details><summary>Code for plotting panels A, B, C</summary>

```
dev.new(width=6, height=5)
 
layout( matrix(c(1,4,1,4,1,4,1,4,2,4,2,5,3,5,3,5,3,5,3,5), ncol=10) )
par(mar=c(3,3,2,0.2))
par(mgp=c(2,1,0))

#make expression tables
TPM1<-merge(orth1[,1:2], big5, by.x="barbgene", by.y=0)
TPM2<-merge(orth2[,1:2], big5, by.x="barbgene", by.y=0)

#boxplot ancestral mean expression
boxnames=c("1:1", "1:2", "1:1", "1:2")
#female
boxcols=c("#FB9A99", "#E31A1C", "#A6CEE3", "#1F78B4")
boxplot(log2(TPM1$meanF), log2(TPM2$meanF), log2(TPM1$meanM), log2(TPM2$meanM), outline=F, names=boxnames, notch=T, col=boxcols, main="A. Expression", at = c(0, 1, 2.5, 3.5), ylab = "Log2(TPM)")

##Add significance
loc<-(7)
pval<-wilcox.test(log2(TPM1$meanF), log2(TPM2$meanF))$p.value
if (pval<0.001) {
text(0.5, loc, "***", cex=2)
} else if (pval<0.01) {
text(0.5, loc, "**", cex=2)
} else if ( pval<0.05) {
text(0.5, loc, "*", cex=2)
} else {
###
}
pval<-wilcox.test(log2(TPM1$meanM), log2(TPM2$meanM))$p.value
if (pval<0.001) {
text(3, loc, "***", cex=2)
} else if (pval<0.01) {
text(3, loc, "**", cex=2)
} else if ( pval<0.05) {
text(3, loc, "*", cex=2)
} else {
###
}
#boxplot sex bias
boxnames=c("1:1", "1:2")
#female
boxcols=c("#B2DF8A", "#33A02C")
boxplot((TPM1$meanLog), (TPM2$meanLog), outline=F, names=boxnames, notch=T, col=boxcols, main="B. Sex-bias", ylab="Abs[Log2(F/M)]")

##Add significance
loc<-(1)
pval<-wilcox.test((TPM1$meanLog), (TPM2$meanLog))$p.value
if (pval<0.001) {
text(1.5, loc, "***", cex=2)
} else if (pval<0.01) {
text(1.5, loc, "**", cex=2)
} else if ( pval<0.05) {
text(1.5, loc, "*", cex=2)
} else {
###
}
####boxplot tau

#make tau tables
ftau1<-merge(orth1[,1:2], fem, by.x="barbgene", by.y=0)
ftau2<-merge(orth2[,1:2], fem, by.x="barbgene", by.y=0)
#make tau tables
mtau1<-merge(orth1[,1:2], male, by.x="barbgene", by.y=0)
mtau2<-merge(orth2[,1:2], male, by.x="barbgene", by.y=0)

#boxplot ancestral tau
boxnames=c("1:1", "1:2", "1:1", "1:2")

#female
boxcols=c("#FB9A99", "#E31A1C", "#A6CEE3", "#1F78B4")
boxplot( (ftau1$tau),  (ftau2$tau),  (mtau1$tau),  (mtau2$tau), outline=F, names=boxnames, notch=T, col=boxcols, main="C. Tau", at = c(0, 1, 2.5, 3.5), ylab = " Tau")

##Add significance
loc<-(0.9)
pval<-wilcox.test(abs(ftau1$tau), abs(ftau2$tau))$p.value
if (pval<0.001) {
text(0.5, loc, "***", cex=2)
} else if (pval<0.01) {
text(0.5, loc, "**", cex=2)
} else if ( pval<0.05) {
text(0.5, loc, "*", cex=2)
} else {
###
}
pval<-wilcox.test(abs(mtau1$tau), abs(mtau2$tau))$p.value
if (pval<0.001) {
text(3, loc, "***", cex=2)
} else if (pval<0.01) {
text(3, loc, "**", cex=2)
} else if ( pval<0.05) {
text(3, loc, "*", cex=2)
} else {
###
}
###Plot tissue-specific

```
</details>

## Add last panel (tissue specificity and tau)

<details><summary>Classifying tissues by which tissue they are primarily expressed in</summary>

```
 #male
 male2<-male/(male$max)
 male2$maxtissue<-"NA"
male2$tau<-male$tau


 maleb<-subset(male2, Mbrain==1)
 maleb$maxtissue<-"brain"
 maleh<-subset(male2, Mheart==1)
 maleh$maxtissue<-"heart"
 males<-subset(male2, Mspleen==1)
 males$maxtissue<-"spleen"
 malel<-subset(male2, Mliver==1)
 malel$maxtissue<-"liver"
 maleg<-subset(male2, Mgonad==1)
 maleg$maxtissue<-"gonad"

male3<-rbind(maleb, maleh, males, malel, maleg)


#female
fem2<-fem/(fem$max)
 fem2$maxtissue<-"NA"
fem2$tau<-fem$tau

 femb<-subset(fem2, Fbrain==1)
 femb$maxtissue<-"brain"
 femh<-subset(fem2, Fheart==1)
 femh$maxtissue<-"heart"
 fems<-subset(fem2, Fspleen==1)
 fems$maxtissue<-"spleen"
 feml<-subset(fem2, Fliver==1)
 feml$maxtissue<-"liver"
 femg<-subset(fem2, Fgonad==1)
 femg$maxtissue<-"gonad"

fem3<-rbind(femb, femh, fems, feml, femg)
```

</details>

<details><summary>Add ploidy information and break up by tau</summary>

```
#make tau tables
ftau1<-merge(orth1[,1:2], fem3, by.x="barbgene", by.y=0)
ftau2<-merge(orth2[,1:2], fem3, by.x="barbgene", by.y=0)
#make tau tables
mtau1<-merge(orth1[,1:2], male3, by.x="barbgene", by.y=0)
mtau2<-merge(orth2[,1:2], male3, by.x="barbgene", by.y=0)

#for each tau check number of genes primarily expressed in each tissue
#male
mtau1_1<-subset(mtau1, tau<0.2)
 mtau1_2<-subset(mtau1, tau>0.2 & tau<0.4)
 mtau1_3<-subset(mtau1, tau>0.4 & tau<0.6)
 mtau1_4<-subset(mtau1, tau>0.6 & tau<0.8)
 mtau1_5<-subset(mtau1, tau>0.8)

mtau2_1<-subset(mtau2, tau<0.2)
 mtau2_2<-subset(mtau2, tau>0.2 & tau<0.4)
 mtau2_3<-subset(mtau2, tau>0.4 & tau<0.6)
 mtau2_4<-subset(mtau2, tau>0.6 & tau<0.8)
 mtau2_5<-subset(mtau2, tau>0.8)

maletable1<-rbind(table(mtau1_1$maxtissue),
table(mtau1_2$maxtissue),
table(mtau1_3$maxtissue),
table(mtau1_4$maxtissue),
table(mtau1_5$maxtissue))

maletable2<-rbind(table(mtau2_1$maxtissue),
table(mtau2_2$maxtissue),
table(mtau2_3$maxtissue),
table(mtau2_4$maxtissue),
table(mtau2_5$maxtissue))

#female
ftau1_1<-subset(ftau1, tau<0.2)
 ftau1_2<-subset(ftau1, tau>0.2 & tau<0.4)
 ftau1_3<-subset(ftau1, tau>0.4 & tau<0.6)
 ftau1_4<-subset(ftau1, tau>0.6 & tau<0.8)
 ftau1_5<-subset(ftau1, tau>0.8)

ftau2_1<-subset(ftau2, tau<0.2)
 ftau2_2<-subset(ftau2, tau>0.2 & tau<0.4)
 ftau2_3<-subset(ftau2, tau>0.4 & tau<0.6)
 ftau2_4<-subset(ftau2, tau>0.6 & tau<0.8)
 ftau2_5<-subset(ftau2, tau>0.8)

femtable1<-rbind(table(ftau1_1$maxtissue),
table(ftau1_2$maxtissue),
table(ftau1_3$maxtissue),
table(ftau1_4$maxtissue),
table(ftau1_5$maxtissue))

femtable2<-rbind(table(ftau2_1$maxtissue),
table(ftau2_2$maxtissue),
table(ftau2_3$maxtissue),
table(ftau2_4$maxtissue),
table(ftau2_5$maxtissue))

#% rediploidized as a function of Tau
femlost<-t(femtable1/(femtable1+femtable2))
colnames(femlost)<-c("<0.2", "0.2-0.4", "0.4-0.6", "0.6-0.8", ">0.8")
malelost<-t(maletable1/(maletable1+maletable2))
colnames(malelost)<-c("<0.2", "0.2-0.4", "0.4-0.6", "0.6-0.8", ">0.8")
```
</details>

<details><summary>Plot % rediploidized as a function of tau</summary>

```
plot(100*malelost[1,], ylim=c(30,max(rbind(100*malelost, 100*femlost))+0.1), ylab="% rediploidized", col="white", xlab="tau category", main="D. Male tissues", cex.axis=1, cex.lab=1.2, col.axis="white", xlim=c(1,5.2))
 lines(100*malelost[1,], col="#1F78B4", lwd=3)
 lines(100*malelost[2,], col="#E31A1C", lwd=3)
 lines(100*malelost[3,], col="#B2DF8A", lwd=3)
 lines(100*malelost[4,], col="#FDBF6F", lwd=3)
 lines(100*malelost[5,], col="#CAB2D6", lwd=3)
axis(1, at=c(1,2,3,4,5),labels=c("<0.2","0.2-0.4", "0.4-0.6", "0.6-0.8", ">0.8"), col.axis="black", las=1)
axis(2, at=c(10,20,30,40,50,60,70),labels=c("10","20","30","40","50","60","70"), col.axis="black", las=1)
#plot(0,type='n',axes=FALSE,ann=FALSE)
legend("topleft", legend=rownames(femlost),
       col=c("#1F78B4", "#E31A1C", "#B2DF8A", "#FDBF6F", "#CAB2D6"), lwd=3, cex=1, bty = "n")

#test for significance with 2x5 chi-square
for (val in 1:5)
{
  pval<-chisq.test(rbind(maletable1[val,], maletable2[val,]))$p.value
print(pval)
if (pval<0.001) {
text(val, 35, "***", cex=1.5)
} else if (pval<0.01) {
text(val, 35, "**", cex=1.5)
} else if ( pval<0.05) {
text(val, 35, "**", cex=1.5)
} else {
###
}
}

plot(100*femlost[1,], ylim=c(30,max(rbind(100*malelost, 100*femlost))+0.1), ylab="% rediploidized", col="white", xlab="tau category", main="E. Female tissues", cex.axis=1, cex.lab=1.2, col.axis="white", xlim=c(1,5.2))
 lines(100*femlost[1,], col="#1F78B4", lwd=3)
 lines(100*femlost[2,], col="#E31A1C", lwd=3)
 lines(100*femlost[3,], col="#B2DF8A", lwd=3)
 lines(100*femlost[4,], col="#FDBF6F", lwd=3)
  lines(100*femlost[5,], col="#CAB2D6", lwd=3)
axis(1, at=c(1,2,3,4,5),labels=c("<0.2","0.2-0.4", "0.4-0.6", "0.6-0.8", ">0.8"), col.axis="black", las=1)
axis(2, at=c(10,20,30,40,50,60,70),labels=c("10","20","30","40","50","60","70"), col.axis="black", las=1)

#plot(0,type='n',axes=FALSE,ann=FALSE)
legend("topleft", legend=rownames(femlost),
       col=c("#1F78B4", "#E31A1C", "#B2DF8A", "#FDBF6F", "#CAB2D6"), lwd=3, cex=1, bty = "n")
#test for significance with 2x5 chi-square
for (val in 1:5)
{
  pval<-chisq.test(rbind(femtable1[val,], femtable2[val,]))$p.value
print(pval)
if (pval<0.001) {
text(val, 35, "***", cex=1.5)
} else if (pval<0.01) {
text(val, 35, "**", cex=1.5)
} else if ( pval<0.05) {
text(val, 35, "**", cex=1.5)
} else {
###
}
}
```

</details>

