# R code for figure 3 - neo and subfunctionalization


## Load data

<details><summary>R code for loading data</summary>

```
#set working directory to directory where "carp2022_upload" is
setwd("~/Documents/Will/Carp/Carp2022/")
library("dplyr")                          # Load dplyr package

 #get set of 1:1 orthologs
orth1<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to1_exhaust.txt")
#get set of 1:2 orthologs
orth2<-read.table("Carp2022_upload/1-Trios/Barb_Carp_1to2_exhaust_correctChrom.txt", head=F, sep=" ")
colnames(orth1)<-c("orthogroup", "barbgene", "carpgene", "chr", "subgenome")
colnames(orth2)<-c("orthogroup", "barbgene", "carpA", "chrA", "carpB", "chrB")

#Get expression
bbrain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Brain_DEseq2.txt", head=T, sep=" ")
bgonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Gonad_DEseq2.txt", head=T, sep=" ")
bheart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Heart_DEseq2.txt", head=T, sep=" ")
bliver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Liver_DEseq2.txt", head=T, sep=" ")
bspleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Barb_Spleen_DEseq2.txt", head=T, sep=" ")
brain<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Brain_DEseq2.txt", head=T, sep=" ")
gonad<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Gonad_DEseq2.txt", head=T, sep=" ")
heart<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Heart_DEseq2.txt", head=T, sep=" ")
liver<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Liver_DEseq2.txt", head=T, sep=" ")
spleen<-read.table("Carp2022_upload/2-Expression/2-Orthogroups/Carp_Spleen_DEseq2.txt", head=T, sep=" ")

#make brain tables

brain1 <- inner_join(orth1[,1:2], bbrain, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]     
brain1a <- inner_join(orth1[,c(1,3)], brain, by = c("carpgene" = "Gene"))[,c(2,4,5,12,13)]

brain2 <- inner_join(orth2[,1:2], bbrain, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
brain2a <- inner_join(orth2[,c(1,3)], brain, by = c("carpA" = "Gene"))[,c(2,4,5,12,13)]
brain2b <- inner_join(orth2[,c(1,5)], brain, by = c("carpB" = "Gene"))[,c(2,4,5,12,13)]

#make gonad tables

gonad1 <- inner_join(orth1[,1:2], bgonad, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
gonad1a <- inner_join(orth1[,c(1,3)], gonad, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

gonad2 <- inner_join(orth2[,1:2], bgonad, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
gonad2a <- inner_join(orth2[,c(1,3)], gonad, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
gonad2b <- inner_join(orth2[,c(1,5)], gonad, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]

#make heart tables

heart1<-inner_join(orth1[,1:2], bheart, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
heart1a<-inner_join(orth1[,c(1,3)], heart, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

heart2<-inner_join(orth2[,1:2], bheart, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
heart2a<-inner_join(orth2[,c(1,3)], heart, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
heart2b<-inner_join(orth2[,c(1,5)], heart, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]
#make liver tables
liver1<-inner_join(orth1[,1:2], bliver, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
liver1a<-inner_join(orth1[,c(1,3)], liver, by = c("carpgene" = "Gene"))[,c(2,4,5,11,12)]

liver2<-inner_join(orth2[,1:2], bliver, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
liver2a<-inner_join(orth2[,c(1,3)], liver, by = c("carpA" = "Gene"))[,c(2,4,5,11,12)]
liver2b<-inner_join(orth2[,c(1,5)], liver, by = c("carpB" = "Gene"))[,c(2,4,5,11,12)]
#make spleen tables
spleen1<-inner_join(orth1[,1:2], bspleen, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
spleen1a<-inner_join(orth1[,c(1,3)], spleen, by = c("carpgene" = "Gene"))[,c(2,4,5,12,13)]

spleen2<-inner_join(orth2[,1:2], bspleen, by = c("barbgene" = "Gene"))[,c(2,4,5,12,13)]
spleen2a<-inner_join(orth2[,c(1,3)], spleen, by = c("carpA" = "Gene"))[,c(2,4,5,12,13)]
spleen2b<-inner_join(orth2[,c(1,5)], spleen, by = c("carpB" = "Gene"))[,c(2,4,5,12,13)]

```

</details>


<details><summary>R code for making normalized expression table with all tissues/species</summary>

```
all<-(cbind(brain2$meanF, brain2a$meanF, brain2b$meanF, gonad2$meanF, gonad2a$meanF, gonad2b$meanF, heart2$meanF, heart2a$meanF, heart2b$meanF,liver2$meanF, liver2a$meanF, liver2b$meanF, spleen2$meanF, spleen2a$meanF, spleen2b$meanF, brain2$meanM, brain2a$meanM, brain2b$meanM, gonad2$meanM, gonad2a$meanM, gonad2b$meanM, heart2$meanM, heart2a$meanM, heart2b$meanM,liver2$meanM, liver2a$meanM, liver2b$meanM, spleen2$meanM, spleen2a$meanM, spleen2b$meanM))
colnames(all)<-c("barb_brainF", "subA_brainF", "subB_brainF", "barb_gonadF", "subA_gonadF", "subB_gonadF", "barb_heartF", "subA_heartF", "subB_heartF",  "barb_liverF", "subA_liverF", "subB_liverF",  "barb_spleenF", "subA_spleenF", "subB_spleenF", "barb_brainM", "subA_brainM", "subB_brainM", "barb_gonadM", "subA_gonadM", "subB_gonadM", "barb_heartM", "subA_heartM", "subB_heartM",  "barb_liverM", "subA_liverM",  "subB_liverM",  "barb_spleenM", "subA_spleenM", "subB_spleenM")

###Normalize
bolFMat<-as.matrix(all, nrow = nrow(all), ncol = ncol(all))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(all)
colnames(temp2)<-colnames(all)
all <-as.data.frame(temp2)
rownames(all)<-brain2$barbgene

all1<-(cbind(brain1$meanF, brain1a$meanF, gonad1$meanF, gonad1a$meanF, heart1$meanF, heart1a$meanF, liver1$meanF, liver1a$meanF, spleen1$meanF, spleen1a$meanF, brain1$meanM, brain1a$meanM, gonad1$meanM, gonad1a$meanM, heart1$meanM, heart1a$meanM, liver1$meanM, liver1a$meanM, spleen1$meanM, spleen1a$meanM))
colnames(all1)<-c("barb_brainF", "subA_brainF",  "barb_gonadF", "subA_gonadF", "barb_heartF", "subA_heartF",  "barb_liverF", "subA_liverF", "barb_spleenF", "subA_spleenF", "barb_brainM", "subA_brainM",  "barb_gonadM", "subA_gonadM", "barb_heartM", "subA_heartM", "barb_liverM", "subA_liverM",  "barb_spleenM", "subA_spleenM")


###Normalize
bolFMat<-as.matrix(all1, nrow = nrow(all1), ncol = ncol(all1))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(all1)
colnames(temp2)<-colnames(all1)
all1 <-as.data.frame(temp2)
rownames(all1)<-brain1$barbgene

#just for fun, heatmap with 1:1
##Heatmap
#store list of colors in variable "crazycols" 
crazycols<-c("pink", "pink", " pink", "pink", " pink", "pink", "pink", " pink", "pink", " pink", "lightgreen", "lightgreen", "lightgreen", "lightgreen", "lightgreen", "lightgreen", "lightgreen", "lightgreen", "lightgreen", "lightgreen")
gencols<-c("#FF7F00", "#A6CEE3",  "#FF7F00", "#A6CEE3","#FF7F00", "#A6CEE3",  "#FF7F00", "#A6CEE3", "#FF7F00", "#A6CEE3", "#FF7F00", "#A6CEE3",  "#FF7F00", "#A6CEE3", "#FF7F00", "#A6CEE3",  "#FF7F00", "#A6CEE3",  "#FF7F00", "#A6CEE3")

#tell R to use the library gplots
library(gplots)

#draw the heatmap
heatmap.2(cor(all1, method="spearman"), col= colorRampPalette(c("blue", "white", "red", "firebrick4"))(15), ColSideColors=crazycols, RowSideColors = gencols, scale="none", symm=T, margins = c(15,15), key=T, trace="none")

#add legend
legend("topright",      # location of the legend on the heatmap plot
    legend = c("female", "male."), # category labels
    col = c("pink", "lightgreen"),  # color key
    lty= 1,             # line style
    lwd = 10            # line width)
)


```

</details>

## Encode different states and transitions

Let's try a very simple approach of assigning one of two states (expressed, not expressed) to each gene in every tissue. We then score transitions as changes between barb and carp in these states. For each gene, we end up with a list of -1 (loss of expression ), 0 (no change) or 1 (gain of expression). Because we leave a buffer zone between expressed (>5TPM) and not expressed (<2TPM), we also have "NA"s both in the expression state and in the list of changes. 

Then we classify genes as:
- conserved: at least one tissue has conserved expression, and no tissue show gain or loss of expression.
- degenerated: at least one tissue has lost expression, and no tissues show gain or conservation.
- subfunctionalized: at least one tissue has lost expression, at least one tissue is conserved, and no tissue has gained expression.
- neofunctionalized: at least one tissue has gained expression, at least one tissue is conserved, and no tissue has lost expression.
- upregulated: at least one tissue has gained expression, no tissues have conserved or lost expression.
- mixed: at least one tissue has gained expression and one tissue has lost expression.

<details><summary>R code for classification</summary>

```
allclean<-all
maxnon<-2
minexp<-5
#replace small numbers with 0
allclean[allclean > maxnon &  allclean < minexp] <- NA
allclean[allclean < maxnon] <- 0
allclean[allclean > minexp] <- 1

barbclean<-allclean[,grep("barb", colnames(allclean))]
subAclean<-allclean[,grep("subA", colnames(allclean))]
subBclean<-allclean[,grep("subB", colnames(allclean))]

#look at change
subA_change<-subAclean-barbclean
subB_change<-subBclean-barbclean

#count for each gene how many tissues have lost, gain or conserved expression
subA_change$count.L <- apply(subA_change[,c(1:10)], 1, function(x) length(which(x=="-1")))
subA_change$count.G <- apply(subA_change[,c(1:10)], 1, function(x) length(which(x=="1")))
subA_change$count.C <- apply(subA_change[,c(1:10)], 1, function(x) length(which(x=="0")))
subB_change$count.L <- apply(subB_change[,c(1:10)], 1, function(x) length(which(x=="-1")))
subB_change$count.G <- apply(subB_change[,c(1:10)], 1, function(x) length(which(x=="1")))
subB_change$count.C <- apply(subB_change[,c(1:10)], 1, function(x) length(which(x=="0")))

#combine and summarize
allchanges<-cbind(subA_change[,c(11,12,13)], subB_change[,c(11,12,13)])
allchanges$codeA<-"NA"
allchanges$codeB<-"NA"

#create new table
newtable<-allchanges[1,]

#add classification
for(i in 1:nrow(allchanges)) {
    row <- allchanges[i,]
    # do stuff with row

#conserved genes
if (row[,1]<1 & row[,2]<1 & row[,3]>0) {
row$codeA<-"C"

#degenerated genes
} else if (row[,1]>0 & row[,2]<1 & row[,3]<1) {
row$codeA<-"D"
#subfunctionalized genes
} else if (row[,1]>0 & row[,2]<1 & row[,3]>0) {
row$codeA<-"S"
#Neofunctionalized genes
} else if (row[,1]<1 & row[,2]>0 & row[,3]>0) {
row$codeA<-"N"
#Upregulated genes
} else if (row[,1]<1 & row[,2]>0 & row[,3]<1) {
row$codeA<-"U"
#Mixed genes --> also neofunctionalized
} else if (row[,1]>0 & row[,2]>0) {
row$codeA<-"N"
} else {
###
}

newtable<-rbind (newtable, row)

}

#get rid of dummy first line
newtable = newtable[-1,]
###SAME FOR SUBB
newtable2<-newtable[1,]

#add classification
for(i in 1:nrow(newtable)) {
    row <- newtable[i,]
    # do stuff with row

#conserved genes
if (row[,4]<1 & row[,5]<1 & row[,6]>0) {
row$codeB<-"C"

#degenerated genes
} else if (row[,4]>0 & row[,5]<1 & row[,6]<1) {
row$codeB<-"D"
#subfunctionalized genes
} else if (row[,4]>0 & row[,5]<1 & row[,6]>0) {
row$codeB<-"S"
#Neofunctionalized genes
} else if (row[,4]<1 & row[,5]>0 & row[,6]>0) {
row$codeB<-"N"
#Upregulated genes
} else if (row[,4]<1 & row[,5]>0 & row[,6]<1) {
row$codeB<-"U"
#Mixed genes --> also neofunctionalized
} else if (row[,4]>0 & row[,5]>0) {
row$codeB<-"N"
} else {
###
}

newtable2<-rbind (newtable2, row)

}

#get rid of dummy first line
newtable2 = newtable2[-1,]
```

</details>

<details><summary>R code for classification - 1:1 genes</summary>

```
allclean_11<-all1
maxnon<-2
minexp<-5
#replace small numbers with 0
allclean_11[allclean_11 > maxnon &  allclean_11 < minexp] <- NA
allclean_11[allclean_11 < maxnon] <- 0
allclean_11[allclean_11 > minexp] <- 1

barbclean_11<-allclean_11[,grep("barb", colnames(allclean_11))]
subAclean_11<-allclean_11[,grep("subA", colnames(allclean_11))]

#look at change
subA_change_11<-subAclean_11-barbclean_11

#count for each gene how many tissues have lost, gain or conserved expression
subA_change_11$count.L <- apply(subA_change_11[,c(1:10)], 1, function(x) length(which(x=="-1")))
subA_change_11$count.G <- apply(subA_change_11[,c(1:10)], 1, function(x) length(which(x=="1")))
subA_change_11$count.C <- apply(subA_change_11[,c(1:10)], 1, function(x) length(which(x=="0")))
changes_11<-subA_change_11[,c(11,12,13)]
changes_11$codeA<-"NA"

#create new table
newtable_11<-changes_11[1,]

#add classification
for(i in 1:nrow(changes_11)) {
    row <- changes_11[i,]
    # do stuff with row

#conserved genes
if (row[,1]<1 & row[,2]<1 & row[,3]>0) {
row$codeA<-"C"

#degenerated genes
} else if (row[,1]>0 & row[,2]<1 & row[,3]<1) {
row$codeA<-"D"
#subfunctionalized genes
} else if (row[,1]>0 & row[,2]<1 & row[,3]>0) {
row$codeA<-"S"
#Neofunctionalized genes
} else if (row[,1]<1 & row[,2]>0 & row[,3]>0) {
row$codeA<-"N"
#Upregulated genes
} else if (row[,1]<1 & row[,2]>0 & row[,3]<1) {
row$codeA<-"U"
#Mixed genes --> also neofunctionalized
} else if (row[,1]>0 & row[,2]>0) {
row$codeA<-"N"
} else {
###
}

newtable_11<-rbind (newtable_11, row)

}

#get rid of dummy first line
newtable_11 = newtable_11[-1,]
```

</details>



## Plot total distribution

```
dev.new(width=6, height=5)
layout( matrix(c(1,1,2,3), ncol=2) )
par(mar=c(9,3,2,0.2))
par(mgp=c(2,0.6,0))
#compare distribution of states between 1:1 and 1:2
summarytable<-as.data.frame(rbind(table(newtable_11[,4]), table(newtable2[,c(7)]), table(newtable2[,8])))
 summarytable$sum<-rowSums(summarytable)
 summarytable/summarytable$sum
barplot(as.matrix(summarytable[,-c(4,7)]/summarytable$sum), beside=T, col=c("#FF7F00", "#A6CEE3", "#B2DF8A"), ylab="Proportion", main="A. Gene classification", names=c("Conserved", "Downregulated", "Neofunctionalized", "Subfunctionalized", "Upregulated"), las=2, cex.names=1.1)

#add legend
legend("center",      # location of the legend on the heatmap plot
    legend = c("1:1", "subgenome A", "subgenome B"), # category labels
    col = c("#FF7F00", "#A6CEE3", "#B2DF8A"),  # color key
    lty= 1,             # line style
    lwd = 10            # line width)
)

#test distribution of 1:1 vs A
chisq.test(as.matrix(summarytable[c(1,2),-7]))
#test 1:1 vs B
chisq.test(as.matrix(summarytable[c(1,3),-7]))
#test A vs B
chisq.test(as.matrix(summarytable[c(2,3),-7]))
```

## Plot proportion with changes in each tissue

```
 par(mar=c(3,3,2,0.2))
#subset neo and subfunctionalized genes
neoB<-subset(newtable2[,c(7,8)], codeB=="N" & codeA=="C")
subfB<-subset(newtable2[,c(7,8)], codeB=="S" & codeA=="C")
#count how many times each tissue changed in the two categories
colSums(merge(neoB, subB_change, by=0)[,c(4:13)], na.rm=T)
abs(colSums(merge(subfB, subB_change, by=0)[,c(4:13)], na.rm=T))

#same but divided by number of genes in each category
neoBprop<-colSums(merge(neoB, subB_change, by=0)[,c(4:13)], na.rm=T)/dim(neoB)[1]
subfBprop<-abs(colSums(merge(subfB, subB_change, by=0)[,c(4:13)], na.rm=T))/dim(subfB)[1]

 boxnames<-c("brain", "gonad", "heart", "liver", "spleen")
 barplot(as.matrix(rbind(neoBprop[c(1:5)], neoBprop[c(6:10)])), beside=T, col=c("#FB9A99", "#A6CEE3"), ylab="Proportion", names=boxnames, main="B. Neofunctionalized")
 barplot(as.matrix(rbind(subfBprop[c(1:5)], subfBprop[c(6:10)])), beside=T, col=c("#FB9A99", "#A6CEE3"), ylab="Proportion", names=boxnames, main="C. Subfunctionalized")
```
